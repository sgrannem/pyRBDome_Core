#!/usr/bin/python


__author__      = "Sander Granneman"
__copyright__   = "Copyright 2025"
__version__     = "0.2.5"
__credits__     = ["Sander Granneman"]
__maintainer__  = ["Sander Granneman"]
__email__       = "Sander.Granneman@ed.ac.uk"
__status__      = "beta"

import sys
from setuptools import setup, find_packages

package_data = {
    			'chrome' : [
               				'Chrome/chrome-linux64/chrome',
                			'Chrome/chromedriver-linux64/chromedriver'
                			]
                }

setup(name='pyRBDome',
	version = '%s' % __version__,
	description = 'Python code for analysing RNA-binding domain mass-spec data.',
	author = 'Sander Granneman',
	author_email = 'Sander.Granneman@ed.ac.uk',
	packages = find_packages(),
	package_data = package_data,
	install_requires = ['selenium >= 3.15.0',
						'argparse >= 1.1',
						'regex >= 2.5.84',
						'biopandas >= 0.2.7',
						'biopython',
						'scipy',
						'scikit-learn >=0.13',
						'statannotations == 0.6.0',
						'matplotlib_venn',
						'xlrd >= 1.0.0',
						'openpyxl',
						'pathlib',
						'pypdb',
						'pandas >= 1.2.3',
						'numpy >= 1.6.5',
						'PyPDF2',
						'pyopenms',
						'bs4',
						'optuna',
						'lxml',
						'pyyaml',
						'reportlab',
						'db-sqlite3',
						'jupyterlab',
						'matplotlib == 3.7.3',
						'urllib3',
						'xgboost',
						'seaborn < 0.12.0',
						'GenbankParser',
						'papermill',
						'statsmodels == 0.14.1',
						'ipywidgets'
						],
	scripts=[
					'pyRBDome/Scripts/BindUP_submit.py',
					'pyRBDome/Scripts/RNABindRPlus_submit.py',
					'pyRBDome/Scripts/FTMap_submit.py',
					'pyRBDome/Scripts/GetSequencesFromPDB.py',
					'pyRBDome/Scripts/ProteinNAdistanceAnalyses.py',
					'pyRBDome/Scripts/FindUniprotRNPStructures.py',
                			'pyRBDome/Scripts/FindUniprotPDBStructures.py',
                    			'pyRBDome/Scripts/PST_PRNA_submit.py',
					'pyRBDome/Scripts/Local_PST_PRNA_submit.py'
		],
	classifiers=[   'Development Status :: 5 - Production/Stable',
					'Environment :: Console',
					'Intended Audience :: Education',
					'Intended Audience :: Developers',
					'Intended Audience :: Science/Research',
					'License :: Freeware',
					'Operating System :: MacOS :: MacOS X',
					'Operating System :: POSIX',
					'Programming Language :: Python :: 3.9',
					'Topic :: Scientific/Engineering :: Bio-Informatics',
					'Topic :: Software Development :: Libraries :: Application Frameworks'
				]
		)
