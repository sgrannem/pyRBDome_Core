# pyRBDome-Core package

## Contents

- [Introduction](#introduction)

- [Repo Contents](#repo-contents)

- [System Requirements](#system-requirements)

- [Installation Guide](#installation-guide)

- [License](./LICENSE)

- [Citation](#citation)

## Introduction

pyRBDome-Core and pyRBDome-Notebooks are packages for protein RNA-binding sites prediction. They combine multiple and distinct RNA-binding prediction algorithms to identify putative RNA-binding amino acids within proteins. The algorithms predict RNA-binding propensity from different aspects, for example using only the protein sequences or a combination fo protein sequence and protein structure. It then aggregates all the data into easily interpretable PDB and PDF data files, which can then be used to desing mutations for more detailed functional analyses. This package (pyRBDome-Core) contains a number of classes, with associated notebooks, that can be readily incorporated into other Python scripts. This includes code for manipulating PDB files, calculating distances between atomic coordinates, mapping peptide sequences to PDB files, etc.

## Repo Contents

- [pyRBDome-Core](./pyRBDome): Python code, including Classes and relevant functions.

- [pyRBDome/Test_Notebooks](./pyRBDome/Test_Notebooks): folder with test data and test code

## System Requirements  
  
### Hardware Requirements  

These data analyses are  computationally intensive, and we routinely run the jobs on 20-40 processors so a server with ~64 cores and at least 64GB of RAM is recommended.
  
The runtimes vary considerably, depending on how large the sequencing data files are.  
But a typical run generally takes several hours to complete.  
  
### Software Requirements  
  
#### OS Requirements  
  
The installer has been tested on Mac OS 12.7 and extensively on Ubuntu OS versions 12.04 to 23.10. It requires Python 3.9, is likely to run fine on Python 3.10 but it hasn't been tested on later versions.

Unfortunately some code in pyRBDome-Core are not compatible with Windows and we will also not be suppporting this operating system.
  
**Other dependencies:**  

These dependencies need to be installed in case you want to run the Jupyter notebooks described in the pyRBDome pipeline.

 - Python 3.9
 - ncbi-blast-2.13.0+ (For building AlphaFold models https://ftp.ncbi.nlm.nih.gov/blast/executables/LATEST/)
 - interproscan-5.61-93.0 (https://interproscan-docs.readthedocs.io/en/latest/HowToRun.html)
 - MM-align (https://zhanggroup.org/MM-align/)
 - RNABindRPlus-Local (https://github.com/jpetucci/RNABindRPlus-Local)

pyRBDome-Core uses Python Selenium to connect to various web severs. For this it installs the Linux version of Chrome and the associated chrome driver on your system. To run the pipeline on Mac OS, you therefore need to download chrome and chrome driver manually and place these in the Chrome folder contained in the pyRBDome package. Make sure that the chromedriver version is exaclty the same as chrome. You then need to modify the config.py file in this package to tell the tool where to find the binaries:

```
PACKAGE_DIR = os.path.dirname(os.path.abspath(__file__))
### Below the default chrome path is for linux. You will need to change this if you want to run the package on Mac OS
CHROME_PATH = os.path.join(PACKAGE_DIR,'Chrome','chrome-macos','chrome')
### Make sure the chromedriver script has the exact same version as chrome.
CHROME_DRIVER_PATH = os.path.join(PACKAGE_DIR,'Chrome','chromedriver-macos','chromedriver')

os.environ['PATH'] = f"{os.environ['PATH']}:{os.path.dirname(CHROME_DRIVER_PATH)}"
```

Then you can install the package as described below.

## Installation Guide

To install the package, we strongly recommend that you install Anaconda first and then generate a new Python evironment:

`conda create -n pyrbdome python=3.9`

Then activate this conda environment:

`conda activate pyrbdome`

To download the package you need to install 'git' and run the following command:

`git clone https://git.ecdf.ed.ac.uk/sgrannem/pyRBDome_Core`

Then go into the directory:

`cd pyrbdome_dev`

Then run the following command:

`pip install -e .`

Make sure to include the dot ('.') at the end of this command, otherwise it won't run!
This command will also install jupyter lab, which you need for running test and pyRBDome data analysis notebooks.

For running our modified version of PST-PRNA locally as well as interpro scan, you need to install the following packages within the pyrbdome environment:

```
conda install -c conda-forge -c schrodinger pymol-bundle
conda install -c anaconda openjdk 
conda install -y bioconda::blast
conda install -y bioconda::cd-hit
conda install -y bioconda::hhsuite
conda install -y bioconda::reduce
conda install -y salilab::dssp
conda install -y bioconda::msms
```