#!/usr/bin/env python
# coding: utf-8

__author__		= "Sander Granneman"
__copyright__	= "Copyright 2023"
__version__		= "0.0.3"
__credits__		= ["Sander Granneman"]
__maintainer__	= "Sander Granneman"
__email__		= "Sander.Granneman@ed.ac.uk"
__status__		= "beta"

##################################################################################
#
#	FindUniprotRNPStructures
#
#
#	Copyright (c) Sander Granneman 2023
#
#	Permission is hereby granted, free of charge, to any person obtaining a copy
#	of this software and associated documentation files (the "Software"), to deal
#	in the Software without restriction, including without limitation the rights
#	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#	copies of the Software, and to permit persons to whom the Software is
#	furnished to do so, subject to the following conditions:
#
#	The above copyright notice and this permission notice shall be included in
#	all copies or substantial portions of the Software.
#
#	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#	THE SOFTWARE.
#
##################################################################################

import os
import sys
import glob
import shutil
import pandas as pd
import requests
import argparse
from pathlib import Path
from biopandas.pdb import PandasPdb
from biopandas.pdb.engines import *
from pandas import json_normalize

def getRCSBPdbInfo(pdb_ids):
    """ Gets the sequence chain and molecule info for pdb_ids and returns
    the results in a pandas dataframe
    """

    query = """
    {
      entries(entry_ids: [%s]) {
        rcsb_id
        polymer_entities {
          rcsb_polymer_entity_container_identifiers {
            entity_id
            asym_ids
            auth_asym_ids
            reference_sequence_identifiers {
              database_name
              database_accession
            }
          }
          entity_poly {
            pdbx_seq_one_letter_code_can
            rcsb_sample_sequence_length
            type
          }
          rcsb_polymer_entity {
            formula_weight
          }
        }
      }
    }
    """ % ",".join(['"%s"' % i for i in pdb_ids])

    ### Requesting the data from rcsb:
    response = requests.post("https://data.rcsb.org/graphql", json={'query': query})
    df = pd.DataFrame()
    
    if response.status_code == 200:
    
        ### Converting the data to json format:
        data = response.json()

        ### Storing the results in a pandas dataframe:
        df = pd.DataFrame()

        for i in data['data']['entries']:
            rcsb_id = i['rcsb_id']
            for j in i['polymer_entities']:
                sequence = j['entity_poly']['pdbx_seq_one_letter_code_can']
                entity_id = j['rcsb_polymer_entity_container_identifiers']['entity_id']
                chains = ",".join(j['rcsb_polymer_entity_container_identifiers']['auth_asym_ids'])
                entity_type = j['entity_poly']['type']
                try:
                    uniprot_id = j['rcsb_polymer_entity_container_identifiers']['reference_sequence_identifiers'][0]['database_accession']
                except TypeError:
                    uniprot_id = None
                row = {'rcsb_id':rcsb_id,
                       'entity_id':entity_id,
                       'chain':chains,
                       'sequence':sequence,
                       'entity_type':entity_type,
                       'uniprot_id':uniprot_id
                      }
                new_df = pd.DataFrame([row])
                df = pd.concat([df, new_df], axis=0, ignore_index=True)
        if not df.empty:
            df = df.sort_values(by='entity_id')
        
    else:
        sys.stderr.write(f"ERROR! {response.status_code}.\n")

    return df
    
def getPDBs(uniprot_id,max_pdbs=10,outdir="./",overwrite=False):
    """ Using a uniprot id as input, the code searhcers on rcsb to 
    find complexes that have the protein of interest in it and also
    nucleic acids. """
    ### Getting the pdb ids for the uniprot number:
    pdb_ids = Query(uniprot_id, query_type="uniprot").search()
    ### Filtering the pdb ids:
    pdb_ids = sorted(set(pdb_ids))

    ### Getting the information about the PDB ids from rcsb.org:
    df = getRCSBPdbInfo(pdb_ids)

    if df.any():
        ### Grabbing only tho RNA chains and the protein with the Uniprot name:
        filtered = df.loc[(df.uniprot_id == uniprot_id)]
        pdb_ids = sorted(set(filtered['rcsb_id']))

        ### Removing pdb files if there are too many:
        if max_pdbs:
            if len(pdb_ids) > max_pdbs:
                pdb_ids = pdb_ids[:max_pdbs]

        ### Downloading and filtering the pdb files:
        for pdb in pdb_ids:
            file_name = "%s.pdb" % pdb
            output_file_dir = os.path.join(outdir,uniprot_id,pdb)
            output_file_path = os.path.join(output_file_dir,file_name)
            if not os.path.exists(output_file_dir):
                os.makedirs(output_file_dir)
            if  os.path.exists(output_file_path) and not overwrite:
                sys.stderr.write("The filtered pdb file %s already exists. Not overwriting.\n" % file_name)
            else:
                ### Getting the chains:
                chains = list()
                chains.extend(filtered[filtered.rcsb_id == pdb].chain.str.split(",").values)
                chains = [item for sublist in chains for item in sublist]
                downloadPDBChains(pdb,chains,output_file_path)
    else:
        sys.stderr.write("No pdb files available for Uniprot ID %s.\n" % uniprot_id)

def getRCSBUniprotPDBs(uniprot_id,resolution=4.0):
    """ Gets the sequence chain and molecule info for pdb_ids and returns
    the results in a pandas dataframe
    """

    url = "https://search.rcsb.org/rcsbsearch/v2/query"

    payload =  {
                  "query": {
                    "type": "group",
                    "nodes": [
                      {
                        "type": "terminal",
                        "service": "full_text",
                        "parameters": {
                          "value": uniprot_id
                        }
                      },
                      {
                        "type": "group",
                        "nodes": [
                          {
                            "type": "terminal",
                            "service": "text",
                            "parameters": {
                              "attribute": "pdbx_database_status.pdb_format_compatible",
                              "operator": "exact_match",
                              "value": "Y"
                            }
                          },
                          {
                            "type": "terminal",
                            "service": "text",
                            "parameters": {
                              "attribute": "rcsb_entry_info.resolution_combined",
                              "operator": "less_or_equal",
                              "value": resolution
                            }
                          }
                        ],
                        "logical_operator": "and"
                      }
                    ],
                    "logical_operator": "and"
                  },
                  "return_type": "entry",
                  "request_options": {
                    "results_content_type": [
                      "computational",
                      "experimental"
                    ],
                    "sort": [
                      {
                        "sort_by": "score",
                        "direction": "desc"
                      }
                    ],
                    "scoring_strategy": "combined"
                  }
                }
    
    ### Requesting the data from rcsb:
    headers = {'Content-Type': 'application/json'}
    response = requests.post(url, json=payload, headers=headers)
        
    if response.status_code == 200:
        ### Converting the data to json format:
        data = response.json()

        ### Converting the json data to a pandas dataframe
        df = pd.DataFrame.from_dict(data['result_set'])
        df.columns = ["pdb_id","score"]
        df.set_index("pdb_id",inplace=True)
        return df
    else:
        sys.stderr.write(f"Failed to download PDB information for Uniprot ID: {uniprot_id}\n")
        sys.stderr.write(f"Error:\t{response.status_code}\n")
        return pd.DataFrame() 
    
def downloadPDBfile(pdb_id,out_dir=".",overwrite=False):
    """ Downloads the PDB files for the pdb files of interest """
    
    outfile = f"{pdb_id}.pdb"
    outfile_path = os.path.join(out_dir,outfile)
    if os.path.exists(outfile_path) and not overwrite:
        sys.stderr.write(f"PDB file {pdb_id}.pdb already downloaded, not overwriting!\n")
        return True
    else:    
        base_url = "https://files.rcsb.org/download/"
        pdb_url = f"{base_url}{pdb_id}.pdb"
        response = requests.get(pdb_url)

        if response.status_code == 200:
            pdb_content = response.text
            if pdb_content:
                with open("%s/%s.pdb" % (out_dir,pdb_id), "w") as pdb_file:
                    pdb_file.write(pdb_content)
                sys.stdout.write(f"PDB file {pdb_id}.pdb downloaded successfully.\n")
                return True
        else:
            sys.stderr.write(f"Failed to download PDB file for RCSB ID: {pdb_id}\n")
            sys.stderr.write(f"Error:\t{response.status_code}\n")
            return False

def main():
    parser = argparse.ArgumentParser(description="Script for analysing distances between amino acids and nucleic acids in pdb files")
    parser.add_argument("--uniprot_id",
                        dest="uniprot_id",
                        help="Name of the uniprot_id you want to find protein-nucleic acid structures for.",
                        metavar="O43660",
                        default=None)
    parser.add_argument("--outfilename",
                        dest="outfilename",
                        help="Name of the pdb_output_file",
                        metavar="outfilename",
                        default=".")
    parser.add_argument("--overwrite",
                        dest="overwrite",
                        help="To overwrite any existing output files. Default is False",
                        action="store_true")
    parser.add_argument("--maxres",
                        dest="maxres",
                        help="The lowest average resolution the structure should have",
                        type=float,
                        default=5.0)
    parser.add_argument("--maxpdbs",
                        dest="maxpdbs",
                        help="The maximum number of pdbs that you want to download and process. Default is 3",
                        type=int,
                        default=3)

    args = parser.parse_args()

    ### Getting all the pdb files for a specific uniprot id that contain the protein and nucleic acids.
    pdbs = getRCSBUniprotPDBs(args.uniprot_id,resolution=args.maxres)

    ### Getting all the sequence and chain information from rcsb.org for these pdb files:
    structure_info = getRCSBPdbInfo(pdbs.index)
    
    if not structure_info.empty:
        ### Only keeping the maximum number of pdb_ids allowed:
        pdb_ids = list(set(structure_info.loc[:,'rcsb_id']))
        pdb_ids_to_keep = pdb_ids[:args.maxpdbs]
        
        ### Removing any non-relevant proteins from the structure information dataframe:
        selected_pdbs = list()
        filtered = structure_info.loc[(structure_info['rcsb_id'].isin(pdb_ids_to_keep)) & \
                                      (structure_info['uniprot_id'] == args.uniprot_id)]
        
        selected_pdbs = list(set(filtered.loc[:,'rcsb_id']))
        ### Now downloading the pdb files. 
        ### NOTE! Cif files are ignored as they cause too many problems with downstream analyses!!
        
        downloaded_pdbs = list()
        
                
        if selected_pdbs:
            ### Making the necessary directories for downloading:
            pdb_dir = "pdb_files"
            output_dir = f"{args.uniprot_id}/{pdb_dir}"
            if not os.path.exists(args.uniprot_id):
                os.mkdir(args.uniprot_id)
            if not os.path.exists(output_dir):
                os.mkdir(output_dir)
                
            for pdb_id in selected_pdbs:
                downloaded = downloadPDBfile(pdb_id,output_dir)
                if downloaded:
                    downloaded_pdbs.append(pdb_id)
                ### NOTE! We only download pdb files as CIF files cause problems with downstream analyses.
                ### This means that very large protein-RNA complexes, such as ribosomes, will be ignored.

            if not downloaded_pdbs:
                sys.stderr.write(f"ERROR! Could not download any pdb files for Uniprot id {args.uniprot_id}! Exiting.\n")
                shutil.rmtree(args.uniprot_id)
            else:
                ### Now for each pdb file, I have to open it and grab the respective chains
                ### for the protein and the nucleic acids:
                filtered_out_dir = f"{args.uniprot_id}/filtered_pdb_files"
                if not os.path.isdir(filtered_out_dir):
                    os.mkdir(filtered_out_dir)

                for pdb_file in glob.glob(f"{args.uniprot_id}/{pdb_dir}/*.pdb"):
                    pdb_id = os.path.splitext(os.path.basename(pdb_file))[0]
                    out_file = f"{filtered_out_dir}/{args.uniprot_id}_{pdb_id}.pdb"

                    if os.path.exists(out_file):
                        sys.stderr.write(f"The outputfile {out_file} already exists. Not overwriting!\n")
                    else:
                        pdb = PandasPdb()
                        pdb.read_pdb(f"{args.uniprot_id}/{pdb_dir}/{pdb_id}.pdb")

                        ### Now extracting only the relevant protein and NA chains:
                        protein_chains = filtered.loc[(filtered['rcsb_id'] == pdb_id) & \
                                                      (filtered['uniprot_id'] == args.uniprot_id),'chain'].values
                        protein_chains = [item for sublist in [item.split(',') if ',' in item else [item] \
                                                               for item in protein_chains] for item in sublist]
                        if protein_chains:
                            ### Only selecting a SINGLE chain for the protein! We do not want homomultimeric complexes to deal with!
                            if len(list(protein_chains)) > 1:
                                protein_chains = list(protein_chains[0])
                            pdb.df['ATOM'] = pdb.df['ATOM'][pdb.df['ATOM']['chain_id'].isin(protein_chains)]
                            ### Writing to a new pdb file:
                            pdb.to_pdb(f"{filtered_out_dir}/{args.uniprot_id}_{pdb_id}.pdb",records=['ATOM'])
                            sys.stdout.write(f"Successfully processed the data for Uniprot id {args.uniprot_id}.\n")
                        else:
                            sys.stderr.write(f"Protein_chains:\t{protein_chains}")
    else:
        sys.stderr.write(f"ERROR! Could not find any structures for {args.uniprot_id} that fit the requirements! Aborting.\n")

if __name__ == "__main__":
    main()
