#!/usr/bin/env python
# coding: utf-8

__author__       = "Sander Granneman"
__copyright__    = "Copyright 2025"
__version__      = "0.0.4"
__credits__      = ["Sander Granneman"]
__maintainer__   = "Sander Granneman"
__email__        = "Sander.Granneman@ed.ac.uk"
__status__       = "beta"

##################################################################################
#
#    PST_PRNA_submit
#
#
#    Copyright (c) Sander Granneman 2025
#
#    Permission is hereby granted, free of charge, to any person obtaining a copy
#    of this software and associated documentation files (the "Software"), to deal
#    in the Software without restriction, including without limitation the rights
#    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#    copies of the Software, and to permit persons to whom the Software is
#    furnished to do so, subject to the following conditions:
#
#    The above copyright notice and this permission notice shall be included in
#    all copies or substantial portions of the Software.
#
#    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#    THE SOFTWARE.
#
##################################################################################


import os
import re
import sys
import time
import tempfile
import zipfile
import shutil
import argparse
import selenium
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from pyRBDome import config

def runPSTPRNAanalyses(pdb_file,out_dir,verbose=True,headless=True,overwrite=False):
    """ Runs the analysis on the PST-PRNA web server"""

    ### Check that the file actually exists!:
    if not os.path.exists(pdb_file):
        sys.stderr.write(f"ERROR! Cannot locate the {pdb_file}!\n ")
        return False
    pdb_file = os.path.abspath(pdb_file)
    pdb_file_name = os.path.splitext(os.path.basename(pdb_file))[0]
    results_file = os.path.join(out_dir, f"{pdb_file_name}.pdb.zip")
    temp_zip_file_path = os.path.join(out_dir, "temp.zip")

    ### Checking if the output file already exists:
    results_dir = os.path.join(out_dir, f"{pdb_file_name}_PST-PRNA_results")
    if os.path.exists(results_file) and not overwrite:
        sys.stderr.write(f"Results for the pdb file {pdb_file_name} have been downloaded and you asked not to overwrite existing files. Exiting\n")
        return False

    ### Checking if the output file already exists:
    results_dir = f"{out_dir}/{pdb_file_name}_PST-PRNA_results"
    if os.path.exists(results_file) and not overwrite:
        sys.stderr.write(f"Results for the pdb file {pdb_file_name} have been downloaded and you asked not to overwrite existing files. Exiting\n")
        return False

    ### Website url:
    url = "http://www.zpliulab.cn/PSTPRNA/"

    ### Set Chrome options
    chrome_options = webdriver.ChromeOptions()
    chrome_options.binary_location = config.CHROME_PATH

    ### Configure Chrome options:
    chrome_options.add_argument(f'--chromedriver={config.CHROME_DRIVER_PATH}')
    chrome_options.add_experimental_option('prefs', {
        "download.default_directory": out_dir,  # Change default directory for downloads
        "download.prompt_for_download": False,  # To auto download the file
        "download.directory_upgrade": True,
        "plugins.always_open_pdf_externally": True  # It will not show PDF directly in chrome
    })

    if headless:
        chrome_options.add_argument('--headless')

    chrome_options.add_argument("--remote-debugging-port=9222")  # This avoids user data conflicts
    chrome_options.add_argument("--window-size=1920,1200")
    chrome_options.add_argument("--disable-extensions")
    chrome_options.add_argument('--disable-gpu')
    chrome_options.add_argument('--disable-dev-shm-usage')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--ignore-certificate-errors')

    ### Initialize the Selenium WebDriver with the specified options.
    browser = webdriver.Chrome(options=chrome_options)

    ### Loading the page:
    browser.get(url)

    # Handle "continue" button if it appears
    try:
        premessage = WebDriverWait(browser, 5).until(
            EC.element_to_be_clickable((By.ID, "continue"))
        )
        premessage.click()  # Click the "continue" button to go to the actual website
        time.sleep(3)  # Wait for the next page to load
    except Exception as e:
        print(f"Continue button not found or error: {e}")  # Log error if button is not found

    ### Submit pdb file
    if verbose:
        sys.stdout.write(f"Submitting pdb file {pdb_file_name}\n")

    # Wait for the formFile element to be present before sending keys
    WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.ID, "formFile"))
    )

    # Send the PDB file path to the formFile input field
    browser.find_element(By.ID, value="formFile").send_keys(pdb_file)

    # Wait for the submit button to be clickable before clicking it
    WebDriverWait(browser, 10).until(
        EC.element_to_be_clickable((By.CSS_SELECTOR, '.btn.btn-xs.btn-primary'))
    )

    # Click the submit button
    browser.find_element(By.CSS_SELECTOR, value='.btn.btn-xs.btn-primary').click()

    # Optional: Add a small delay to ensure the submission is processed
    time.sleep(2)  # Adjust delay as necessary based on your application's response time


def main():
    parser = argparse.ArgumentParser(description='Script for submitting jobs to aaRNA\n')
    parser.add_argument("--pdb_id",
                        dest="pdb_id",
                        help="Name of the pdb ID you want to analyse.",
                        metavar="7L0A",
                        default=None)
    parser.add_argument("--out_dir",
                        dest="out_dir",
                        help="Path to the directory where the files will be stored",
                        metavar="download",
                        default=".")
    parser.add_argument("-v",
                        "--verbose",
                        dest="verbose",
                        action="store_true",
                        help="Shows the status messages",
                        default=True)
    parser.add_argument("--overwrite",
                        dest="overwrite",
                        action="store_true",
                        help="use this to overwrite existing output files",
                        default=False)
    parser.add_argument("--headless",
                        dest="headless",
                        action="store_true",
                        help="To switch off the headless mode, which makes it possible \
                        to follow each step in a browser. Useful for debugging.",
                        default=True)

    args = parser.parse_args()

    if not args.pdb_id:
        parser.error("You forgot to include the pdb ID name. Please use the --pdb_id flag\n")

    pdb_file = os.path.abspath(args.pdb_id)

    runPSTPRNAanalyses(pdb_file,args.out_dir,args.verbose,args.headless,args.overwrite)

if __name__ == "__main__":
    main()




