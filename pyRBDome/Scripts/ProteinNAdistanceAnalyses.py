#!/usr/bin/env python
# coding: utf-8

__author__		= "Sander Granneman"
__copyright__	= "Copyright 2024"
__version__		= "0.0.4"
__credits__		= ["Sander Granneman"]
__maintainer__	= "Sander Granneman"
__email__		= "Sander.Granneman@ed.ac.uk"
__status__		= "beta"

##################################################################################
#
#	ProteinNADistanceAnalyses
#
#
#	Copyright (c) Sander Granneman 2024
#
#	Permission is hereby granted, free of charge, to any person obtaining a copy
#	of this software and associated documentation files (the "Software"), to deal
#	in the Software without restriction, including without limitation the rights
#	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#	copies of the Software, and to permit persons to whom the Software is
#	furnished to do so, subject to the following conditions:
#
#	The above copyright notice and this permission notice shall be included in
#	all copies or substantial portions of the Software.
#
#	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#	THE SOFTWARE.
#
##################################################################################

import pandas as pd
import sys
import re
import glob
import os
import regex
import argparse
from biopandas.pdb import PandasPdb
from biopandas.pdb.engines import *
from pyRBDome.Classes.PeptideAnalyser import *

def distanceAnalysis(coordinates,reference_coordinates):
    """ Function for calculating the minimum euclidian distances
    between molecule coordinates and reference coordinates in a pdb 
    structure. Requires lists of arrays as input. """
    tree = spatial.cKDTree(np.array(reference_coordinates.values))
    min_distance = min(tree.query(np.array(coordinates))[0])
    return min_distance

def calculateDistancesToNucleicAcids(pdb_file,binary=False,threshold=4.2,outfile=None,overwrite=False):
    """ Calculates for each residue within the pdb file containing a protein-nucleic acid complex 
    the distances for each amino acid to the closest nucleotide. If you set binary is set
    to True then it will add a 1 to the b-factor column for each amino acid in the structure
    that is within hydrogen-bonding distance from a nucleic acid to 1 and the rest to 0. 
    If binary is False then it will just report the distances for each amino acid in the
    b-factor column. """
    
    aminoacidlist = ['ASH', 'ALA', 'ASX', 'CYX', 'CSD', 'CYS', 
    				 'CGV', 'CSO', 'OCS', 'YCM', 'ASP', 'GLU', 
                     'PHE', 'GLY', 'HIS', 'HID', 'HIE', 'HIP', 
                     'ILE', 'LYS', 'LEU', 'MET', 'MSE', 'ASN', 
                     'PYL', 'HYP', 'PRO', 'GLN', 'ARG', 'SER', 
                     'THR', 'SEL', 'VAL', 'TRP', 'TYR', 'GLX', 
                     'GLH']
    
    nucleotidelist = ['A','U','T','C','G','a','u','t','c','g']

    ### DNA nucleotides are sometimes indicated with 'D' (deoxy) in front of the nucleotide
    nucleotidelist.extend(['DA','DC','DG','DT','da','dc','dg','dt'])
    
    ### Opening the pdb file:
    pdb = PeptidePDBAnalyser()
    pdb.loadPDBFile(pdb_file,records=['ATOM'])
    
    if not outfile:
        basename = os.path.splitext(os.path.basename(pdb_file))[0]
        outfile = "%s_with_distances_to_NA.pdb" % basename
        
    if not overwrite and os.path.exists(outfile):
        sys.stderr.write("Outputfile %s already exists! Not overwriting!\n" % outfile)
        return False
        
    protein_chains = list(set(pdb.pdb_df[pdb.pdb_df.residue_name.isin(aminoacidlist)]['chain_id']))
    protein_df = pdb.pdb_df[pdb.pdb_df.chain_id.isin(protein_chains)]
    
    nucleotide_chains = list(set(pdb.pdb_df[pdb.pdb_df.residue_name.isin(nucleotidelist)]['chain_id']))
    if not nucleotide_chains:
        sys.stderr.write("ERROR! No nucleotide sequences found in pdb file %s" % pdb_file)
        return False
    else:
        nucleotide_df = pdb.pdb_df[pdb.pdb_df.chain_id.isin(nucleotide_chains)]

        nucleotide_coordinates = nucleotide_df[['x_coord','y_coord','z_coord']]
        aminoacid_coordinates = protein_df[['x_coord','y_coord','z_coord']]

        ### Calculating the minimal distances for each pdb file:
        for i in aminoacid_coordinates.index:
            coordinates = aminoacid_coordinates.loc[i,['x_coord','y_coord','z_coord']].values
            if len(coordinates) == 3:
                coordinates = [coordinates]
            try:
                min_distance = distanceAnalysis(coordinates,nucleotide_coordinates)
            except TypeError:
                sys.stderr.write(f"pdb_file:\t{pdb_file}\n")
                sys.stderr.write(f"Length coordinates:\t{len(coordinates)}\n")
                sys.stderr.write(f"Coordinates:\t{coordinates}\n")
                sys.stderr.write(f"Length reference coordinates:\t{len(reference_coordinates.values)}\n")
                sys.stderr.write(f"Reference Coordinates:\t{reference_coordinates.values}\n")
            else:
                if binary:
                    if min_distance <= threshold:
                        protein_df.loc[i,'b_factor'] = 1
                    else:
                        protein_df.loc[i,'b_factor'] = 0
                else:
                    protein_df.loc[i,'b_factor'] = min_distance
            
        ### Writing the output file:
        pdb.writePDBFile(outfilename=outfile,records=['ATOM'],compress=False,df=protein_df)
        return True

def main():
    parser = argparse.ArgumentParser(description="Script for analysing distances between amino acids and nucleic acids in pdb files")
    parser.add_argument("--pdb_file",
                        dest="pdb_file",
                        help="Name of the pdb ID you want to analyse.",
                        metavar="7L0A.pdb",
                        default=None)
    parser.add_argument("--outfilename",
                        dest="outfilename",
                        help="Name of the pdb_output_file",
                        metavar="output_dir",
                        default=".")
    parser.add_argument("--binary",
                        dest="binary",
                        help="Highlights amino acids that are within a certain threshold of nucleic acids as a 1 in the b-factor column. Rest will be 0",
                        action="store_true")
    parser.add_argument("--threshold",
                        dest="threshold",
                        help="To set a distance threshold for --binary for what constitutes an interaction with nucleic acids. Default = 4.2A",
                        type=float,
                        default=4.2)
    parser.add_argument("--overwrite",
                        dest="overwrite",
                        help="In case you want to overwrite the results from previous analyses. Default is False.",
                        action="store_true")

    args = parser.parse_args()

    calculateDistancesToNucleicAcids(args.pdb_file,
                                     args.binary,
                                     args.threshold,
                                     args.outfilename,
                                     args.overwrite)
    
    
if __name__ == "__main__":
	main()
    
    
    
    
    
    
    
    
    
    
    
    


