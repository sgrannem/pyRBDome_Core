#!/usr/bin/env python
# coding: utf-8

__author__		= "Sander Granneman"
__copyright__	= "Copyright 2020"
__version__		= "0.0.1"
__credits__		= ["Sander Granneman"]
__maintainer__	= "Sander Granneman"
__email__		= "Sander.Granneman@ed.ac.uk"
__status__		= "beta"

##################################################################################
#
#	FTMap_submit
#
#
#	Copyright (c) Sander Granneman 2020
#
#	Permission is hereby granted, free of charge, to any person obtaining a copy
#	of this software and associated documentation files (the "Software"), to deal
#	in the Software without restriction, including without limitation the rights
#	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#	copies of the Software, and to permit persons to whom the Software is
#	furnished to do so, subject to the following conditions:
#
#	The above copyright notice and this permission notice shall be included in
#	all copies or substantial portions of the Software.
#
#	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#	THE SOFTWARE.
#
##################################################################################

import os
import sys
import time
import argparse
from pyRBDome.Classes.FTMap import *

def main():
    parser = argparse.ArgumentParser(description='Script for submitting jobs to STP\n')
    parser.add_argument("--pdb_id",
                        dest="pdb_id",
                        help="Name of the pdb ID you want to analyse.",
                        metavar="7L0A",
                        default=None)
    parser.add_argument("--chains",
                        dest="chains",
                        help="The chains you want to analyse. By default it will analyse all",
                        metavar="AB",
                        default="All")
    parser.add_argument("--out_dir",
    					dest="out_dir",
    					help="Path to the directory where the files will be stored",
    					metavar="download",
    					default=".")
    parser.add_argument("--username",
                        dest="username",
                        help="Your FTMap username",
                        metavar="yourfavusrname",
                        default=None)
    parser.add_argument("--password",
                        dest="password",
                        help="Your FTMap password",
                        metavar="abcdefg",
                        default=None)
    parser.add_argument("-v",
                        "--verbose",
                        dest="verbose",
                        action="store_true",
                        help="Shows the status messages",
                        default=True)
    parser.add_argument("--ow",
                        "--overwrite",
                        dest="overwrite",
                        action="store_true",
                        help="use this to overwrite existing output files",
                        default=False)

    args = parser.parse_args()

    if not args.pdb_id:
        parser.error("You forgot to include the pdb ID name. Please use the --pdb_id flag\n")
    if not args.chains:
        parser.error("You forgot to include the chains that you want to analyse. Please use the --chains flag\n")
    if not args.username:
        parser.error("You need to provide your FTMap username to login to the website\n")
    if not args.password:
        parser.error("You need to provide your FTMap password to login to the website\n")

    ftmap = FTMapAnalysis(headless=True,
                          verbose=args.verbose,
                          overwrite=args.overwrite,
                          out_dir=args.out_dir)

    ftmap.connectToServer(username=args.username,password=args.password)
    ftmap.submitJob(pdb_id=args.pdb_id,chains=args.chains)

    while not ftmap.jobIsFinished():
        time.sleep(120)
        if time.job_finished:
            break
    else:
        ftmap.downloadResults()
        ftmap.renameResultsFile()
        ftmap.closeBrowser()

if __name__ == "__main__":
    main()
