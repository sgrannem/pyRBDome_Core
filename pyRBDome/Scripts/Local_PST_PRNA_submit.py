#!/usr/bin/env python
# coding: utf-8

__author__       = "Sander Granneman"
__copyright__    = "Copyright 2025"
__version__      = "0.0.1"
__credits__      = ["Sander Granneman"]
__maintainer__   = "Sander Granneman"
__email__        = "Sander.Granneman@ed.ac.uk"
__status__       = "beta"

##################################################################################
#
#    PST_PRNA_submit
#
#
#    Copyright (c) Sander Granneman 2025
#
#    Permission is hereby granted, free of charge, to any person obtaining a copy
#    of this software and associated documentation files (the "Software"), to deal
#    in the Software without restriction, including without limitation the rights
#    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#    copies of the Software, and to permit persons to whom the Software is
#    furnished to do so, subject to the following conditions:
#
#    The above copyright notice and this permission notice shall be included in
#    all copies or substantial portions of the Software.
#
#    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#    THE SOFTWARE.
#
##################################################################################


import os
import re
import sys
import shutil
import argparse

PST_PRNA_DIR = '/home/pyrbdome/Software/PST-PRNA/'
MODEL_PATH = '/home/pyrbdome/Software/PST-PRNA/data/model/best_9.ckpt'

sys.path.append(PST_PRNA_DIR)

from protein_for_predict import *
from predict import *
from pyRBDome.Classes.PDBAnalyser import *

def runPSTPRNAanalyses(pdb_file,chain, out_dir,verbose=True,overwrite=False):
    """ Runs the analysis on the PST-PRNA web server"""

    ### Check that the file actually exists!:
    if not os.path.exists(pdb_file):
        sys.stderr.write(f"ERROR! Cannot locate the {pdb_file}!\n ")
        return False
    pdb_file = os.path.abspath(pdb_file)
    pdb_id = os.path.splitext(os.path.basename(pdb_file))[0]
    
    ### Extracting the features:
    rbp = RBP(pdb_file,chain,out_dir)
    rbp.get_data()

    ### Dictionary file:
    stored_features = f"{out_dir}/{pdb_id}_{chain}.h5"

    ### Calculating RNA-binding propensities for each protein:
    predict(stored_features,model_path=MODEL_PATH, out_dir=out_dir)

    ### Storing the results in a PDB file:
    prediction_results_path = f"{out_dir}/{pdb_id}_{chain}_predictions.csv"

    if not overwrite and os.path.exists(prediction_results_path):
        pdb = PDBAnalyser()
        pdb.loadPDBFile(pdb_file)

        ### Setting the name and path of the pdb output file:
        pdb_out_file = f"{out_dir}/{pdb_id}_{chain}_PST_PRNA.pdb"

        results_df = pd.read_csv(prediction_results_path, sep=",", header=0, index_col=0)

        # Vectorized operation to update 'b_factor' based on 'residue_number'
        pdb.pdb_df['b_factor'] = pdb.pdb_df['residue_number'].map(results_df['RNA_binding_score'].to_dict()).fillna(0)

        ### Writing the pdb file:
        pdb.writePDBFile(pdb_out_file,records=['ATOM'])    

    return True

def main():
    parser = argparse.ArgumentParser(description='Script for submitting jobs to aaRNA\n')
    parser.add_argument("--pdb_id",
                        dest="pdb_id",
                        help="Name of the pdb ID you want to analyse.",
                        metavar="7L0A",
                        default=None)
    parser.add_argument("--chain",
                        dest="chain",
                        metavar="A",
                        default=None)
    parser.add_argument("--out_dir",
                        dest="out_dir",
                        help="Path to the directory where the files will be stored",
                        metavar="download",
                        default=".")
    parser.add_argument("-v",
                        "--verbose",
                        dest="verbose",
                        action="store_true",
                        help="Shows the status messages",
                        default=True)
    parser.add_argument("--overwrite",
                        dest="overwrite",
                        action="store_true",
                        help="use this to overwrite existing output files",
                        default=False)

    args = parser.parse_args()

    if not args.pdb_id:
        parser.error("You forgot to include the pdb ID name. Please use the --pdb_id flag\n")

    pdb_file = os.path.abspath(args.pdb_id)
    if not args.chain:
        sys.stderr.write("ERROR! You need to provide a chain for the analyses!\n")
        exit()

    runPSTPRNAanalyses(pdb_file,args.out_dir,args.verbose,args.headless,args.overwrite)

if __name__ == "__main__":
    main()




