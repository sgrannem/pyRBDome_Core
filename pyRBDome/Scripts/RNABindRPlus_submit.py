#!/usr/bin/env python
# coding: utf-8

__author__		= "Sander Granneman"
__copyright__	= "Copyright 2023"
__version__		= "0.1.6"
__credits__		= ["Sander Granneman"]
__maintainer__	= "Sander Granneman"
__email__		= "Sander.Granneman@ed.ac.uk"
__status__		= "beta"

##################################################################################
#
#	RNABindRPlus_submit
#
#
#	Copyright (c) Sander Granneman 2023
#
#	Permission is hereby granted, free of charge, to any person obtaining a copy
#	of this software and associated documentation files (the "Software"), to deal
#	in the Software without restriction, including without limitation the rights
#	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#	copies of the Software, and to permit persons to whom the Software is
#	furnished to do so, subject to the following conditions:
#
#	The above copyright notice and this permission notice shall be included in
#	all copies or substantial portions of the Software.
#
#	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#	THE SOFTWARE.
#
##################################################################################

import os
import sys
import time
import urllib.request, urllib.error, urllib.parse
import subprocess
import argparse
from pathlib import Path
from biopandas.pdb import PandasPdb
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.chrome.service import Service as ChromeService
from pyRBDome.Classes.PDBAnalyser import *
from pyRBDome.Functions.RBDomeAnalysisCode import *
from pyRBDome.Functions.RBDomeSQL import *
from pyRBDome import config

def getSeqsFromPDBFile(pdb_id,chains,pdb_dir):
    """ Looks in PDB files and returns amino acid sequences for specific chains """

    pdb_file = "%s/%s.pdb" % (pdb_dir,pdb_id)
    pdb_file = os.path.abspath(pdb_file)

    try:
        pdb = PDBAnalyser()
        pdb.loadPDBFile(pdb_file,records=['ATOM'],remove_ligands=True)
    except FileNotFoundError:
        sys.stderr.write("The pdb %s was not found in the directory %s\n" % (pdb_id,pdb_dir))
        return None
    else:
        fasta = str()
        for chain in chains:
            sequence = pdb.getPDBProteinSequence(chain)
            if not "?" in sequence: 
                pdb_id = os.path.basename(pdb_id)
                uniprot_id = pdb_id.split("_")[0]
                fasta += ">%s_%s\n%s\n" % (uniprot_id,chain,sequence)
            else:
                sys.stderr.write("The sequence for uniprot_id %s and chains %s has strange characters:\n%s\n") \
                % (uniprot_id,chains,sequence)
                return None
        return fasta

def runRNABindRPlus(pdb_ids,chains,pdb_dir,job_title="test",e_mail_address=None,verbose=True,headless=True):

    """ Runs the RNAbindRPlus on the website. Requires fasta file containing
    the pdb ids and the amino acid sequence of the chain that you want to analyse.
    You can only analyse one chain at a time. This script will also automatically
    rename the file into a more human readible format. This script was successfully
    tested on MacOSX and Ubuntu machines using Chrome as default browser.
    """

    if verbose:
        sys.stdout.write("Submitting pdbs %s with chains %s and job title '%s'\n" \
        % (pdb_ids,chains,job_title))

    ### Getting the fasta sequences:
    sequences = str()
    for pdb_id,chain in list(zip(pdb_ids,chains)):
        sequences += getSeqsFromPDBFile(pdb_id,chain,pdb_dir)
    if not sequences:
        sys.stderr.write("Could not find sequences for chains %s in pdb %s!\n" % (chains,pdb_ids))
        return False

    if verbose:
        sys.stdout.write("Amino acid sequences have been successfully extracted from the pdb files\n")

    # Correctly initialize the ChromeDriver service
    service = ChromeService(executable_path=config.CHROME_DRIVER_PATH)

    # Set up the Chrome options
    chrome_options = webdriver.ChromeOptions()
    chrome_options.binary_location = config.CHROME_PATH

    if headless:
        chrome_options.add_argument("--headless")
    chrome_options.add_argument("--no-sandbox")  # Bypass OS security model, necessary on certain systems
    chrome_options.add_argument("--disable-dev-shm-usage")  # Overcome limited resource problems

    # Instantiate a Chrome session
    driver = webdriver.Chrome(service=service, options=chrome_options)
    driver.implicitly_wait(20)

    ### NOTE! This url is linked to an RNABindRPlus installation on your local machine!
    url = "http://0.0.0.0:8080/RNABindRPlus"
    driver.get(url)

    ### Providing the e-mail address:
    e_mail = driver.find_element(By.NAME, value="email")
    e_mail.clear()
    e_mail.send_keys(e_mail_address)

    ### Setting the job title:
    title = driver.find_element(By.NAME, value="JobTitle")
    title.clear()
    title.send_keys(job_title)

    ### Pasting the fasta sequences in the text box:
    sequence_box = driver.find_element(By.NAME, value="QuerySeq")
    sequence_box.clear()
    sequence_box.send_keys(sequences)

    ### Submitting sequences:
    driver.set_page_load_timeout(600)  # Timeout 600 seconds

    submit = driver.find_element(By.NAME, value=".submit")

    try:
        submit.click()
    except:
        if verbose:
            sys.stderr.write("ERROR! Your job could not be submitted!\n")
        driver.close()

    else:
        if verbose:
            sys.stdout.write("Your job has been submitted!\n")
        
        ### Getting the download link and waiting for the final predictions to finish:
        try:
            # Wait until the "Final Predictions" link is visible
            final_predictions_link = WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located((By.LINK_TEXT, "Final Predictions"))
            )
            
            # Get the URL of the "Final Predictions" link
            file_url = final_predictions_link.get_attribute("href")
            
            # Now you can use the file_url as needed
            
        except Exception as e:
            print("An error occurred:", e)
            driver.quit()
        else:

            # Define the maximum number of retries
            max_retries = 100
            retry_interval = 240  # Time to wait between retries in seconds

            # Perform retries
            for i in range(max_retries):
                response = requests.get(file_url)
                if response.status_code == 200:
                    output_file = f"{job_title}_results.txt"
                    # Download the file
                    with open(output_file, 'wb') as file:
                        file.write(response.content)
                    if verbose:
                        sys.stdout.write("File downloaded successfully!\n")
                    break
                else:
                    if verbose:
                        sys.stderr.write(f"File not yet ready. Retrying in {retry_interval} seconds...\n")
                    time.sleep(retry_interval)
            else:
                if verbose:
                    sys.stderr.write("Max retries exceeded. File may not be available.\n")

def main():
    parser = argparse.ArgumentParser(description='Script for submitting jobs to RNABindRPlus\n')
    parser.add_argument("--pdb_ids",
    					dest="pdb_ids",
                        nargs='*',
    					help="Name of the pdb IDs you want to analyse. You can analyse several at a time.",
    					metavar="7L0A",
    					default=None)
    parser.add_argument("--chains",
    					dest="chains",
                        nargs='*',
    					help="The chains you want to analyse. You can analyse several at a time.",
    					metavar="AB",
    					default="None")
    parser.add_argument("--pdb_dir",
                        dest="pdb_dir",
                        help="Path to the directory containing the pdb files that you want to analyse.",
                        metavar="pdb_files",
                        default="pdb_files")
    parser.add_argument("--job_title",
                        dest="job_title",
                        help="Provide a title for the job. Default is test.",
                        metavar="test",
                        default="test")
    parser.add_argument("--e_mail",
                        dest="e_mail",
                        help="You need to provide your e-mail address for RNABindRPlus.",
                        default=None)
    parser.add_argument("-v",
                        "--verbose",
                        dest="verbose",
                        action="store_true",
                        help="Shows the status messages",
                        default=False)

    args = parser.parse_args()

    if not args.pdb_ids:
        parser.error("You forgot to include the pdb ID name. Please use the --pdb_id flag\n")
    if not args.chains:
        parser.error("You forgot to include the chains you want to analyse.\n")
    if not args.e_mail:
        parser.error("You need to provide an e-mail address for the job.\n")

    runRNABindRPlus(pdb_id=args.pdb_ids,
                    chains=args.chains,
                    pdb_dir=args.pdb_dir,
                    job_title=args.job_title,
                    e_mail_address=args.e_mail,
                    verbose=args.verbose)

    return True

if __name__ == "__main__":
	main()
