#!/usr/bin/env python
# coding: utf-8

__author__		= "Sander Granneman"
__copyright__	= "Copyright 2020"
__version__		= "0.0.1"
__credits__		= ["Sander Granneman"]
__maintainer__	= "Sander Granneman"
__email__		= "Sander.Granneman@ed.ac.uk"
__status__		= "beta"

##################################################################################
#
#	GetSequencesFromPDB
#
#
#	Copyright (c) Sander Granneman 2020
#
#	Permission is hereby granted, free of charge, to any person obtaining a copy
#	of this software and associated documentation files (the "Software"), to deal
#	in the Software without restriction, including without limitation the rights
#	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#	copies of the Software, and to permit persons to whom the Software is
#	furnished to do so, subject to the following conditions:
#
#	The above copyright notice and this permission notice shall be included in
#	all copies or substantial portions of the Software.
#
#	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#	THE SOFTWARE.
#
##################################################################################

import os
import sys
import argparse
import pandas as pd
import numpy as np
from biopandas.pdb import PandasPdb

def getSeqsFromPDBFile(pdb_id,chains,pdb_dir,output_file=None):
    """ Looks in PDB files and returns amino acid sequences for specific chains """

    if not output_file:
        output = sys.stdout
    else:
        output = open(output_file,"w")

    ppdb = PandasPdb()
    try:
        ppdb.read_pdb("%s/%s.pdb" % (pdb_dir,pdb_id))
    except FileNotFoundError:
        sys.stderr.write("The pdb %s was not found in the directory %s\n" % (pdb_id,pdb_dir))
    else:
        sequence_df = ppdb.amino3to1()
        for chain in chains:
            sequence = ''.join(sequence_df[sequence_df['chain_id'] == chain]['residue_name'].values)
            if not "?" in sequence: ### to remove sequences with strange characters:
                output.write(">%s_%s\n%s\n" % (pdb_id,chain,sequence))
            else:
                sys.stderr.write("The sequence for pdb_id %s and chains %s has strange characters:\n%s\n") \
                % (pdb_id,chains,sequence)

    output.close()

def main():
    parser = argparse.ArgumentParser(description='Script for submitting jobs to aaRNA\n')
    parser.add_argument("--pdb_id",
    					dest="pdb_id",
    					help="Name of the pdb ID you want to analyse.",
    					metavar="7L0A",
    					default=None)
    parser.add_argument("--chains",
    					dest="chains",
    					help="The chains you want to analyse. By default it will analyse all",
    					metavar="AB",
    					default="All")
    parser.add_argument("--pdb_dir",
                        dest="pdb_dir",
                        help="Path to the directory containing the pdb files to analyse",
                        metavar="pdb_files",
                        default="pdb_files")
    parser.add_argument("-o",
                        "--output_file",
    					dest="output_file",
    					help="To print the sequences to an output file. Default is standard output",
    					metavar="out.txt",
    					default=None)

    args = parser.parse_args()

    if not args.pdb_id:
        parser.error("You forgot to include the pdb ID name. Please use the --pdb_id flag\n")
    if not args.chains:
    	parser.error("YOu forgot to include the chains you want to analyse.\n")

    getSeqsFromPDBFile(pdb_id=args.pdb_id,
                       chains=args.chains,
                       pdb_dir=args.pdb_dir,
                       output_file=args.output_file)

    return True

if __name__ == "__main__":
	main()
