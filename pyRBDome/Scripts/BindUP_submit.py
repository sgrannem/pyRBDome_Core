#!/usr/bin/env python
# coding: utf-8

__author__		= "Sander Granneman"
__copyright__	= "Copyright 2025"
__version__		= "0.0.3"
__credits__		= ["Sander Granneman"]
__maintainer__	= "Sander Granneman"
__email__		= "Sander.Granneman@ed.ac.uk"
__status__		= "beta"

##################################################################################
#
#	BindUP_submit
#
#
#	Copyright (c) Sander Granneman 2025
#
#	Permission is hereby granted, free of charge, to any person obtaining a copy
#	of this software and associated documentation files (the "Software"), to deal
#	in the Software without restriction, including without limitation the rights
#	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#	copies of the Software, and to permit persons to whom the Software is
#	furnished to do so, subject to the following conditions:
#
#	The above copyright notice and this permission notice shall be included in
#	all copies or substantial portions of the Software.
#
#	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#	THE SOFTWARE.
#
##################################################################################

import os
import sys
import argparse
from pyRBDome.Classes.BindUP import *

def main():
    parser = argparse.ArgumentParser(description='Script for submitting jobs to BindUP\n')
    parser.add_argument("--pdb_file",
                        dest="pdb_file",
                        help="Name of the pdb ID you want to analyse.",
                        metavar="7L0A",
                        default=None)
    parser.add_argument("--chain",
                        dest="chain",
                        help="Name of the chain you want to analyse.",
                        metavar="A",
                        default=None)
    parser.add_argument("--out_dir",
    					dest="out_dir",
    					help="Path to the directory where the files will be stored",
    					metavar="download_dir",
    					default=".")
    parser.add_argument("-v",
                        "--verbose",
                        dest="verbose",
                        action="store_true",
                        help="Shows the status messages",
                        default=False)
    parser.add_argument("--ow",
                        "--overwrite",
                        dest="overwrite",
                        action="store_true",
                        help="use this to overwrite existing output files",
                        default=False)
    parser.add_argument("--headless",
                        dest="headless",
                        action="store_true",
                        help="To switch on the headless mode.",
                        default=False)

    args = parser.parse_args()

    if not args.pdb_file:
        parser.error("You forgot to include the pdb ID name. Please use the --pdb_id flag\n")

    analyser = BindUPanalysis(verbose=args.verbose,
                              overwrite=args.overwrite,
                              headless=args.headless,
                              out_dir=args.out_dir)
    analyser.connectToServer()
    analyser.submitJob(pdb_file=args.pdb_file,chain=args.chain)
    analyser.downloadResults()
    analyser.closeBrowser()

if __name__ == "__main__":
    main()
