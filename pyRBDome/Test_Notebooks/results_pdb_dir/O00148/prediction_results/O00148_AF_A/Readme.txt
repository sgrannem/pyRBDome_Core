############################################
##### Explanation of Prediction Result #####
############################################

Predictor returns files nominated in the following way:

1. BP_1_X_Y.dat

2. BP_16_X_Y.dat

3. BP_X_Y.pdf

4. X_Y_EC.pdb

5. X_Y_LNMIN.pdb

6. X_Y_LNMAX.pdb

7. X_Y_BP.pdb

8. X_Y.pml

9. Uploaded_PDB_FileName.ori (optional)

X is the name of target protein according to user's input, and Y is the chain identifier/identifiers.
Specific for prediction using uploaded structure file, an empty file combining the original file name and extension .ori is included.


File 1: binary binding propensity result
The first column indicates the chain identifier, the second column indicates residues' number and insertion code (if any) in the structure, and the last column gives the binding propensity scores.

File 2: di-nucleotide binding propensity result
Similar to the binary propensity file, di-nucleotide propensity file gives 16 columns of propensity scores following the column of residues' number and insertion code.

File 3: a residue-based plot of two types of binding propensities.
The top Xticklable lists residue names along a protein chain. If a residues is not solved in the structure, it will be marked as "_" and shown as "X" on the binary plot.
The middle Xticklable lists the indices of residues on sequence. The first residue will be numbered as 1.
The bottom Xticklable lists residue number + insertion code (if existing) of residues in the structure.
Column-wise binary and di-nucleotide binding propensities are aligned along their residue positions.
A blue dashed line on the binary plot indicates the cutoff (0.164) under a stringency of 85% expected specificity evaluated from the training dataset.
Residues with predicted scores greater than this cutoff are viewed as binding residues.

File 4: a structure file with residues' evolution conservation values tweaked into B-factor column of the pdb file.

File 5: a structure file with residues' LN values under a small scale tweaked into B-factor column of the pdb file.

File 6: a structure file with residues' LN values under a large scale tweaked into B-factor column of the pdb file.

File 7: a structure file with residues' binary binding propensities tweaked into B-factor column of the pdb file.

File 8: a Pymol script that will display structures of files 4-7 side by side.
To execute this script, please load it from command line by inputting "pymol X_Y.pml".
To display only the slot for binding propensity, please input "unset grid_mode" at pymol command line.
