__author__              = ["Niki Christopoulou","Sophie Winterbourne","Sander Granneman"]
__copyright__           = "Copyright 2023"
__version__             = "0.1.0"
__credits__             = ["Niki Christopoulou","Sophie Winterbourne","Sander Granneman"]
__maintainer__          = "Sander Granneman"
__email__               = "Sander.Granneman@ed.ac.uk"
__status__              = "beta"

##################################################################################
#
#       pyRBDome SQLite functions
#
#
#       Copyright (c) Niki Christopoulou and Sander Granneman 2021
#
#       Permission is hereby granted, free of charge, to any person obtaining a copy
#       of this software and associated documentation files (the "Software"), to deal
#       in the Software without restriction, including without limitation the rights
#       to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#       copies of the Software, and to permit persons to whom the Software is
#       furnished to do so, subject to the following conditions:
#
#       The above copyright notice and this permission notice shall be included in
#       all copies or substantial portions of the Software.
#
#       THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#       IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#       FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#       AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#       LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#       OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#       THE SOFTWARE.
#
##################################################################################

import sqlite3
import sys
import pandas as pd

### SQLite is case insensitive. Table and column names can be typed in uppercase, lowercase, or mixed case.
### When sqlite3.connect(database) is used, then the database is created in case it does not exist. When sqlite3.connect('file:%s?mode=rw' % database, uri=True) is used, the database is not created if it does not already exist, the connection is simply not created.

def dataframeToSQLite_overwrite(dataframe, table_name, database, verbose=True):
    """
    Writes records stored in a pandas DataFrame (argument 'dataframe') to an SQLite database.
    If a database with the provided name does not exist, then itis created.
    If a table with the same name already exists in the database, it is overwritten.
    """
    try:
        connection=sqlite3.connect(database)
        cursor=connection.cursor()
        dataframe.to_sql(table_name, con=connection, if_exists='replace', index=False)
        connection.commit()
        connection.close()
        
    except Exception as error:
        if verbose:
            sys.stderr.write("'%s' table could not be created in database '%s'. Error message: '%s' \n" % 
                             (table_name, database, error))
            return False
    else:
        if verbose:
            sys.stdout.write("Table '%s' successfully created in database '%s'! \n" % (table_name, database))
        return True
    
def dataframeToSQLite_append_only_new(dataframe, table_name, database, filtering_column = 'ID', verbose=True):
    """
    Writes records stored in a pandas DataFrame (argument 'dataframe') to an SQLite database.
    If a database with the provided name does not exist, then itis created.
    If a table with the same name already exists in the database, it only adds to this table the entries that do 
    not exist, based on the column that you specify using the 'filtering_column' argument. 
    By default, it filters based on a column named 'ID'.
    The filtering_column should exist in both the existing table and the table to be added.
    """
    try:
        connection=sqlite3.connect(database)
        cursor=connection.cursor()
        #first, read the table already existing in the database (if it exists)
        existing_df = loadTableFromSQLite(table_name, database, index_col=None, verbose = False)
        #if the database is empty, just write the input dataframe in the database
        if existing_df.empty:
            dataframe.to_sql(table_name, con=connection, index=False)
        else:        
            #then, read the dataframe that is to be added and discard the overlapping part of it
            extra_entries = []
            if filtering_column in dataframe.columns and filtering_column in existing_df.columns:
                for i in dataframe[filtering_column]:
                    if i not in list(existing_df[filtering_column]):
                        extra_entries.append(i)
                dataframe_to_add = dataframe[dataframe[filtering_column].isin(extra_entries)]
                #now, add the dataframe to the database
                dataframe_to_add.to_sql(table_name, con=connection, if_exists='append', index=False)
            else:
                raise ValueError("The table already exists in the database, so you need to provide the name of a column that exists in both the existing table and the dataframe that is to be added, in order to filter and keep the entries that do not exist already.")
        connection.commit()
        connection.close()
        
    except Exception as error:
        if verbose:
            sys.stderr.write("'%s' table could not be created in database '%s'. Error message: '%s' \n" % 
                             (table_name, database, error))
            return False
    else:
        if verbose:
            sys.stdout.write("Table '%s' successfully updated in database '%s'! \n" % (table_name, database))
        return True
    
def tabToSQLite(file_name, table_name, database, verbose = True):
    """
    Writes records stored in a tab-delimited text file (argument 'file_name') to an SQLite database.
    If a database with the provided name does not exist, then itis created.
    If a table with the same name already exists in the database, it is overwritten.
    """
       
    try:
        connection=sqlite3.connect(database)
        cursor=connection.cursor()
        dataframe=pd.read_csv(file_name,sep="\t")
        dataframe.to_sql(table_name, con=connection, if_exists='replace', index=False)
        connection.commit()
        connection.close()

    except Exception as error:
        if verbose:
            sys.stderr.write("'%s' table could not be created in database '%s'. Error message: '%s' \n" % 
                             (table_name, database, error))
            return False
    else:
        if verbose:
            sys.stdout.write("Table '%s' successfully created in database '%s'! \n" % (table_name, database))
        return True                    
    
def loadTableFromSQLite(table_name, database, index_col=None, verbose = True):
    """
    Loads a table from a database into a pandas dataframe.
    The table name (argument 'table_name') and the database name (argument 'database') need to be provided.
    The index of the dataframe is numerical by default, but you can specify which column of the table you want to use as index (argument 'index_col').
    If the table does not exist in the database, then an empty pandas dataframe is returned.
    """

    try:
        #with the format below, if the database does not already exist, it is not created.
        connection=sqlite3.connect('file:%s?mode=rw' % database, uri=True)
        cursor=connection.cursor()
        dataframe = pd.read_sql('SELECT * FROM %s' % table_name, con=connection, index_col=index_col)
        connection.close()
    except Exception as error:
        if verbose:
            sys.stderr.write("'%s' table could not be loaded from database '%s'. Error message: '%s' \n" % 
                             (table_name, database, error))
        #return an empty dataframe, so later I can use the 'if loadTableFromSQLite.empty' to check if data already exist on database
        return (pd.DataFrame())
    else:
        if verbose:
            sys.stdout.write("Table '%s' successfully loaded from database '%s' \n" % (table_name, database))
        return (dataframe)                 

def tableFromSQLiteToTab(table_name, database):
    """
    This function extracts the contents of a table of a database and writes them in a tab-delimited text file.
    It produces a text file in the working directory, which has the same name as the table and ends in .txt. 
    """
    
    table = loadTableFromSQLite(table_name, database)
    if not table.empty:
        table.to_csv('%s.txt' % table_name, index=False)
    
def renameTableFromSQLite(table_name, new_table_name, database, verbose=True):
    """
    This functions changes the name of a table of a database.
    The existing name of the table (argument 'table_name') and the new name of the table (argument 'new_table_name') must be provided.
    If either the database or the table does not exist (or another error occurs), then it will return False and give an error message.
    """
    
    try:
        #with the format below, if the database does not already exist, it is not created.
        connection=sqlite3.connect('file:%s?mode=rw' % database, uri=True)
        cursor=connection.cursor()
        cursor.execute("ALTER TABLE %s RENAME TO %s" % (table_name,new_table_name))
        connection.commit()
        connection.close()
    except Exception as error:
        if verbose:
            sys.stderr.write("'%s' table could not be renamed in database '%s'. Error message: '%s' \n" % 
                             (table_name, database, error))
        return False
    else:
        if verbose:
            sys.stdout.write("Table '%s' successfully renamed to '%s' in database '%s': \n" % (table_name,new_table_name,database))
        return True
    
def removeTableFromSQLite(table_name, database, verbose=True):
    """
    This function removes a table from a database by permanently deleting it.
    If either the database or the table does not exist (or another error occurs), then it will return False and give an error message.
    """
    
    try:
        #with the format below, if the database does not already exist, it is not created.
        connection=sqlite3.connect('file:%s?mode=rw' % database, uri=True)
        cursor=connection.cursor()
        cursor.execute("DROP TABLE %s;" % table_name)
        connection.commit()
        connection.close()
    except Exception as error:
        if verbose:
            sys.stderr.write("'%s' table could not be deleted from database '%s'. Error message: '%s' \n" % 
                             (table_name, database, error))
            return False
    else:
        if verbose:
            sys.stdout.write("Table '%s' successfully removed from database '%s': \n" % (table_name, database))
        return True

def listTablesFromSQLite(database, verbose = True):
    """
    This function returns a list of the names of all the tables included in a database.
    If the database does not exist or there are no tables in the database, then it returns False and gives an error message.
    If verbose is set to True, it also returns an error message in case there are no tables in the database.
    """
    
    tableQuery = "select * from sqlite_master"
    
    try:
        #with the format below, if the database does not already exist, it is not created.
        connection=sqlite3.connect('file:%s?mode=rw' % database, uri=True)
        cursor=connection.cursor()
        cursor.execute(tableQuery)
        tableList = cursor.fetchall()
        tables_names=list(i[1] for i in tableList)
        connection.close()
        
    except Exception as error:
        if verbose:
            sys.stderr.write("Listing the tables was not possible in database '%s'. Error message: '%s' \n" % 
                             (database, error))
        return False
    else:
        if not tables_names:     #this means if the tables_names list is empty
            if verbose:
                sys.stderr.write("There are not tables in database %s \n" % database)
            return (tables_names)
        else:
            return (tables_names)
    
def listColumnsFromSQLite(table_name, database, verbose = True):
    """
    This function returns a list of the columns contained in a table of a database.
    If the database does not exist or there are no tables in the database, then it returns an empty list.
    If verbose is set to True, it also returns an error message in case there are no tables in the database.
    """
    
    listcolumns = "PRAGMA table_info(%s)" % table_name
    
    try:
        #with the format below, if the database does not already exist, it is not created.
        connection=sqlite3.connect('file:%s?mode=rw' % database, uri=True)
        cursor=connection.cursor()
        cursor.execute(listcolumns)
        columnsList = cursor.fetchall()
        columns_names=list(i[1] for i in columnsList)
        connection.close()
       
    except Exception as error:
        if verbose:
            sys.stderr.write("Listing the columns of table '%s' was not possible in database '%s'. Error message: '%s' \n" % 
                             (table_name, database, error))
        return False
    else:
        return (columns_names)

def getColumnFromSQLite(column_name, table_name, database, verbose = True):
    """
    This function retrieves the entries of a specified column of a table of a database into a python list.
    If the column or the table specified does not exist, it returns False.
    """
    
    database_df = loadTableFromSQLite(table_name, database, verbose=False)
    
    if column_name in list(database_df.columns):
        column_content = list(database_df[column_name])
        return (column_content)
    else:
        if verbose:
            sys.stderr.write("Column '%s' does not exist in database '%s'. Are you sure the database exists? \n" % (column_name,database))
        return False    
    
def filterFromSQLiteTable(table_name, condition, database, verbose =True):
    """
    This function filters out a part of a table of a database into a pandas dataframe, based on one or multiple conditions.
    It executes the SQL 'SELECT' statement: 'SELECT * FROM table_name WHERE condition'.
    
    The argument 'condition' should have this format, for example: 
        condition = "ID == 'SAUSA300_RS04190'"
    or if we want to filter based on multiple columns:
        condition = "ID == 'SAUSA300_RS04190' AND aaRNA == 'Results downloaded'"
        (This will return the rows of a table in which the ID column contains 'SAUSA300_RS04190' AND the aaRNA column contains 'Results downloaded'.
        
    If we have a list of elements and we want to filter out all the rows in which a column contains any of these elements, 
    then we should use function filterRowsFromSQLiteTable instead. 
    """
    
    filtering = 'SELECT * FROM %s WHERE %s' % (table_name, condition)
    
    try:
        #with the format below, if the database does not already exist, it is not created.
        connection=sqlite3.connect('file:%s?mode=rw' % database, uri=True)
        cursor=connection.cursor()
        dataframe=pd.read_sql(filtering, connection)
        connection.commit()
        connection.close()
        
    except Exception as error:
        if verbose:
            sys.stderr.write("Filtering was not possible in database '%s'. Error message: '%s' \n" % 
                             (database, error))
        return False
    else:
        if len(dataframe) == 0:
            if verbose:
                sys.stderr.write("Specified value(s) do(es) not exist in database '%s' \n" % 
                                 (database))
            return False
        else:
            return (dataframe)

def filterRowsFromSQLiteTable(table_name, column, rows_of_interest, database, verbose=True): 
    """
    This function filters the table of interest in the database for a specified column and retains the rows which 
    contain the values in the list rows_of_interest e.g. the list returned by the function checkAvailableFilesFromDatabase

    For example:
    rows_of_interest = ['x','y','z']
    column = 'letter'
    
    would generate the sql command: filtering = "SELECT * FROM pyrbdome_analysis WHERE letter = 'x' OR letter = 'y' OR letter = 'z'" 
    
    Which would retain all rows where the column "letter" contained either "x", "y" or "z". 
    """
    
    statement = '' 
    # iterates through items in list to generate statement "column=value OR *n"
    for i in rows_of_interest:
        statement += (str(column)+" = '"+str(i)+"' OR ")
    # remove last OR
    statement = statement[:-4]
    # compile whole filtering statement 
    filtering = 'SELECT * FROM %s WHERE %s' % (table_name,statement)

    try:
        #with the format below, if the database does not already exist, it is not created.
        connection=sqlite3.connect('file:%s?mode=rw' % database, uri=True)
        cursor=connection.cursor()
        dataframe=pd.read_sql(filtering, connection)
        connection.commit()
        connection.close()
        
    except Exception as error:
        if verbose:
            sys.stderr.write("Filtering was not possible in database '%s'. Error message: '%s' \n" % 
                             (database, error))
        return False
    else:
        if len(dataframe) == 0:
            if verbose:
                sys.stderr.write("Values %s do not exist in column '%s' in database '%s' \n" % 
                                 (rows_of_interest, column, database))
            return False
        else:
            return (dataframe)
        
def filterCellFromSQLiteTableFromPDB(column_to_return,table_name,database,pdb_id,verbose):
    """
    This function filters a table of the database based on the 'pdb_id' provided (argument pdb_id).
    It returns a list containing the cell(s) from a desired column that match(es) the specified pdb_id.
    The function is quite specific for the pyRBDome pipeline, as it returns the entries of the desired column 
    based on filtering the 'pdb_id' column specifically.
    
    For example, if we want to get the ID of a protein that has pdb_id '7TCH', we would use as arguments:
    column = "ID"
    pdb_id = "7TCH"
    
    *** Note: if the pdb_id is found in multiple rows, then a list of all the desired column's entries from all these rows is returned.
    Like ['Q02878', 'P18124', 'P46777'] could be the output when using arguments: column = "ID", pdb_id = "6IP5"***
    """
    
    filtering = 'SELECT %s FROM %s WHERE pdb_id == "%s"' % (column_to_return, table_name, pdb_id)
    
    try:
        #with the format below, if the database does not already exist, it is not created.
        connection=sqlite3.connect('file:%s?mode=rw' % database, uri=True)
        cursor=connection.cursor()
        dataframe=pd.read_sql(filtering, connection)
        cells=list(dataframe[column_to_return])
        connection.commit()
        connection.close()
        
    except Exception as error:
        if verbose:
            sys.stderr.write("Filtering was not possible in database '%s'. Error message: '%s' \n" % 
                             (database, error))
        return False
    else:
        if len(cells) == 0:
            if verbose:
                sys.stderr.write("Value %s does not exist in column pdb_id in database '%s' \n" % 
                                 (pdb_id, database))
            return False
        else:
            return (cells)
        
def addTablewithID(table_name, database, verbose = True):
    """
    This function creates a table containing one column named 'ID'. This is because we cannot
    create a completely empty table, without any columns.
    """
    connection=sqlite3.connect(database)
    cursor=connection.cursor()
    
    addtable = "CREATE TABLE IF NOT EXISTS %s (ID varchar(32));" % table_name
    
    tables_list = listTablesFromSQLite(database, verbose = False)
    if table_name not in tables_list:
        try:
            cursor.execute(addtable)
        except Exception as error:
            if verbose:
                sys.stderr.write("'%s' table could not be added to database '%s'. Error message: '%s' \n" % 
                                 (table_name, database, error))
            return False
        else:
            if verbose:
                sys.stdout.write("'%s' table successfully created in database '%s' \n" % 
                                (table_name, database))
            return True
    else:
        if verbose:
            sys.stdout.write("Table '%s' already exists in database '%s'! No new table added. \n" % 
                             (table_name, database))
    connection.commit()
    connection.close() 

def addEmptyColumn(column_name, table_name, database, verbose = True):
    """
    This function adds an empty column to a table in an SQLite database.
    If the specified table does not exist, it creates it and adds the desired column.
    If neither the table nor the database exist, it creates the database, the table and the desired column.
    """
    
    addcolumn = "ALTER TABLE %s ADD COLUMN %s varchar(32)" %(table_name, column_name)
    addcolumnandtable = "CREATE TABLE IF NOT EXISTS %s (%s varchar(32));" %(table_name, column_name)
    
    connection=sqlite3.connect(database)
    cursor=connection.cursor()
    
    tables_list = listTablesFromSQLite(database, verbose = False)
    if table_name not in tables_list:
        try:
            cursor.execute(addcolumnandtable)
        except Exception as error:
            if verbose:
                sys.stderr.write("'%s' column could not be added to table '%s' of database '%s'. Error message: '%s' \n" % 
                                 (column_name, table_name, database, error))
            return False
        else:
            if verbose:
                sys.stdout.write("'%s' column successfully created in table '%s' of database '%s' \n" % 
                                (column_name, table_name, database))
            return True
    else:
        columns_list = listColumnsFromSQLite(table_name, database)
        if column_name not in columns_list:
            try:
                cursor.execute(addcolumn)
            except Exception as error:
                if verbose:
                    sys.stderr.write("'%s' column could not be added in table '%s' of database '%s'. Error message: '%s' \n" %
                                     (column_name, table_name, database, error))
                return False
            else:
                if verbose:
                    sys.stdout.write("'%s' column successfully created in table '%s' of database '%s' \n" % 
                                     (column_name, table_name, database))
                return True
        else:
            if verbose:
                sys.stdout.write("Column '%s' already exists in table '%s' of database '%s'! No new column added. \n" % (column_name, table_name, database))

    connection.commit()
    connection.close()
    
def addEmptyColumn_default_ID(column_name, table_name, database, verbose = True):
    """
    This function adds an empty column to a table in a database.
    If the specified table does not exist, it creates a table containing a column named 'ID', 
    to which it then adds the desired column.
    """
    
    addTablewithID(table_name, database, verbose = False)
    
    connection=sqlite3.connect(database)
    cursor=connection.cursor()
    
    addcolumn = "ALTER TABLE %s ADD COLUMN %s varchar(32)" %(table_name, column_name)
    
    columns_list = listColumnsFromSQLite(table_name, database)
    if column_name not in columns_list:
        try:
            cursor.execute(addcolumn)
        except Exception as error:
            if verbose:
                sys.stderr.write("'%s' column could not be added in table '%s' of database '%s'. Error message: '%s' \n" % 
                             (column_name, table_name, database, error))
            return False
        else:
            if verbose:
                sys.stdout.write("You have successfully made a new column, named '%s' in table '%s' of database '%s'! \n" % (column_name, table_name, database))
            return True
    else:
        if verbose:
            sys.stdout.write("Column '%s' already exists in table '%s' of database '%s'! No new column added. \n" % (column_name, table_name, database))
            
    connection.commit()
    connection.close()
    
def updateColumn(table_name, set_new_value, set_known_value, database, verbose = True):
    """
    This function updates a table of a database based on a filter of other column(s).
    *** Careful with the quotes, the comma and the 'AND' in the arguments: The value should be in quotes, 
    comma should be used for updating multiple columns, 'AND' should be used for updating entries based 
    on filters of multiple columns! Check below example format for arguments***
    
    For example, in a table that contains the columns 'ID', 'pdb_id', 'function', 'localisation', we may 
    want to update the column 'function' with the value 'cold-shock protein' for an entry that contains 
    the value '2LXJ' in the 'pdb_id' column. For this, the arguments should be like this:
    set_new_value = "function == 'cold-shock protein'"
    set_known_value = "pdb_id == '2LXJ'"
    
    We can also update multiple columns, for example columns 'function' and 'localisation':
    set_new_value = "function == 'cold-shock protein' , localisation == 'transmembrane'"
    set_known_value = "ID == '2LXJ'"
    
    We can also update based on filters of multiple columns. For example if we have multiple entries with 
    pdb_id '2LXJ', we may want to update column 'function' only for the entry that has 'SAUSA300_RS04190' 
    in the 'ID' column. The arguments should be formatted like this:
    set_new_value = "function == 'cold-shock protein'"
    set_known_value = 'pdb_id == "2LXJ" AND ID == "SAUSA300_RS04190"'
    """
    
    addvalue = '''UPDATE %s
                SET %s  
                WHERE %s;''' % (table_name, set_new_value, set_known_value)
    
    filterFromSQLiteTable(table_name, set_known_value, database, verbose = False)

    try:
        #with the format below, if the database does not already exist, it is not created.
        connection=sqlite3.connect('file:%s?mode=rw' % database, uri=True)
        cursor=connection.cursor()
        cursor.execute(addvalue)
        connection.commit()
        connection.close()
    except Exception as error:
        if verbose:
            sys.stderr.write("Column(s) NOT updated in database '%s'. Error message: '%s'. \n" % (database, error))
        return False
    else:
        if type(filterFromSQLiteTable(table_name, set_known_value, database, verbose = False)) != pd.core.frame.DataFrame: # this is basically if the filtering returns 'False' instead of returning a dataframe
            if verbose:
                sys.stderr.write("Column(s) NOT updated in database '%s'. The known value(s) was(/were) not found or there was another filtering exception. \n" % (database))
            return False
        else:
            if verbose:
                sys.stdout.write("Column(s) updated for %s in database '%s'. \n" % (set_known_value, database))
            return True
    
def checkAvailableFilesFromDatabase(column_to_return, column_to_check, table_name, database, verbose = True):
    """
    This function finds the part of the table for which a specified column (column_to_check) contains 
    'Yes' or 'Results downloaded' and returns the values of another column we specify (column_to_return).
    This function is quite specific for the pyRBDome pipeline, as it is used in many notebooks to extract 
    information from the main table of the database, on which algorithms have run:
    For example, if we have data for aaRNA, the aaRNA column will contain 'Results downloaded'. 
    If we have a swissmodel structure downloaded, then the swiss_model_downloaded column will contain 'Yes'.
    
    If we want to extract all the IDs for the proteins for which we have downloaded aaRNA results, the arguments will be:
    column_to_return = 'ID'
    column_to_check = 'aaRNA'
    
    For other purposes, the function can be modified to look for other values, instead of 'Yes' or 'Results downloaded'.
    """
    
    database_df = loadTableFromSQLite(table_name, database, verbose = False)
    
    if column_to_check in list(database_df.columns):
        dataframe_downloaded = database_df[(database_df[column_to_check] == 'Yes') | (database_df[column_to_check] == 'Results downloaded')]
        files_downloaded = list(dataframe_downloaded[column_to_return])
    else:
        #if the column does not exist (e.g. because an algorithm hasn't run yet), it will return nothing (empty list), as there is no available files
        files_downloaded = list()
        if verbose:
            sys.stderr.write("Column '%s' does not exist in database '%s'. \n" % (column_to_check, database))
    return ([i for i in files_downloaded if i]) 

    ### Here, we did return ([i for i in files_NOT_downloaded if i]) in order to get rid of any 'None' values.For example, in case we run "checkUnavailableFilesFromDatabase('pdb_id', 'BindUP', 'pyrbdome_analysis', 'pyrbdome.db')" and for some entries there is nothing in the pdb_id column - if there is no pdb downloaded - these entries will be 'None', so we want them removed.

def checkUnavailableFilesFromDatabase(column_to_return, column_to_check, table_name, database, verbose = True):
    """
    This function finds the part of the table for which a specified column (column_to_check) contains NEITHER 
    'Yes' NOR 'Results downloaded' and returns the values of another column we specify (column_to_return).
    
    This function is quite specific for the pyRBDome pipeline, as it is used in many notebooks to extract 
    information from the main table of the database, on which algorithms have run:
    For example, if we have data for aaRNA, the aaRNA column will contain 'Results downloaded'. 
    If we have a swissmodel structure downloaded, then the swiss_model_downloaded column will contain 'Yes'.
    
    If we want to extract all the IDs for the proteins for which we DON'T have downloaded aaRNA results, the arguments will be:
    column_to_return = 'ID'
    column_to_check = 'aaRNA'
    
    For other purposes, the function can be modified to exclude other values in specified columns, instead of 'Yes' or 'Results downloaded'.
    """
    
    database_df = loadTableFromSQLite(table_name, database, verbose=False)
    
    if column_to_check in list(database_df.columns):
        dataframe_downloaded = database_df[(database_df[column_to_check] != 'Yes') & (database_df[column_to_check] != 'Results downloaded')]
        files_NOT_downloaded = list(dataframe_downloaded[column_to_return])
    else:
        #if the column does not exist (e.g. because an algorithm hasn't run yet), it will return all the entries of the database
        files_NOT_downloaded = list(database_df[column_to_return])
        if verbose:
            sys.stderr.write("Column '%s' does not exist in database '%s' \n" % (column_to_check,database))
    return ([i for i in files_NOT_downloaded if i])

    ### Here, we did return ([i for i in files_NOT_downloaded if i]) in order to get rid of any 'None' values.For example, in case we run "checkUnavailableFilesFromDatabase('pdb_id', 'BindUP', 'pyrbdome_analysis', 'pyrbdome.db')" and for some entries there is nothing in the pdb_id column - if there is no pdb downloaded - these entries will be 'None', so we want them removed.

def checkFailedFromDatabase(column_to_return, column_to_check, table_name, database, verbose = True):
    """
    This function finds the part of the table for which a specified column (column_to_check) contains 
    the word 'ERROR' (in capital characters!) and returns the values of another column we specify (column_to_return).
    
    This function is quite specific for the pyRBDome pipeline, as it is used in many notebooks to extract 
    information from the main table of the database, on which algorithms have run:
    For example, if there has been something wrong while running aaRNA, the aaRNA column will contain a message including the word 'ERROR'.
    
    If we want to extract all the IDs for the proteins for which aaRNA has been run and failed, the arguments will be:
    column_to_return = 'ID'
    column_to_check = 'aaRNA'
    
    For other purposes, the function can be modified to look for other values in specified columns, instead of 'ERROR'.
    """
    
    database_df = loadTableFromSQLite(table_name, database, verbose=False)
    
    failed_entries = list()
    
    if column_to_check in list(database_df.columns):
        for i in database_df.index:
            if database_df[column_to_check][i] != None and 'ERROR' in database_df[column_to_check][i]:
                failed_entry = database_df[column_to_return][i]
                # the if statement below is in case e.g. we ask for pdb_id to be returned and it is null
                if failed_entry != None: 
                    failed_entries.append(failed_entry)
    else:
        failed_entries = list()
        if verbose:
            sys.stderr.write("Column '%s' does not exist in database '%s' \n" % (column_to_check,database))
    return ([i for i in failed_entries if i])
    ### Here, we did return ([i for i in files_NOT_downloaded if i]) in order to get rid of any 'None' values.For example, in case we run "checkUnavailableFilesFromDatabase('pdb_id', 'BindUP', 'pyrbdome_analysis', 'pyrbdome.db')" and for some entries there is nothing in the pdb_id column - if there is no pdb downloaded - these entries will be 'None', so we want them removed.

def createNewTableFromMain(database,keepColumns,input_table,new_table,newColumns):
    """
    This function takes the main database table as input and slices the columns ID,
    Number_of_peptides, chains and pdb_id. These are used to create a new table in SQLite
    which consists only of these columns. New columns are then added from the list of 
    "newColumns" that the user provides - e.g.:
    
    database = "pyrbdome.db"
    keepColumns = "ID,Number_of_peptides,chains,pdb_id"
    input_table = "pyrbdome_analysis"
    new_table = "RNA_bindingsite_analyses"
    newColumns = ("aaRNA","BindUP","FTMap","STP","RNABindRPlus")
    """
    
    #download columns in keepColumns from database table
    connection=sqlite3.connect(database)
    cursor=connection.cursor()
    dataframe = pd.read_sql('SELECT %s FROM %s' % (keepColumns, input_table), con=connection)
    connection.close()
    
    #create new table in SQLite database
    dataframeToSQLite_append_only_new(dataframe, new_table, database)
    
    #add new columns
    for column in newColumns:
        addEmptyColumn(column, new_table, database, verbose = True)

    return True
    
def removeColumn(column_name, table_name, database, verbose=True):
    """
    This function deletes a column from a table in an SQLite database.
    If the specified table does not exist, it creates it and adds the desired column.
    If neither the table nor the database exist, it creates the database, the table and the desired column.
    """
        
    connection=sqlite3.connect(database)
    cursor=connection.cursor()
    
    delcolumn = "ALTER TABLE %s DROP COLUMN %s" % (table_name, column_name)
    
    tables_list = listTablesFromSQLite(database,verbose=False)
    if table_name not in tables_list:
        print("YES")
        print(tables_list)
        if verbose:
            sys.stderr.write("'%s' column could not be removed from table '%s' of database '%s'.\n" % \
                             (column_name, table_name, database))
        return False
    else:
        columns_list = listColumnsFromSQLite(table_name, database)
        if column_name in columns_list:
            try:
                cursor.execute(delcolumn)
            except Exception as error:
                if verbose:
                    sys.stderr.write("'%s' column could not be removed from table '%s' in database '%s'. Error message: '%s' \n" % \
                                     (column_name, table_name, database, error))
                return False
            else:
                if verbose:
                    sys.stdout.write("'%s' column successfully removed from table '%s' in database '%s'.\n" % \
                                     (column_name, table_name, database))
                return True
        else:
            if verbose:
                sys.stdout.write("Column '%s' does not exists in table '%s' in database '%s'! No columns were deleted.\n" % \
                                 (column_name, table_name, database))

    connection.commit()
    connection.close()
