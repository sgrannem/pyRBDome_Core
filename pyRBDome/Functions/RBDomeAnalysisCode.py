#!/usr/bin/python

__author__		= ["Liang-Cui Chu","Niki Christopoulou","Shichao Wang","Sander Granneman"]
__copyright__	= "Copyright 2025"
__version__		= "0.3.0"
__credits__		= ["Liang-Cui Chu","Niki Christopoulou","Shichao Wang","Sander Granneman"]
__maintainer__	= "Sander Granneman"
__email__		= "Sander.Granneman@ed.ac.uk"
__status__		= "beta"

##################################################################################
#
#	RBDome Functions
#
#
#	Copyright (c) Sander Granneman 2025
#
#	Permission is hereby granted, free of charge, to any person obtaining a copy
#	of this software and associated documentation files (the "Software"), to deal
#	in the Software without restriction, including without limitation the rights
#	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#	copies of the Software, and to permit persons to whom the Software is
#	furnished to do so, subject to the following conditions:
#
#	The above copyright notice and this permission notice shall be included in
#	all copies or substantial portions of the Software.
#
#	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#	THE SOFTWARE.
#
##################################################################################


import os
import sys
import time
import pandas as pd
import numpy as np
import re
import regex
import glob
import math
import Bio
import itertools
from Bio import SeqIO
from pyopenms import *
from biopandas.pdb import PandasPdb
from collections import defaultdict
from pyRBDome.Classes.PeptideAnalyser import *
from pyRBDome.Classes.PDBAnalyser import *
from pyRBDome.Classes.BindUP import *
from pyRBDome.Classes.FTMap import *
from pyRBDome.Functions.RBDomeSQL import *

pd.set_option('mode.chained_assignment', None)

def convertToNumeric(dataframe,columns):
    """ This function converts any numbers formatted as strings in the table columns
    to number/floates. Any strings in that column will be replaced by NaN's. """
    for col in columns:
        dataframe.loc[:,col] = pd.to_numeric(dataframe.loc[:,col],errors='coerce')
    return dataframe

def getPDBandChainsFromDataFile(data):
    """ Grabbing the pdb and chain information from a data table """
    pdbsandchains = sorted(set(zip(*map(data.get,['pdb_id','chains']))))
    return pdbsandchains

def getMotifs(sequence,kmer_length=3):
    """ returns a dictionary of motifs from a DNA/RNA or proteins sequence
    of specific k-mer length. """

    motif_list = list()
    length_seq = len(sequence) - kmer_length
    for i in range (0,length_seq+1):
        motif = sequence[i:(i+kmer_length)]
        motif_list.append(motif)

    return motif_list

def numpy_overlap(numpy_array,start,end,overlap=1,returnall=False):
    # much improved overlap function which uses numpy numerical calculations. Three to four times faster than the old ones
    """ Checks whether there is overlap between read coordinates and chromosomal coordinates but uses numpy arrays
    and numpy numerical conversions. 
    Is much faster than regular iterations over lists. The read or cluster can be larger than the
    gene itself. 
    The result of the overlap function can return zero, which means that two positions in the intervals are identical.
    This is why the overlap value should always be subtracted by 1.

    Possibilities:
       start	   start				   end						   start				  end	   start				   end
     ---|			|----------------------|							|----------------------|		|----------------------|
        |---					<-overlap->								<-overlap->						<-------overlap-------->
       end	(overlap=0)	   f0					f1		 f0						   f1				 f0							   f1
                            |-------------------|		  |------------------------|				  |----------------------------|

    f0					  f1							   f0					   f1	   f0					  f1
     |----------------------|								|----------------------|		|----------------------|
                  <-overlap->								 <-overlap->					 <-------overlap-------->
                 start				 end	 start					  end				 start						  end
                   |------------------|		  |------------------------|				  |----------------------------|

    Won't work for the following:
     f0			   f1			 start			end			start		   end			  f0			f1
     |--------------| <-overlap-> |--------------|			  |-------------| <-overlap-> |-------------|

    """
    overlap -= 1
    seqoverlap = (end - numpy_array['f0'] >= overlap) & (numpy_array['f1'] - start >= overlap)	
    # determines whether there is overlap between a read and gene chromosomal coordinates. Returns a bool index.

    if returnall:
        return list(numpy_array[seqoverlap])
    else:
        return list(numpy_array[seqoverlap]['f2'])
    # if overlap was found, then the gene names are stored in the results list, otherwise it returns an empty list

def calcMotifZscores(motifs,control):
    """Calculates the Z-scores for each k-mer sequence"""
    motif_list = list()
    a = set(motifs.keys())
    b = set(control.keys())
    z_scores = defaultdict(float)
    motif_list = a | b
    total_motif_count = sum(motifs.values())
    control_motif_count = sum(control.values())
    for i in motif_list:
        p1 = float(motifs[i]) / total_motif_count
        p2 = float(control[i]) / control_motif_count
        try:
            if p1 or p2:
                Z_score = math.sqrt(total_motif_count) * (p1-p2) / math.sqrt(p1*(1-p1) + p2*(1-p2))
                z_scores[i] = Z_score
        except ValueError:
            sys.stderr.write("\nValueError! Can not calculate a Z-score for the %s motif. The number of motifs (%s) is higher than the total number of sequences (%s)\n" % (motifs,motif_list[i],total_motif_count))
            pass

    return z_scores

def getPeptideCoordinatesFromPDB(pdb_id,chain,peptides,out_dir=".",pdb_dir='pdb_files'):
    pdb_file = "%s.pdb" % pdb_id
    pdb_file_path = os.path.abspath(os.path.join(pdb_dir,pdb_file))
    if not os.path.exists(pdb_file_path):
        sys.stderr.write("ERROR! The pdb file %s could not be found in the %s directory\n" \
                         % (pdb_file,pdb_dir))
        return False

    if not os.path.exists(out_dir):
        sys.stderr.write("ERROR! Directory %s does not exist!\n" % out_dir)
        return False
    else:
        outfile_name = "%s_%s_peptides.pdb" % (pdb_id,chain)
        outfile_name_path = os.path.join(out_dir,outfile_name)
        analyser = PeptidePDBAnalyser()
        analyser.loadPDBFile(pdb_file_path,records=['ATOM'],remove_ligands=True)
        peptide_coordinates = analyser.getPeptideCoordinatesFromPDBFile(peptides)
        analyser.writePDBFile(outfile_name_path,records=['ATOM'],df=peptide_coordinates)
        return True

def mapPeptides(pdb_id,chains,peptides,pdb_dir='pdb_dir'):
    analyser = PeptidePDBAnalyser()
    pdb_file = "%s/%s.pdb" % (pdb_dir,pdb_id)
    try:
        analyser.loadPDBFile(pdb_file,records=['ATOM'],remove_ligands=True)
    except:
        sys.stderr.write("ERORR! Could not find the pdb file %s!\n" % pdb_file)
        return None
    else:
        analyser.mapPeptidesToStructures(pdb_id,chains,peptides)
        return analyser.mapped_peptides

def extractMoleculesFromFTMapPDB(pdb_id,
							     chains,
							     out_dir=".",
							     database=True,
                                 database_table='pyrbdome_analysis',
                                 database_name='pyrbdome.db',
                                 database_verbose=True):
    """ Parses the data for the docked molecules in the FTMap PDB file and
    prints them to a new pdb file ending with 'FTMap_docked.pdb'. The 'out_dir'
    argument specifies the directory where the pyRBDome files are stored. This
    directory should contain all the folders with analysis results for individual
    pdb files."""

    if not os.path.exists(out_dir):
        sys.stderr.write("The directory %s does not exist\n" % out_dir)
        if database:
            updateColumn(database_table,'FTMap == "Missing data"', 'pdb_id == "%s"' % pdb_id, database_name, verbose = database_verbose)
        return False

    ### Reading the FTMap pdb file:
    pdb_file = "%s_%s_FTMap.pdb" % (pdb_id,chains)
    pdb_file_path = os.path.join(out_dir,pdb_file)
    
    if not os.path.exists(pdb_file_path):
        sys.stderr.write("ERROR! The processed pdb file %s does not exist\n" % pdb_file)
        #Populate the column FTMap of the sqlite database
        if database:
            updateColumn(database_table,'FTMap == "Missing data"', 'pdb_id == "%s"' % pdb_id, database_name, verbose = database_verbose)
        return False

    if not os.path.exists(pdb_file_path):
        sys.stderr.write("ERROR! The processed pdb file %s does not exist\n" % pdb_file)
        if database:
                updateColumn(database_table,'FTMap == "Missing data"', 'pdb_id == "%s"' % pdb_id, database_name, verbose = database_verbose)
        return False

    ### Setting the names of the output file:
    outfile = "%s_%s_FTMap_docked.pdb" % (pdb_id,chains)
    outfile_path = os.path.join(out_dir,outfile)

    if os.path.exists(outfile_path):
        sys.stdout.write("File %s already exists\n" % outfile)
        return False
    else:
        ### getting docked molecules and writing them to an output file:
        dockedmolecules = extractLinesBetweenStrings(pdb_file_path,'HEADER crosscluster','REMARK Residue')
        output_file = open(outfile_path,"w")
        for line in dockedmolecules:
            output_file.write(line)
        output_file.close()

        if os.path.exists(outfile_path):
            sys.stdout.write("File %s created\n" % outfile)
            return True

def calculateFTMapMinDistance(pdb_id,chains,
                              pdb_dir='pdb_files',
                              out_dir=".",
                              binary=True,
                              mindistance=4.2):
    """ Calculates the minimal distance between reference points and each residue in the
    pdb dataframe."""
    
    original_pdb = PDBAnalyser()
    original_pdb.loadPDBFile(os.path.join(pdb_dir,("%s.pdb" % pdb_id)),records=['ATOM'])
    
    reference_file = "%s/%s_%s_FTMap_docked.pdb"%(out_dir,pdb_id,chains)
    output_file = "%s/%s_%s_FTMap_distances.pdb"%(out_dir,pdb_id,chains)

    if os.path.exists(reference_file):
        references = PandasPdb().read_pdb(reference_file)
        referencepoints = references.df['ATOM'].loc[:,['x_coord','y_coord','z_coord']].values
        tree = spatial.cKDTree(referencepoints)
        
        ### Now calculating the distances:
        for residue_number in sorted(list(set(original_pdb.pdb_df.loc[:,'residue_number']))):
            residue_data = original_pdb.pdb_df.loc[original_pdb.pdb_df['residue_number'] == residue_number]
            coordinates = np.array(residue_data.loc[:,['x_coord','y_coord','z_coord']].values)
            shortest_distance = min(tree.query(np.array(coordinates))[0])
            
            ### Writing the new min distance to the pdb b-factor column:
            
            if binary:
                if shortest_distance <= mindistance:
                    original_pdb.pdb_df.loc[original_pdb.pdb_df['residue_number'] == residue_number,'b_factor'] = 1
                else:
                    original_pdb.pdb_df.loc[original_pdb.pdb_df['residue_number'] == residue_number,'b_factor'] = 0
            else:
                original_pdb.pdb_df.loc[original_pdb.pdb_df['residue_number'] == residue_number,'b_factor'] = shortest_distance
                                              
        ### Writing the output file:
        original_pdb.writePDBFile(output_file,records=['ATOM']) 
    else:
        sys.stderr.write("%s/%s_%s_FTMap_docked.pdb was not found. \n" %(out_dir,pdb_id,chains))
        
def processFTMapData(pdb_id,chains,
                    max_score=4,
                    out_dir=".",
                    pdb_dir = '.',
                    database=True,
                    database_table='pyrbdome_analysis',
                    database_name='pyrbdome.db',
                    database_verbose=True,
                    overwrite=True):
    """ Puts the FTMap results from the FTMapdistances.txt file in the
    new FTMapdistances.pdb file. The 'out_dir'argument specifies
    the directory where the pyRBDome files are stored. This directory
    should contain all the folders with analysis results for individual
    pdb files. NOTE!!! If the output file already exists, it will NOT
    overwrite it!! """

    ### Checking if the FTMapdistances.pdb output file exist:
    outfile = "%s_%s_FTMapdistances.pdb" % (pdb_id,chains)
    outfile_path = os.path.join(out_dir,outfile)
    if os.path.exists(outfile_path) and not overwrite:
        sys.stderr.write("The %s file already exists\n" % outfile_path)
        #Populate the column FTMap of the sqlite database
        if database:
            updateColumn(database_table,'FTMap == "Results processed"', 'pdb_id == "%s"' % pdb_id, database_name, verbose = database_verbose)
        return False

    ### Checking if the FTMapdistances.txt data file exists:
    txt_file = "%s_%s_FTMapdistances.txt" % (pdb_id,chains)
    txt_file_path = os.path.join(out_dir,txt_file)
    if not os.path.exists(txt_file_path):
        sys.stderr.write("ERROR! The %s file does not exist\n" % txt_file_path)
        #Populate the column FTMap of the sqlite database
        if database:
            updateColumn(database_table,'FTMap == "Missing data"', 'pdb_id == "%s"' % pdb_id, database_name, verbose = database_verbose)
        return False

    ### Using the original pdb file as input file:
    pdb_file = "%s.pdb" % pdb_id
    pdb_file_path = os.path.join(pdb_dir,pdb_file)
    if not os.path.exists(pdb_file_path):
        sys.stderr.write("The pdb_file %s does not exist in the %s directory\n" % (pdb_file_path,pdb_dir))
        #Populate the column FTMap of the sqlite database
        if database:
            updateColumn(database_table,'FTMap == "Missing data"', 'pdb_id == "%s"' % pdb_id, database_name, verbose = database_verbose)
        return False

    ### If everything is now OK, start reading the data:
    data = pd.read_csv(txt_file_path,sep="\t",header=0,index_col=0)
    data['chain'] = data.index.str[-1:]

    ppdb = PandasPdb()
    ppdb.read_pdb(pdb_file_path)

    ### Setting the b-values to 0:
    ppdb.df['ATOM'].loc[:,'b_factor'] = 0.0
    ppdb.df['ATOM'].loc[:,'blank_4'] = ''
    
    
    ### Putting the FTMapdistance.txt results in the b_factor column:
    try:
        for i in data[data['distance'] <= max_score].index:
            chain,residue_number,score = data.loc[i,['chain','residue_number','distance']]
            locations = ppdb.df['ATOM'][(ppdb.df['ATOM']['chain_id'] == chain) & \
                                        (ppdb.df['ATOM']['residue_number'] == residue_number)].index

            ppdb.df['ATOM'].loc[locations,'b_factor'] = 1.0
    except:
        print(data)
        #Populate the column FTMap of the sqlite database
        if database:
            updateColumn(database_table,'FTMap == "Missing data"', 'pdb_id == "%s"' % pdb_id, database_name, verbose = database_verbose)
        return False
    else:
        ppdb.to_pdb(outfile_path,records=['ATOM'],gz=False,append_newline=True)
        sys.stdout.write("File %s created! \n" % outfile_path)
        #Populate the column FTMap of the sqlite database
        if database:
            updateColumn(database_table,'FTMap == "Results processed"', 'pdb_id == "%s"' % pdb_id, database_name, verbose = database_verbose)
        return True
    
def collapsePeptidesByShortestDistances(dataframe,numeric_columns=[]): 
    """
    Collapses the peptides for each pdb ID by the shortest distances (Å)
    to a ligand-binding site.
    
    We need to give as input:
    - 'dataframe': a dataframe that contains:
        - the pdb_id in the index column
        - the peptides (column named 'Peptide') for each pdb_id 
        - the distances of each peptide from the closest ligand-binding site predicted by the prediction algorithms
          (the column names could be 'PST_PRNA_distances', 'BindUP_distances', etc)
    - 'numeric_columns': a list of column names that contain the distance of each peptide from a ligand-binding site
      (the column names could be 'PST_PRNA_distances', 'BindUP_distances', etc). These columns should contain numeric values. 
      To convert the values into numeric, the function 'convertToNumeric', found in RBDomeAnalysisCode.py, can be used.
      
    If a peptide has multiple entries in the provided dataframe, because it has been mapped to multiple chains of the pdb,
    then this function picks the entry (row of the dataframe) that has the shortest distance from a ligand-binding site, 
    based on the average of the distances calculated by all the algorithms that have run for the peptide.
    
    It returns a dataframe in which each peptide has only one entry, the one with the smallest mean distance. 
    
    """

    if not numeric_columns:
        sys.stderr.write("ERROR! Please provide a list of column names!\n")
        return None
    
    collapsed_df = pd.DataFrame(columns=dataframe.columns)

    for a,b in sorted(set(zip(*map(dataframe.get,['ID','Peptide'])))):
        selection = dataframe[(dataframe['ID'] == a) & (dataframe['Peptide'] == b)]
        # Here, we create a number index for the selected dataframe, so that we can filter using iloc
        selection_with_number_index = selection.reset_index()
        mean_values = [] # this list will hold the average distance (mean of the values of the numeric columns)
        for i in selection_with_number_index.index:
            # First, add all the distances for each row. The np.nansum does not mind the NaN values.
            sum_distance = np.nansum(selection_with_number_index.iloc[i][numeric_columns])
            # Then, count how many values of the ones used for the sum_distance were not a NaN. Will use to get the mean.
            sum_values = sum(~np.isnan(list(selection_with_number_index.iloc[i][numeric_columns])))
            mean_distance = sum_distance/sum_values
            mean_values.append(mean_distance)
        # Create a new column that will hold the mean distance for each peptide
        selection_with_number_index['mean_numeric'] = mean_values
        # Select the row of the dataframe that has the lowest mean distance
        smallest = selection_with_number_index.nsmallest(1,'mean_numeric')
        # Bring back the initial index and delete the mean distance column - we only needed it to choose one peptide
        # the 'drop=True' argument prevents a 'level_0' column from being created.
        smallest = smallest.reset_index(drop=True).drop(columns='mean_numeric')
        # Add the selected row for the peptide in the dataframe collapsed_df, which will be returned in the end.
        collapsed_df = pd.concat([collapsed_df,smallest])
    return collapsed_df

def collapseAminoAcidsByShortestDistances(dataframe,numeric_columns=[]): 
    if not numeric_columns:
        sys.stderr.write("ERROR! Please provide a list of column names!\n")
        return None
    
    collapsed_df = pd.DataFrame(columns=dataframe.columns)

    for a,b in sorted(set(zip(*map(dataframe.get,['ID','Peptide_original'])))):
        selection = dataframe[(dataframe['ID'] == a) & (dataframe['Peptide_original'] == b)]
        # Here, we create a number index for the selected dataframe, so that we can filter using iloc
        selection_with_number_index = selection.reset_index()
        mean_values = [] # this list will hold the average distance (mean of the values of the numeric columns)
        for i in selection_with_number_index.index:
            # First, add all the distances for each row. The np.nansum does not mind the NaN values.
            sum_distance = np.nansum(selection_with_number_index.iloc[i][numeric_columns])
            # Then, count how many values of the ones used for the sum_distance were not a NaN. Will use to get the mean.
            sum_values = sum(~np.isnan(list(selection_with_number_index.iloc[i][numeric_columns])))
            mean_distance = sum_distance/sum_values
            mean_values.append(mean_distance)
        # Create a new column that will hold the mean distance for each peptide
        selection_with_number_index['mean_numeric'] = mean_values
        # Select the row of the dataframe that has the lowest mean distance
        smallest = selection_with_number_index.nsmallest(1,'mean_numeric')
        # Bring back the initial index and delete the mean distance column - we only needed it to choose one peptide
        # the 'drop=True' argument prevents a 'level_0' column from being created.
        smallest = smallest.reset_index(drop=True).drop(columns='mean_numeric')
        # Add the selected row for the peptide in the dataframe collapsed_df, which will be returned in the end.
        collapsed_df = pd.concat([collapsed_df,smallest])
    return collapsed_df

def sortIndexWithNumbersAndString(indexlist):
    """ takes a list containing indices that have numbers and characters
    and sorts them numerically """

    splitlist = list()
    for index in indexlist:
        pos = str()
        chain = str()
        for char in index:
            if char == "-" or char.isnumeric():
                pos += char
            if char.isalpha():
                chain += char
        pos = int(pos)
        splitlist.append([pos,char])

    sortedlist = sorted(splitlist)
    sortedlist = ["".join([str(i[0]),i[1]]) for i in sortedlist]
    return(sortedlist)

def parseRNABindRPlusResults(file_name):
    """ Takes the results of an RNABindRPlus analysis and puts the
    results in a dictionary """

    results_file = defaultdict(lambda: defaultdict(list))
    with open(file_name,"r") as infile:
        for line in infile:
            if line.startswith(">"):
                fld = line.strip()[1:].split("_")
                if len(fld) > 2:   ### in case a pdb_id has more than one '_':
                    pdb_id = "_".join(fld[:2])
                    chains = fld[-1]
                else:
                    pdb_id,chains = fld
            elif re.search("Prediction from RNABindRPlus:",line):
                scores = line.split("\t")[-1].strip()
                results_file[pdb_id][chains] = np.array([float(i) for i in scores.split(",")])

    return results_file

def processRNABindRPlusResults(pdb_id,
							   chains,
							   results_dict,
							   pdb_dir='pdb_files',
							   out_dir=".",
							   database=True,
                               database_table='pyrbdome_analysis',
                               database_name='pyrbdome.db',
                               database_verbose=True):
    """ Takes the results of an RNABindRPlus analysis and adds the results to the
    b-factor column of the pdb file. It expects a pdb ID, chains associated with
    this pdb file and a dictionary containing the RNABindRPlus results for that
    pdb_id. The latter is a dictionary with the RNABindRPlus results for each
    individual chain. The 'out_dir'argument specifies
    the directory where the pyRBDome files are stored. This directory
    should contain all the folders with analysis results for individual
    pdb files."""

    ### Using the original pdb file as input file:
    pdb_file = "%s.pdb" % pdb_id
    pdb_file_path = os.path.join(pdb_dir,pdb_file)
    if not os.path.exists(pdb_file_path):
        sys.stderr.write("The pdb_file %s does not exist in the %s directory\n" % (pdb_file_path,pdb_dir))
        #Populate the column RNABindRPlus of the sqlite database
        if database:
            updateColumn(database_table,'RNABindRPlus == "Missing data"', 'pdb_id == "%s"' % pdb_id, database_name, verbose = database_verbose)
        return False

    ### Making the output files:
    directory = "%s_%s" % (pdb_id,chains)
    directory_path = os.path.join(out_dir,directory)
    if not os.path.exists(directory_path):
        sys.stderr.write("The directory %s does not exist\n" % directory_path)
        return False
    output_file = "%s_%s_RNABindRPlus.pdb" % (pdb_id,chains)
    output_file_path = os.path.join(directory_path,output_file)

    ### Reading the original pdb file:
    ppdb = PandasPdb()
    ppdb.read_pdb(pdb_file_path)

    ### Setting the b-factor column to zero:
    ppdb.df['ATOM'].loc[:,'b_factor'] = 0.0
    ppdb.df['ATOM'].loc[:,'blank_4'] = ''

    ### Setting the index to the residue_numbers:
    #ppdb.df['ATOM'].index = ppdb.df['ATOM'].loc[:'residue_numbers']

    ### Now adding the RNABindRPlus results, one chain at a time:
    for chain in results_dict:
        rnabindr_results = results_dict[chain]
        residue_numbers = sorted(set(ppdb.df['ATOM'][ppdb.df['ATOM']['chain_id'] == chain]['residue_number']))
        for i in range(len(rnabindr_results)):
            value = rnabindr_results[i]
            if value > 0.0:
                residue_number = residue_numbers[i]
                locations = ppdb.df['ATOM'][(ppdb.df['ATOM']['chain_id'] == chain) & \
                                            (ppdb.df['ATOM']['residue_number'] == residue_number)].index

                ppdb.df['ATOM'].loc[locations,'b_factor'] = value


    ### Writing to the output file:
    ppdb.to_pdb(output_file_path,records=["ATOM"],gz=False,append_newline=True)
    
    #check if outfile now exists
    if os.path.exists(output_file_path):
        sys.stdout.write("%s file created\n" % output_file)
        #Populate the column RNABindRPlus of the sqlite database
        if database:
            updateColumn(database_table,'RNABindRPlus == "Results processed"', 'pdb_id == "%s"' % pdb_id, database_name, verbose = database_verbose)
        return True

def parseDisoRDPbindResults(file_name):
    """ Takes the results of an DisoRDPbind analysis and puts the
    results in a dictionary """

    results_file = defaultdict(lambda: defaultdict(list))
    with open(file_name,"r") as infile:
        for line in infile:
            if line.startswith(">"):
                fld = line.strip()[1:].split("_")
                if len(fld) > 2:   ### in case a pdb_id has more than one '_':
                    pdb_id = "_".join(fld[:2])
                    chains = fld[-1]
                else:
                    pdb_id,chains = fld
            elif re.search("RNA-binding residues:",line):
                scores = line.split(":")[-1].strip()
                results_file[pdb_id][chains] = np.array([float(i) for i in list(str(scores))])
    return results_file

def processDisoRDPbindResults(pdb_id,
							  chains,
							  results_dict,
							  pdb_dir='pdb_files',
							  out_dir=".",
							  database=True,
							  database_table='pyrbdome_analysis',
							  database_name='pyrbdome.db',
							  database_verbose=True):
    """ Takes the results of an DisoRDPbind analysis and adds the results to the
    b-factor column of the pdb file. It expects a pdb ID, chains associated with
    this pdb file and a dictionary containing the DisoRDPbind results for that
    pdb_id. The latter is a dictionary with the DisoRDPbind results for each
    individual chain. The 'out_dir'argument specifies
    the directory where the pyRBDome files are stored. This directory
    should contain all the folders with analysis results for individual
    pdb files."""

    ### Using the original pdb file as input file:
    pdb_file = "%s.pdb" % pdb_id
    pdb_file_path = os.path.join(pdb_dir,pdb_file)
    if not os.path.exists(pdb_file_path):
        sys.stderr.write("The pdb_file %s does not exist in the %s directory\n" % (pdb_file_path,pdb_dir))
        #Populate the column DisoRDPbind of the sqlite database
        if database:
            updateColumn(database_table, 'DisoRDPbind == "Missing data"', 'pdb_id == "%s"' % pdb_id, database_name, verbose = database_verbose)
        return False

    ### Making the output files:
    directory = "%s_%s" % (pdb_id,chains)
    directory_path = os.path.join(out_dir,directory)
    if not os.path.exists(directory_path):
        sys.stderr.write("The directory %s does not exist\n" % directory_path)
        return False
    output_file = "%s_%s_DisoRDPbind.pdb" % (pdb_id,chains)
    output_file_path = os.path.join(directory_path,output_file)

    ### Reading the original pdb file:
    ppdb = PandasPdb()
    ppdb.read_pdb(pdb_file_path)

    ### Setting the b-factor column to zero:
    ppdb.df['ATOM'].loc[:,'b_factor'] = 0.0
    ppdb.df['ATOM'].loc[:,'blank_4'] = ''

    ### Setting the index to the residue_numbers:
    #ppdb.df['ATOM'].index = ppdb.df['ATOM'].loc[:'residue_numbers']

    ### Now adding the DisoRDPbind results, one chain at a time:
    for chain in results_dict:
        DisoRDPbind_results = results_dict[chain]
        residue_numbers = sorted(set(ppdb.df['ATOM'][ppdb.df['ATOM']['chain_id'] == chain]['residue_number']))
        for i in range(len(DisoRDPbind_results)):
            value = DisoRDPbind_results[i]
            if value > 0.0:
                residue_number = residue_numbers[i]
                locations = ppdb.df['ATOM'][(ppdb.df['ATOM']['chain_id'] == chain) & \
                                            (ppdb.df['ATOM']['residue_number'] == residue_number)].index

                ppdb.df['ATOM'].loc[locations,'b_factor'] = value


    ### Writing to the output file:
    ppdb.to_pdb(output_file_path,records=["ATOM"],gz=False,append_newline=True)

    #check if outfile now exists
    if os.path.exists(output_file_path):
        sys.stdout.write("%s file created\n" % output_file)
        #Populate the column DisoRDPbind of the sqlite database
        if database:
            updateColumn(database_table, 'DisoRDPbind == "Results processed"', 'pdb_id == "%s"' % pdb_id, database_name, verbose = database_verbose)
        return True

def extractLinesBetweenStrings(infile,start,end):
    """ You can use this method to extract lines from your input file (infile)
    that begin with a string indicated by 'start' and end before string indicated
    by 'end'. It returns a list of the lines that it found in yoru file."""
    lines = list()
    copy = False
    for line in open(infile,'r').readlines():
        if line.startswith(start):
            lines.append("REMARK\n")
            lines.append(line)
            copy = True
            continue
        elif line.startswith(end):
            copy = False
            continue
        elif copy:
            lines.append(line)
    return lines

def parseAnalysisResults(file):
    """ Parses the data analyses results of peptides"""
    data = pd.read_csv(file,sep='\t',header=0,index_col=None)
    data.replace(np.nan,'',inplace=True)
    data.loc[:,'Peptide'] = data.loc[:,'Peptide'].str.replace('\\s*\\([^\\)]+\\)','')
    return data

def doOpenMSProteinProteaseDigestion(proteinseq,enzyme='Trypsin',minlength=5,maxlength=40):
    """ Does an in silico protease digestion of your protein. You need to provide the
    protein sequence as a single string. The minlength and maxlength variables allows
    you to set the minimum and maximum lengths of the peptides in the results."""

    proteinseq = AASequence.fromString(proteinseq)
    dig = ProteaseDigestion()
    try:
        dig.setEnzyme(enzyme)
    except:
        names = list()
        ProteaseDB().getAllNames(names)
        sys.stderr.write('ERROR! enzyme %s does not exist in the database!\nPlease choose from the following:\n%s') \
        % (enzyme,','.join(names))
        return None

    results = list()
    output  = list()
    dig.digest(proteinseq, results)
    for i in results:
        peptide = i.toString()
        if (len(peptide) >= minlength) & (len(peptide) <= maxlength):
            try:
                output.append(peptide.decode())
            except AttributeError:
                output.append(peptide)
    return output

def inSilicoProteaseDigestionProteinSequence(sequence,
                                             enzyme = 'Trypsin',
                                             shortest = 6,
                                             longest = 37,
                                             remove_duplicates = True):
    """ A function that performs in silico digestion of protein sequences. You can digest
    the sequences with Trypsin or Lys-C. 
    This function returns a pandas dataframe containing the protein name, pdb_id, chains
    and the peptide sequence. It requires an input file that has a column with pdb_ids 
    labeled 'pdb_id' and a column with 'chains'. It also requires that you have the 
    corresponding pdb files in a pdb_files directory. """

    digestedpeptides = doOpenMSProteinProteaseDigestion(sequence,
                                                        enzyme = enzyme,
                                                        minlength = shortest,
                                                        maxlength = longest)
    
    if remove_duplicates:
        digestedpeptides = list(set(digestedpeptides))  ### to remove any repeated peptides
        
    return digestedpeptides

def inSilicoProteaseDigestionPDBstructures(pdb_id,
                                           chains,
                                           enzyme = 'Trypsin',
                                           shortest = 6,
                                           longest = 37,
                                           pdb_dir = 'pdb_files',
                                           remove_duplicates = True):
    """ A function that performs in silico digestion of protein sequences present
    in crystal structure pdb files. You can digest the sequences with Trypsin or Lys-C. 
    Some pdb files will have information on homomeric proteins so you can remove any 
    duplicate peptides in the list by setting the variable 'remove_duplicates' to True.
    This function returns a pandas dataframe containing the protein name, pdb_id, chains
    and the peptide sequence. It requires an input file that has a column with pdb_ids 
    labeled 'pdb_id' and a column with 'chains'. It also requires that you have the 
    corresponding pdb files in a pdb_files directory. """

    results_array = list()
    
    try:
        directory = pdb_dir
        pdb_file = "%s.pdb" % pdb_id
        pdb_file_path = "%s/%s" % (pdb_dir,pdb_file)
    except FileNotFoundError:
        sys.stderr.write("Could not find a pdb file for pdb_id %s\n" % pdb_id)
        return None
    else:
        reader = PDBAnalyser()
        reader.loadPDBFile(pdb_file_path)
        try:
            for chain in chains:
                sequence = reader.getPDBProteinSequence(chain)
                try:
                    digestedpeptides = doOpenMSProteinProteaseDigestion(sequence,
                                                                        enzyme = enzyme,
                                                                        minlength = shortest,
                                                                        maxlength = longest)
                except: ### When there is a problem with the sequence because of strange characters:
                    sys.stderr.write("Could not process the sequence %s from the following pdb %s and chain %s\n" \
                                     % (pdb_id,sequence,chain))
                    return None
                else:
                    digestedpeptides = list(set(digestedpeptides))  ### to remove any repeated peptides
                    for peptide in digestedpeptides:
                        results_array.append((pdb_id,chains,peptide))

        except:
            sys.stderr.write("Could not process the chains %s from the following pdb %s.\n" \
                                     % (chains, pdb_id))
            return None
        else:
            dataset = pd.DataFrame(results_array,columns=['pdb_id','chains','Peptide'])
            if remove_duplicates:
                dataset.drop_duplicates(subset=['Peptide'],keep='first',inplace=True)
            return dataset

def countAminoAcidHits(peptides,minaahits=1,minlength=4,maxlength=40):
    """ Looks in a list of peptides returnd by the analyses and asks how many have
    amino acids that are upper case and therefore above the threshold """

    resultstable = pd.DataFrame(0,\
                            index=np.arange(minlength,maxlength+1),\
                            columns=['YES','NO'])

    aaseqs = list()
    for i in peptides:
        if "_" not in i and i != None:
            try:
                peptide = i.split('-')[1]
                aaseqs.append(peptide)
            except:
                pass

    if aaseqs:
        for peptide in aaseqs:
            peptidelength = len(peptide)
            if (peptidelength >= minlength) & (peptidelength <= maxlength):
                count = 0
                for j in peptide:
                    if j.isupper():
                        count += 1
                if count >= minaahits:
                    resultstable.loc[peptidelength,'YES'] += 1
                else:
                    resultstable.loc[peptidelength,'NO'] += 1
    return resultstable

def countHitsWithinDistance(values,threshold=0.0):
    """ Counts the number of time a peptide is w"""
    total_values = int()
    count = int()
    for i in values:
        try:
            i = float(i)
            total_values += 1
            if i <= threshold:
                count += 1
        except:
            #sys.stderr.write("ERROR! value %s is not a float\n" % i)
            continue
    return total_values,count

def reportAminoAcidHits(peptides,minlength=4,maxlength=40):
    """ Looks in a list of peptides returnd by the analyses and asks how many have
    amino acids that are upper case and therefore above the threshold and returns
    a list stating whether a peptide has an upper-case character or not """

    results = list()
    aaseqs = list()
    for i in peptides:
        if "_" not in i and i != None:
            try:
                peptide = i.split('-')[1]
                aaseqs.append(peptide)
            except:
                results.append("unknown")
        else:
            results.append("unknown")

    if aaseqs:
        for peptide in aaseqs:
            peptidelength = len(peptide)
            if (peptidelength >= minlength) & (peptidelength <= maxlength):
                count = 0
                for j in peptide:
                    if j.isupper():
                        count += 1
                results.append(count)
            elif peptidelength < minlength:
                results.append("too_short")
            elif peptidelength > maxlength:
                results.append("too_long")

    assert len(results) == len(peptides)
    return results

def countAminoAcidFrequencies(peptides,minaahits=1,minlength=4,maxlength=40):
    """ Looks in a list of peptides returnd by the analyses and asks how many have
    amino acids that are upper case and therefore above the threshold """

    resultstable = pd.DataFrame(0,\
                            index=np.arange(minlength,maxlength+1),\
                            columns=['YES','NO'])

    aaseqs = list()
    for i in peptides:
        if "_" not in i and i != None:
            try:
                peptide = i.split('-')[1]
                aaseqs.append(peptide)
            except:
                pass

    if aaseqs:
        for peptide in aaseqs:
            peptidelength = len(peptide)
            if (peptidelength >= minlength) & (peptidelength <= maxlength):
                count = 0
                for j in peptide:
                    if j.isupper():
                        count += 1
                if count >= minaahits:
                    resultstable.loc[peptidelength,'YES'] += 1
                else:
                    resultstable.loc[peptidelength,'NO'] += 1
    return resultstable

def findPeptideHits(peptide,pdb_df,threshold=0.18):
    """Looks for a peptide in the protein sequence of the pdb_file and asks if there are any
    amino acids in the sequence that have a high scores in the analyses of the peptides.
    It returns the peptide sequence it found in lower case and puts the amino acids
    with high scores in uppercase. NOTE that the peptide sequence provided
    by the mass spectrometer will not always be the same as what is present in
    the protein sequence. So the peptideSearch function will allow some mismatches!"""

    sequence,start,end = getPeptideInfo(peptide)
    if sequence:
        aminoacids = list(sequence)
        peptide_coordinates = pdb_df.loc[start:end,:]
        count = 0
        for i in peptide_coordinates.index:
            try:
                if peptide_coordinates.loc[i,'b_factor'] >= threshold:
                    aminoacids[count] = aminoacids[count].upper()
            except IndexError:
                sys.stderr.write("ERROR! Could not figure out where the binding site is in the peptide %s\n" % "".join(peptide))
                return 'no_data'
            count += 1

        newpeptide = "".join(aminoacids)
        return "%s_%s_%s" % (start,newpeptide,end)
    else:
        return peptide

def convertPeptideSeqToFeatureString(peptide):
    peptide = peptide.upper()
    featuredict = {"A":"L","G":"L","I":"L","L":"L","P":"L","V":"L",
                   "F":"R","W":"R","Y":"R",
                   "D":"C","E":"C",
                   "R":"B","H":"B","K":"B",
                   "C":"S","M":"S",
                   "S":"H","T":"H",
                   "N":"M","Q":"M"
                  }

    newseq = [featuredict[i] for i in peptide]
    return ''.join(newseq)

def mergeMultiplesDataFrames(dataframes):
    """ Merges dataframes using the first dataframe in a list as a base"""

    base_df = dataframes[0]
    for i in dataframes[1:]:
        base_df = base_df.merge(i,how='outer',sort=False)
    return base_df
    
def runPST_PRNADistanceAnalyses(pdb_id,
                                chains,
                                peptide_data,
                                pdb_dir=".",
                                minscore=1):
    """ Runs the PST_PRNA analysis on output files in a directory with a specific
    pdb id and length and returns the results in a pandas dataframe. The 'out_dir'
    argument specifies the directory where the pyRBDome files are stored. This
    directory should contain all the folders with analysis results for individual
    pdb files."""

    column_title = "PST_PRNA_distances"

    ### Checking if the PST_PRNA pdb output file exist:
    pdb_file = "%s_%s_PST_PRNA.pdb" % (pdb_id,chains)
    pdb_file_path = os.path.join(pdb_dir,pdb_file)

    pdb = PeptidePDBAnalyser()
    pdb.mapped_peptides = peptide_data

    if not os.path.exists(pdb_file_path):
        sys.stderr.write("ERROR! The pdb file %s does not exist!\n" % pdb_file_path)
        pdb.mapped_peptides[column_title] = 'no_data'
    else:
        pdb.loadPDBFile(pdb_file_path,records=['ATOM'],remove_ligands=True)
        referencepoints = pdb.grabReferencePoints(score=minscore)
        if referencepoints.size > 0:
            pdb.measureAllPeptideDistancesToReferences(referencepoints,column_title)
        else:
            sys.stderr.write("ERROR! No reference points for %s and %s!\n" % (pdb_id,chains))
            pdb.mapped_peptides[column_title] = 'no_data'

    return pdb.mapped_peptides
    
def runPST_PRNABindingSiteAnalyses(pdb_id,
                                   chains,
                                   peptide_data,
                                   pdb_dir=".",
                                   minscore=1):
    """ Runs the PST-PRNA analysis on output files in a directory with a specific
    pdb id and length and returns the results in a pandas dataframe. The 'out_dir'
    argument specifies the directory where the pyRBDome files are stored. This
    directory should contain all the folders with analysis results for individual
    pdb files."""

    column_title = "PST_PRNA_results"

    ### Checking if the PST_PRNA pdb file exists:
    pdb_file = "%s_%s_PST_PRNA.pdb" % (pdb_id,chains)
    pdb_file_path = os.path.join(pdb_dir,pdb_file)

    pdb = PeptidePDBAnalyser()
    pdb.mapped_peptides = peptide_data

    if not os.path.exists(pdb_file_path):
        sys.stderr.write("ERROR! The pdb file %s does not exist!\n" % pdb_file_path)
        pdb.mapped_peptides[column_title] = 'no_data'
    else:
        ### Doing the analyses:
        pdb.loadPDBFile(pdb_file_path,records=['ATOM'],remove_ligands=True)
        pdb.highlightPeptideAminoAcidsByScore(score=minscore,header=column_title)
        
    return pdb.mapped_peptides

def processSTPData(pdb_id,
			       chains,
			       out_dir="."):
    """ Process STP prediction results. Normalize b-factors to 0-1 for pyMOL interpretation."""
    
    ### Checking if the STP pdb file exists:
    pdb_file = "%s_%s_STP.pdb" % (pdb_id,chains)
    pdb_file_path = os.path.join(out_dir,pdb_file)
    
    ### Checking if the STP pdb normalized output file exist:
    outfile = "%s_%s_STP_norm.pdb" % (pdb_id,chains)
    outfile_path = os.path.join(out_dir,outfile)

    if not os.path.exists(pdb_file_path):
        sys.stderr.write("ERROR! The pdb file %s does not exist!\n" % pdb_file_path)
    else:

        if os.path.exists(outfile_path):
            sys.stdout.write("The STP normalized file %s already exists\n" % outfile_path)
        else:
            pdb = PeptidePDBAnalyser()
            pdb.loadPDBFile(pdb_file_path,records=['ATOM'],remove_ligands=True)
            ### Need to normalise the b-factor values to values between 0-1 for STP analyses:
            pdb.df['ATOM'] = pdb.normaliseBfactorValues()
            pdb.to_pdb(outfile_path,
                records=["ATOM"],
                gz=False,
                append_newline=True)

    if os.path.exists(outfile_path):
        sys.stderr.write("File %s created\n" % outfile)

def runSTPBindingSiteAnalyses(pdb_id,
						      chains,
						      peptide_data,
						      pdb_dir=".",
                              minscore=0.8):
    """ Runs the STP analysis on output files in a directory with a specific
    pdb id and length and returns the results in a pandas dataframe. The 'out_dir'
    argument specifies the directory where the pyRBDome files are stored. This
    directory should contain all the folders with analysis results for individual
    pdb files."""

    column_title = "STP_results"

    ### Checking if the STP pdb file exists:
    pdb_file = "%s_%s_STP.pdb" % (pdb_id,chains)
    pdb_file_path = os.path.join(pdb_dir,pdb_file)

    pdb = PeptidePDBAnalyser()
    pdb.mapped_peptides = peptide_data

    if not os.path.exists(pdb_file_path):
        sys.stderr.write("ERROR! The pdb file %s does not exist!\n" % pdb_file_path)
        pdb.mapped_peptides[column_title] = 'no_data'
    
    else:
        ### Doing the analyses:
        pdb.loadPDBFile(pdb_file_path,records=['ATOM'],remove_ligands=True)
        ### Need to normalise the b-factor values to values between 0-1 for STP analyses:
        pdb.pdb_df = pdb.normaliseBfactorValues()
        pdb.highlightPeptideAminoAcidsByScore(score=minscore,header=column_title)


    return pdb.mapped_peptides

def runSTPDistanceAnalyses(pdb_id,
						   chains,
						   peptide_data,
						   pdb_dir=".",
						   minscore=0.8):
    """ Runs the STP analysis on output files in a directory with a specific
    pdb id and length and returns the results in a pandas dataframe. The 'out_dir'
    argument specifies the directory where the pyRBDome files are stored. This
    directory should contain all the folders with analysis results for individual
    pdb files."""

    column_title = "STP_distances"

    ### Checking if the STP pdb file exists:
    pdb_file = "%s_%s_STP.pdb" % (pdb_id,chains)
    pdb_file_path = os.path.join(pdb_dir,pdb_file)

    pdb = PeptidePDBAnalyser()
    pdb.mapped_peptides = peptide_data

    if not os.path.exists(pdb_file_path):
        sys.stderr.write("ERROR! The pdb file %s does not exist!\n" % pdb_file_path)
        pdb.mapped_peptides[column_title] = 'no_data'
    else:
        pdb.loadPDBFile(pdb_file_path,records=['ATOM'],remove_ligands=True)
        pdb.pdb_df = pdb.normaliseBfactorValues()
        referencepoints = pdb.grabReferencePoints(score=minscore)
        if referencepoints.size > 0:
            pdb.measureAllPeptideDistancesToReferences(referencepoints,column_title)
        else:
            sys.stderr.write("ERROR! No reference points for %s and %s!\n" % (pdb_id,chains))
            pdb.mapped_peptides[column_title] = "no_data"
            
    return pdb.mapped_peptides

def runBindUP(pdb_file,
              chain,
              verbose,
              overwrite,
              headless,
              out_dir,
              database=True,
              database_name='pyrbdome_full.db',
              database_table=None):
    """ Code used to run the BindUP analysis over multiple processors """
    analyser = BindUPanalysis(verbose=verbose,
                                overwrite=overwrite,
                                headless=headless,
                                out_dir=out_dir,
                                database=database,
                                database_name=database_name,
                                database_table=database_table)
    analyser.connectToServer()
    analyser.submitJob(pdb_file=pdb_file,chain=chain)
    analyser.downloadResults()
    analyser.closeBrowser()

def runBindUPBindingSiteAnalyses(pdb_id,
								 chains,
								 peptide_data,
								 pdb_dir=".",
								 minscore=1):
    """ Runs the BindUP analysis on output files in a directory with a specific
    pdb id and length and returns the results in a pandas dataframe. The 'out_dir'
    argument specifies the directory where the pyRBDome files are stored. This
    directory should contain all the folders with analysis results for individual
    pdb files."""

    column_title = "BindUP_results"

    ### Checking if the BindUP pdb file exists:
    pdb_file = "%s_%s_BindUP.pdb" % (pdb_id,chains)
    pdb_file_path = os.path.join(pdb_dir,pdb_file)

    pdb = PeptidePDBAnalyser()
    pdb.mapped_peptides = peptide_data

    if not os.path.exists(pdb_file_path):
        sys.stderr.write("ERROR! The pdb file %s does not exist!\n" % pdb_file_path)
        pdb.mapped_peptides[column_title] = 'no_data'
    else:
        ### Doing the analyses:
        pdb.loadPDBFile(pdb_file_path,records=['ATOM'],remove_ligands=True)
        pdb.highlightPeptideAminoAcidsByScore(score=1,header=column_title)

    return pdb.mapped_peptides

def runBindUPDistanceAnalyses(pdb_id,
							  chains,
							  peptide_data,
							  pdb_dir=".",
                              minscore=1):
    """ Runs the BindUP analysis on output files in a directory with a specific
    pdb id and length and returns the results in a pandas dataframe. The 'out_dir'
    argument specifies the directory where the pyRBDome files are stored. This
    directory should contain all the folders with analysis results for individual
    pdb files."""

    column_title = "BindUP_distances"

    ### Checking if the BindUP pdb file exists:
    pdb_file = "%s_%s_BindUP.pdb" % (pdb_id,chains)
    pdb_file_path = os.path.join(pdb_dir,pdb_file)

    pdb = PeptidePDBAnalyser()
    pdb.mapped_peptides = peptide_data

    if not os.path.exists(pdb_file_path):
        sys.stderr.write("ERROR! The pdb file %s does not exist!\n" % pdb_file_path)
        pdb.mapped_peptides[column_title] = 'no_data'
    else:
        pdb.loadPDBFile(pdb_file_path,records=['ATOM'],remove_ligands=True)
        referencepoints = pdb.grabReferencePoints(score=1)
        if referencepoints.size > 0:
            pdb.measureAllPeptideDistancesToReferences(referencepoints,column_title)
        else:
            sys.stderr.write("ERROR! No reference points for %s and %s!\n" % (pdb_id,chains))
            pdb.mapped_peptides[column_title] = "no_data"

    return pdb.mapped_peptides
    
def runFTMapAnalyses(pdb_file,
                     job_title,
                     chains,
                     username,
                     password,
                     pdb_dir,
                     database=True,
                     database_table='',
                     database_name=''):
    """ function that runs the FTMap analyses """
    
    output_file = "%s/%s_FTMap.pdb" % (pdb_dir,job_title)
    pdb_id = os.path.basename(os.path.splitext(pdb_file)[0])
    
    ftmap = FTMapAnalysis(headless=True,
                          verbose=True,
                          overwrite=False,
                          out_dir=pdb_dir,
                          database=True,
                          database_table=database_table,
                          database_name=database_name,
                          database_verbose=False)

    ftmap.connectToServer(username,password)
    ftmap.submitJob(pdb_file,job_title,chains)

    while not ftmap.jobIsFinished():
        time.sleep(20)
    else:
        if ftmap.job_finished:
            ftmap.downloadResults()
            if ftmap.job_error:
                ### In case the server throws an error, make a screenshot of the page!
                html = ftmap.browser.page_source
                with open('%s/FTMap_screenshot_%s_%s.html' % (pdb_dir,pdb_id,job_title),'w') as outfile:
                    outfile.write(html)
                return (pdb_id,chains)
            ftmap.closeBrowser()

def runFTMapDistanceAnalyses(pdb_id,chains,peptide_data,pdb_dir=".",mindistance=4.2):
    """ Runs the FTMap analysis on output files in a directory with a specific
    pdb id and length and returns the results in a pandas dataframe. The 'out_dir'
    argument specifies the directory where the pyRBDome files are stored. This
    directory should contain all the folders with analysis results for individual
    pdb files."""

    column_title = "FTMap_distances"
    
    ### Checking if the RNABindRPlus pdb file exists:
    pdb_file = "%s_%s_FTMap_distances.pdb" % (pdb_id,chains)
    pdb_file_path = os.path.join(pdb_dir,pdb_file)

    pdb = PeptidePDBAnalyser()
    pdb.mapped_peptides = peptide_data

    if not os.path.exists(pdb_file_path):
        sys.stderr.write("ERROR! The pdb file %s does not exist!\n" % pdb_file_path)
        pdb.mapped_peptides[column_title] = 'no_data'
    else:
        pdb.loadPDBFile(pdb_file_path,records=['ATOM'],remove_ligands=True)
        ### The b-factor column contains distances in Angstrom to docked molecules.
        ### Hence the smaller the better. As a default thresshold we use 4.2 Anstrom.
        ### which is about hydrogen-binding distances away from the docked molecule.
        pdb.pdb_df.loc[pdb.pdb_df['b_factor'] <= mindistance,'b_factor'] = 1.0
        pdb.pdb_df.loc[pdb.pdb_df['b_factor'] > mindistance,'b_factor'] = 0.0
        referencepoints = pdb.grabReferencePoints(score=1.0)
        if referencepoints.size > 0:
            pdb.measureAllPeptideDistancesToReferences(referencepoints,column_title)
        else:
            sys.stderr.write("ERROR! No reference points for %s and %s!\n" % (pdb_id,chains))
            pdb.mapped_peptides[column_title] = "no_data"

    return pdb.mapped_peptides

def runFTMapBindingSiteAnalyses(pdb_id,chains,peptide_data,pdb_dir=".",mindistance=4.2):
    """ Runs the FTMap analysis on output files in a directory with a specific
    pdb id and length and returns the results in a pandas dataframe. The 'out_dir'
    argument specifies the directory where the pyRBDome files are stored. This
    directory should contain all the folders with analysis results for individual
    pdb files."""

    column_title = "FTMap_results"
    
    ### Checking if the RNABindRPlus pdb file exists:
    pdb_file = "%s_%s_FTMap_distances.pdb" % (pdb_id,chains)
    pdb_file_path = os.path.join(pdb_dir,pdb_file)

    pdb = PeptidePDBAnalyser()
    pdb.mapped_peptides = peptide_data

    if not os.path.exists(pdb_file_path):
        sys.stderr.write("ERROR! The pdb file %s does not exist!\n" % pdb_file_path)
        pdb.mapped_peptides[column_title] = 'no_data'
    else:
        pdb.loadPDBFile(pdb_file_path,records=['ATOM'],remove_ligands=True)
        ### The b-factor column contains distances in Angstrom to docked molecules.
        ### Hence the smaller the better. As a default thresshold we use 4.2 Anstrom.
        ### which is about hydrogen-binding distances away from the docked molecule.
        pdb.pdb_df.loc[pdb.pdb_df['b_factor'] <= mindistance,'b_factor'] = 1.0
        pdb.pdb_df.loc[pdb.pdb_df['b_factor'] > mindistance,'b_factor'] = 0.0
        referencepoints = pdb.grabReferencePoints(score=1.0)
        if referencepoints.size > 0:
            pdb.highlightPeptideAminoAcidsByScore(score=1.0,header=column_title)
        else:
            sys.stderr.write("ERROR! No reference points for %s and %s!\n" % (pdb_id,chains))
            pdb.mapped_peptides[column_title] = "no_data"

    return pdb.mapped_peptides

def runRNABindRPlusBindingSiteAnalyses(pdb_id,chains,peptide_data,pdb_dir=".",minscore=1):
    """ Runs the FTMap analysis on output files in a directory with a specific
    pdb id and length and returns the results in a pandas dataframe. The 'out_dir'
    argument specifies the directory where the pyRBDome files are stored. This
    directory should contain all the folders with analysis results for individual
    pdb files."""

    column_title = "RNABindRPlus_results"

    ### Checking if the RNABindRPlus pdb file exists:
    pdb_file = "%s_%s_RNABindRPlus.pdb" % (pdb_id,chains)
    pdb_file_path = os.path.join(pdb_dir,pdb_file)

    pdb = PeptidePDBAnalyser()
    pdb.mapped_peptides = peptide_data

    if not os.path.exists(pdb_file_path):
        sys.stderr.write("ERROR! The pdb file %s does not exist!\n" % pdb_file_path)
        pdb.mapped_peptides[column_title] = 'no_data'
    else:
        ### Doing the analyses:
        pdb.loadPDBFile(pdb_file_path,records=['ATOM'],remove_ligands=True)
        pdb.highlightPeptideAminoAcidsByScore(score=minscore,header=column_title)

    return pdb.mapped_peptides

def runRNABindRPlusDistanceAnalyses(pdb_id,chains,peptide_data,pdb_dir=".",minscore=1):
    """ Runs the RNABindRPlus analysis on output files in a directory with a specific
    pdb id and length and returns the results in a pandas dataframe. The 'out_dir'
    argument specifies the directory where the pyRBDome files are stored. This
    directory should contain all the folders with analysis results for individual
    pdb files."""

    column_title = "RNABindRPlus_distances"

    ### Checking if the RNABindRPlus pdb file exists:
    pdb_file = "%s_%s_RNABindRPlus.pdb" % (pdb_id,chains)
    pdb_file_path = os.path.join(pdb_dir,pdb_file)

    pdb = PeptidePDBAnalyser()
    pdb.mapped_peptides = peptide_data

    if not os.path.exists(pdb_file_path):
        sys.stderr.write("ERROR! The pdb file %s does not exist!\n" % pdb_file_path)
        pdb.mapped_peptides[column_title] = 'no_data'
    else:
        pdb.loadPDBFile(pdb_file_path,records=['ATOM'],remove_ligands=True)
        referencepoints = pdb.grabReferencePoints(score=minscore)
        if referencepoints.size > 0:
            pdb.measureAllPeptideDistancesToReferences(referencepoints,column_title)
        else:
            sys.stderr.write("ERROR! No reference points for %s and %s!\n" % (pdb_id,chains))
            pdb.mapped_peptides[column_title] = "no_data"

    return pdb.mapped_peptides

def runDisoRDPbindDistanceAnalyses(pdb_id,chains,peptide_data,pdb_dir=".",minscore=1):
    """ Runs the DisoRDPbindDistance analysis on output files in a directory with a specific
    pdb id and length and returns the results in a pandas dataframe. The 'out_dir'
    argument specifies the directory where the pyRBDome files are stored. This
    directory should contain all the folders with analysis results for individual
    pdb files."""

    column_title = "DisoRDPbind_distances"

    ### Checking if the DisoRDPbind pdb file exist:
    pdb_file = "%s_%s_DisoRDPbind.pdb" % (pdb_id,chains)
    pdb_file_path = os.path.join(pdb_dir,pdb_file)

    pdb = PeptidePDBAnalyser()
    pdb.mapped_peptides = peptide_data

    if not os.path.exists(pdb_file_path):
        sys.stderr.write("ERROR! The pdb file %s does not exist!\n" % pdb_file_path)
        pdb.mapped_peptides[column_title] = 'no_data'
        #Populate the column DisoRDPbind of the sqlite database
        #if database:
        #    updateColumn(database_table,'DisoRDPbind == "Missing data"', 'pdb_id == "%s"' % pdb_id, database_name, verbose = database_verbose)
    else:
        pdb.loadPDBFile(pdb_file_path,records=['ATOM'],remove_ligands=True)
        referencepoints = pdb.grabReferencePoints(score=minscore)
        if referencepoints.size > 0:
            pdb.measureAllPeptideDistancesToReferences(referencepoints,column_title)
            #Populate the column DisoRDPbind of the sqlite database
            #if database:
            #    updateColumn(database_table, 'DisoRDPbind == "Distance analysis done"', 'pdb_id == "%s"' % pdb_id, database_name, verbose=database_verbose)
        else:
            sys.stderr.write("ERROR! No reference points for %s and %s!\n" % (pdb_id,chains))
            pdb.mapped_peptides[column_title] = "no_data"
            #Populate the column DisoRDPbind of the sqlite database
            #if database:
            #    updateColumn(database_table, 'DisoRDPbind == "No reference points"', 'pdb_id == "%s"' % pdb_id, database_name, verbose=database_verbose)

    return pdb.mapped_peptides

def runDisoRDPbindBindingSiteAnalyses(pdb_id,chains,peptide_data,pdb_dir=".",minscore=1):
    """ Runs the DisoRDPbind analysis on output files in a directory with a specific
    pdb id and length and returns the results in a pandas dataframe. The 'out_dir'
    argument specifies the directory where the pyRBDome files are stored. This
    directory should contain all the folders with analysis results for individual
    pdb files."""

    column_title = "DisoRDPbind_results"

    ### Checking if the DisoRDPbind pdb file exists:
    pdb_file = "%s_%s_DisoRDPbind.pdb" % (pdb_id,chains)
    pdb_file_path = os.path.join(pdb_dir,pdb_file)

    pdb = PeptidePDBAnalyser()
    pdb.mapped_peptides = peptide_data

    if not os.path.exists(pdb_file_path):
        sys.stderr.write("ERROR! The pdb file %s does not exist!\n" % pdb_file_path)
        pdb.mapped_peptides[column_title] = 'no_data'
    else:
        ### Doing the analyses:
        pdb.loadPDBFile(pdb_file_path,records=['ATOM'],remove_ligands=True)
        pdb.highlightPeptideAminoAcidsByScore(score=minscore,header=column_title)
    return pdb.mapped_peptides

def runPLIPBindingSiteAnalyses(pdb_id,peptide_data,pdb_dir=".",minscore=1):
    """ Runs the Ground truth analyses on output files in a directory with a specific
    pdb id and length and returns the results in a pandas dataframe. The 'out_dir'
    argument specifies the directory where the pyRBDome files are stored. This
    directory should contain all the folders with analysis results for individual
    pdb files."""

    column_title = "PLIP_results"

    ### Checking if the groundtruth pdb file exists:
    pdb_file = "%s.pdb" % pdb_id
    pdb_file_path = os.path.join(pdb_dir,pdb_file)

    pdb = PeptidePDBAnalyser()
    pdb.mapped_peptides = peptide_data

    if not os.path.exists(pdb_file_path):
        sys.stderr.write("ERROR! The pdb file %s does not exist!\n" % pdb_file_path)
        pdb.mapped_peptides[column_title] = 'no_data'
        
    else:
        ### Doing the analyses:
        pdb.loadPDBFile(pdb_file_path,records=['ATOM'],remove_ligands=True)
        pdb.highlightPeptideAminoAcidsByScore(score=minscore,header=column_title)
        
    return pdb.mapped_peptides
    
def runPLIPDistanceAnalyses(pdb_id,peptide_data,pdb_dir=".",minscore=1):
    """ Runs the BindUP analysis on output files in a directory with a specific
    pdb id and length and returns the results in a pandas dataframe. The 'out_dir'
    argument specifies the directory where the pyRBDome files are stored. This
    directory should contain all the folders with analysis results for individual
    pdb files."""

    column_title = "PLIP_distances"

    ### Checking if the groundtruth pdb file exists:
    pdb_file = "%s.pdb" % pdb_id
    pdb_file_path = os.path.join(pdb_dir,pdb_file)

    pdb = PeptidePDBAnalyser()
    pdb.mapped_peptides = peptide_data

    if not os.path.exists(pdb_file_path):
        sys.stderr.write("ERROR! The pdb file %s does not exist!\n" % pdb_file_path)
        pdb.mapped_peptides[column_title] = 'no_data'
        
    else:
        pdb.loadPDBFile(pdb_file_path,records=['ATOM'],remove_ligands=True)
        referencepoints = pdb.grabReferencePoints(score=minscore)
        if referencepoints.size > 0:
            pdb.measureAllPeptideDistancesToReferences(referencepoints,column_title)
        else:
            sys.stderr.write("ERROR! No reference points for %s!\n" % pdb_id)
            pdb.mapped_peptides[column_title] = "no_data"
            
    return pdb.mapped_peptides
    
def runGroundTruthDistanceAnalyses(pdb_id,peptide_data,pdb_dir=".",mindistance=4.2):
    """ Runs the FTMap analysis on output files in a directory with a specific
    pdb id and length and returns the results in a pandas dataframe. The 'out_dir'
    argument specifies the directory where the pyRBDome files are stored. This
    directory should contain all the folders with analysis results for individual
    pdb files."""

    column_title = "Ground_truth_distances"
    
    ### Checking if the RNABindRPlus pdb file exists:
    pdb_file = "%s.pdb" % pdb_id
    pdb_file_path = os.path.join(pdb_dir,pdb_file)

    pdb = PeptidePDBAnalyser()
    pdb.mapped_peptides = peptide_data

    if not os.path.exists(pdb_file_path):
        sys.stderr.write("ERROR! The pdb file %s does not exist!\n" % pdb_file_path)
        pdb.mapped_peptides[column_title] = 'no_data'
    else:
        pdb.loadPDBFile(pdb_file_path,records=['ATOM'],remove_ligands=True)
        ### The b-factor column contains distances in Angstrom to docked molecules.
        ### Hence the smaller the better. As a default thresshold we use 4.2 Anstrom.
        ### which is about hydrogen-binding distances away from the RNA molecule in the structure.
        pdb.pdb_df.loc[pdb.pdb_df['b_factor'] <= mindistance,'b_factor'] = 1.0
        pdb.pdb_df.loc[pdb.pdb_df['b_factor'] > mindistance,'b_factor'] = 0.0
        referencepoints = pdb.grabReferencePoints(score=1.0)
        if referencepoints.size > 0:
            pdb.measureAllPeptideDistancesToReferences(referencepoints,column_title)
        else:
            sys.stderr.write("ERROR! No reference points for %s!\n" % pdb_id)
            pdb.mapped_peptides[column_title] = "no_data"

    return pdb.mapped_peptides

def runGroundTruthBindingSiteAnalyses(pdb_id,peptide_data,pdb_dir=".",mindistance=4.2):
    """ Runs the FTMap analysis on output files in a directory with a specific
    pdb id and length and returns the results in a pandas dataframe. The 'out_dir'
    argument specifies the directory where the pyRBDome files are stored. This
    directory should contain all the folders with analysis results for individual
    pdb files."""

    column_title = "Ground_truth_results"
    
    ### Checking if the RNABindRPlus pdb file exists:
    pdb_file = "%s.pdb" % pdb_id
    pdb_file_path = os.path.join(pdb_dir,pdb_file)

    pdb = PeptidePDBAnalyser()
    pdb.mapped_peptides = peptide_data

    if not os.path.exists(pdb_file_path):
        sys.stderr.write("ERROR! The pdb file %s does not exist!\n" % pdb_file_path)
        pdb.mapped_peptides[column_title] = 'no_data'
    else:
        pdb.loadPDBFile(pdb_file_path,records=['ATOM'],remove_ligands=True)
        ### The b-factor column contains distances in Angstrom to docked molecules.
        ### Hence the smaller the better. As a default thresshold we use 4.2 Anstrom.
        ### which is about hydrogen-binding distances away from the RNA molecule in the structure.
        pdb.pdb_df.loc[pdb.pdb_df['b_factor'] <= mindistance,'b_factor'] = 1.0
        pdb.pdb_df.loc[pdb.pdb_df['b_factor'] > mindistance,'b_factor'] = 0.0
        referencepoints = pdb.grabReferencePoints(score=1.0)
        if referencepoints.size > 0:
            pdb.highlightPeptideAminoAcidsByScore(score=1.0,header=column_title)
        else:
            sys.stderr.write("ERROR! No reference points for %s!\n" % pdb_id)
            pdb.mapped_peptides[column_title] = "no_data"

    return pdb.mapped_peptides

def foundPeptideSingleAA(found_peptide):
    if found_peptide != 'not_found' and found_peptide != 'unambiguous':
        old_peptide = found_peptide
        start = re.split(r'(\d+)', old_peptide)[1]
        end = re.split(r'(\d+)', old_peptide)[-2]
        chain = re.split(r'(\d+)',old_peptide)[-1]
        new_start = "%s%s"%(int((int(start)+int(end))/2),chain)
        new_end = new_start
        peptide = old_peptide.split("_")[1]
        new_peptide = peptide[(len(peptide)-1)//2:(len(peptide)+2)//2]
        new_peptide = "%s_%s_%s"%(new_start,new_peptide,new_end)
    else:
        new_peptide = found_peptide
    return new_peptide

def makeNewPDBDataFrame():
    """ Returns an empty PandasPdb object with ATOM, HEADER and OTHERS keys. """
    keys = ['ATOM','HETATM','OTHERS']
    others_columns = ['record_name','entry','line_idx']
    general_columns = ['record_name','atom_number','blank_1','atom_name','alt_loc','residue_name',
                       'blank_2','chain_id','residue_number','insertion','blank_3','x_coord',
                       'y_coord','z_coord','occupancy','b_factor','blank_4','segment_id',
                       'element_symbol','charge','line_idx']

    ### Make an empty PandasPdb:
    ppdb = PandasPdb()

    ### Populate the dictionary keys and add the collumns:
    ppdb._df["OTHERS"] = pd.DataFrame(columns=others_columns)
    ppdb._df["ATOM"]  = pd.DataFrame(columns=general_columns)
    ppdb._df["HETATM"] = pd.DataFrame(columns=general_columns)

    return ppdb

def getPeptideCoordinatesFromPDBHeader(pdb_id,chain,peptides,out_dir=".",pdb_dir='pdb_files'):
    """ Extracts all the pdb coordinates for the cross-linked peptides from the pdb file and
    saves it to a separate pdb file. """
    pdb_file = "%s.pdb" % pdb_id
    pdb_file_path = os.path.abspath(os.path.join(pdb_dir,pdb_file))
    if not os.path.exists(pdb_file_path):
        sys.stderr.write("ERROR! The pdb file %s could not be found in the %s directory\n" \
                         % (pdb_file,pdb_dir))
        return False

    if not os.path.exists(out_dir):
        sys.stderr.write("ERROR! Directory %s does not exist!\n" % out_dir)
        return False
    else:
        outfile_name = "%s_%s_peptides.pdb" % (pdb_id,chain)
        outfile_name_path = os.path.join(out_dir,outfile_name)
        analyser = PeptidePDBAnalyser()
        analyser.loadPDBFile(pdb_file_path,records=['ATOM'],remove_ligands=True)

        new_ppdb = makeNewPDBDataFrame()
        
        ### Writing the first REMARK:
        count = 1
        remark = pd.DataFrame({'record_name':['REMARK'],'entry':[''],'line_idx':[count]}) 
        new_ppdb._df['OTHERS'] = pd.concat([new_ppdb._df['OTHERS'],remark])
        count +=1
        
        ### Running the analyses and adding the coordinates to the new pdb file:
        for peptide in peptides:
            ### Writing the headers:

            header = pd.DataFrame({'record_name':['HEADER'],'entry':[peptide],'line_idx':[count]})
            new_ppdb._df['OTHERS'] = pd.concat([new_ppdb._df['OTHERS'],header],ignore_index=True,axis=0)
            count +=1
            
            ### Grabbing coordinates from peptide sequences from the pdb file:
            sequence,start,end,chain = analyser.getPeptideInfo(peptide,split_coordinates=True)
            coordinates = analyser.pdb_df.loc[(analyser.pdb_df['chain_id'] == chain) &
                                              (analyser.pdb_df['residue_number'] >= start) &
                                              (analyser.pdb_df['residue_number'] <= end)]

            if not coordinates.empty:
                number_of_coordinates = len(coordinates.index)
                start_count = count
                count += number_of_coordinates
                end_count = count
                line_index = np.arange(start_count,end_count)

                coordinates['line_idx'] = np.arange(start_count,end_count)
                #peptide_coordinates = analyser.getPeptideCoordinatesFromPDBFile(peptides)
                new_ppdb._df['ATOM'] = pd.concat([new_ppdb._df['ATOM'],coordinates],ignore_index=True)
                count += number_of_coordinates
            else:
                sys.stderr.write(f"ERROR! Could not find coordinates for peptide {peptide} in {pdb_file}!\n")
                
        if not new_ppdb._df['ATOM'].empty:
            ### Adding the final REMARK
            count += 1
            final = pd.DataFrame({'record_name':['REMARK'],'entry':[''],'line_idx':[count]})
            new_ppdb._df['OTHERS'] = pd.concat([new_ppdb._df['OTHERS'],final],ignore_index=True)

            ### Writing to pdb output file:
            new_ppdb.to_pdb(outfile_name_path,
                            records=["ATOM","OTHERS"],
                            gz=False,
                            append_newline=True)
            return True
        else:
            sys.stderr.write("ERROR! The dataframe is empty. Nothing to write to a pdb file\n")
            return False

def getAminoAcidCoordinatesFromPDBHeader(pdb_id,chain,peptides,out_dir=".",pdb_dir='pdb_files'):
    """ Extracts all the pdb coordinates for the cross-linked amino acids from the pdb file and
    saves it to a separate pdb file. """
    pdb_file = "%s.pdb" % pdb_id
    pdb_file_path = os.path.abspath(os.path.join(pdb_dir,pdb_file))
    if not os.path.exists(pdb_file_path):
        sys.stderr.write("ERROR! The pdb file %s could not be found in the %s directory\n" \
                         % (pdb_file,pdb_dir))
        return False

    if not os.path.exists(out_dir):
        sys.stderr.write("ERROR! Directory %s does not exist!\n" % out_dir)
        return False
    else:
        outfile_name = "%s_%s_cross_linked_amino_acids.pdb" % (pdb_id,chain)
        outfile_name_path = os.path.join(out_dir,outfile_name)
        analyser = PeptidePDBAnalyser()
        analyser.loadPDBFile(pdb_file_path,records=['ATOM'],remove_ligands=True)

        new_ppdb = makeNewPDBDataFrame()
        
        ### Writing the first REMARK:
        count = 1
        remark = pd.DataFrame({'record_name':['REMARK'],'entry':[''],'line_idx':[count]}) 
        new_ppdb._df['OTHERS'] = pd.concat([new_ppdb._df['OTHERS'],remark])
        count +=1
        
        ### Running the analyses and adding the coordinates to the new pdb file:
        for peptide in peptides:
            ### Writing the headers:

            header = pd.DataFrame({'record_name':['HEADER'],'entry':[peptide],'line_idx':[count]})
            new_ppdb._df['OTHERS'] = pd.concat([new_ppdb._df['OTHERS'],header],ignore_index=True,axis=0)
            count +=1
            
            ### Grabbing coordinates from peptide sequences from the pdb file:
            sequence,start,end,chain = analyser.getPeptideInfo(peptide,split_coordinates=True)
            coordinates = analyser.pdb_df.loc[(analyser.pdb_df['chain_id'] == chain) &
                                              (analyser.pdb_df['residue_number'] >= start) &
                                              (analyser.pdb_df['residue_number'] <= end)]

            if not coordinates.empty:
                number_of_coordinates = len(coordinates.index)
                start_count = count
                count += number_of_coordinates
                end_count = count
                line_index = np.arange(start_count,end_count)

                coordinates['line_idx'] = np.arange(start_count,end_count)
                #peptide_coordinates = analyser.getPeptideCoordinatesFromPDBFile(peptides)
                new_ppdb._df['ATOM'] = pd.concat([new_ppdb._df['ATOM'],coordinates],ignore_index=True)
                count += number_of_coordinates
            else:
                sys.stderr.write(f"ERROR! Could not find coordinates for peptide {peptide} in {pdb_file}!\n")
                
        if not new_ppdb._df['ATOM'].empty:
            ### Adding the final REMARK
            count += 1
            final = pd.DataFrame({'record_name':['REMARK'],'entry':[''],'line_idx':[count]})
            new_ppdb._df['OTHERS'] = pd.concat([new_ppdb._df['OTHERS'],final],ignore_index=True)

            ### Writing to pdb output file:
            new_ppdb.to_pdb(outfile_name_path,
                            records=["ATOM","OTHERS"],
                            gz=False,
                            append_newline=True)
            return True
        else:
            sys.stderr.write("ERROR! The dataframe is empty. Nothing to write to a pdb file\n")
            return False

def getProteinSequencesFromFasta(dataframe,proteome):
    dataframe['sequence'] = None
    for seq_record in SeqIO.parse(proteome, "fasta"): #proteome is the fasta file you uploaded.
        uniprot_id = seq_record.id.split("|")[1]        
        sequence = seq_record.seq
        for i in dataframe.index:
            if uniprot_id in i:
                dataframe['sequence'][i] = str(sequence)
