# config.py

import os

PACKAGE_DIR = os.path.dirname(os.path.abspath(__file__))
### Below the default chrome path is for linux. You will need to change this if you want to run the package on Mac OS
CHROME_PATH = os.path.join(PACKAGE_DIR,'Chrome','chrome-linux64','chrome')
### Make sure the chromedriver script has the exact same version as chrome.
CHROME_DRIVER_PATH = os.path.join(PACKAGE_DIR,'Chrome','chromedriver-linux64','chromedriver')

os.environ['PATH'] = f"{os.environ['PATH']}:{os.path.dirname(CHROME_DRIVER_PATH)}"
