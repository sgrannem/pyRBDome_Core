__author__		= "Sander Granneman"
__copyright__	= "Copyright 2020"
__version__		= "0.0.1"
__credits__		= ["Sander Granneman"]
__maintainer__	= "Sander Granneman"
__email__		= "Sander.Granneman@ed.ac.uk"
__status__		= "beta"

##################################################################################
#
#	PyPDBResults
#
#
#	Copyright (c) Sander Granneman 2020
#
#	Permission is hereby granted, free of charge, to any person obtaining a copy
#	of this software and associated documentation files (the "Software"), to deal
#	in the Software without restriction, including without limitation the rights
#	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#	copies of the Software, and to permit persons to whom the Software is
#	furnished to do so, subject to the following conditions:
#
#	The above copyright notice and this permission notice shall be included in
#	all copies or substantial portions of the Software.
#
#	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#	THE SOFTWARE.
#
##################################################################################

import os
import sys
import pandas as pd
from pypdb import *


class ParsePyPDBResults():
    """ A class for parsing rcsb.org sequence search results by pypdb """

    def __init__(self,resultsdict):
        self.resultsdict = resultsdict

    def getPDBIDs(self):
        """ returns the PDB IDs in a pandas DataFrame """
        pdb_ids = [i['identifier'].split("_")[0] for i in self.resultsdict['result_set']]
        return pd.DataFrame(pdb_ids,columns=['PDB_ID'])

    def getNormScores(self):
        """ returns the PDB IDs and normalized scores in a pandas DataFrame """
        df = self.getPDBIDs()
        norm_scores = [float(i['services'][0]['nodes'][0]['norm_score']) for i in self.resultsdict['result_set']]
        df['norm_scores'] = norm_scores
        return df

    def getAlignmentResults(self):
        """ Returns all the alignment and scoring results in a pandas DataFrame """
        df = self.getNormScores()
        for i in df.index:
            alignmentresults = self.resultsdict['result_set'][i]['services'][0]['nodes'][0]['match_context'][0]
            results = pd.DataFrame.from_dict(alignmentresults,orient='index',columns=[i]).T
            df.loc[i,results.columns] = results.values[0]
        return df

    def getBestMatchingResults(self):
        """ Gets the best matching results from the sequence similarity search """
        df = self.getAlignmentResults()
        maxnorm_score = max(df['norm_scores'])
        return df[df['norm_scores'] >= maxnorm_score]

    def getTopMatchingResult(self):
        """ Gets the first of the best matching results from the sequence similarity search """
        df = self.getAlignmentResults()
        maxnorm_score = max(df['norm_scores'])
        return df[df['norm_scores'] >= maxnorm_score].loc[0]
