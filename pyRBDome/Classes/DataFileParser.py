__author__		= ["Niki Christopoulou"]
__copyright__	= "Copyright 2021"
__version__		= "0.0.1"
__credits__		= ["Niki Christopoulou","Sander Granneman"]
__maintainer__	= "Sander Granneman"
__email__		= "Sander.Granneman@ed.ac.uk"
__status__		= "beta"

##################################################################################
#
#	DataFileParser
#
#
#	Copyright (c) Niki Christopoulou and Sander Granneman 2020
#
#	Permission is hereby granted, free of charge, to any person obtaining a copy
#	of this software and associated documentation files (the "Software"), to deal
#	in the Software without restriction, including without limitation the rights
#	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#	copies of the Software, and to permit persons to whom the Software is
#	furnished to do so, subject to the following conditions:
#
#	The above copyright notice and this permission notice shall be included in
#	all copies or substantial portions of the Software.
#
#	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#	THE SOFTWARE.
#
##################################################################################

import os
import sys
import pandas as pd
import numpy as np

class DataFileParser():
    """ A class for parsing MS data files for pyRBDome analysis.
    A datafile containing 'ID' and 'Peptide' columns is required as an argument.
    It can return an output csv file containing only ID and peptide sequence either for the whole dataset
    or for a selected list of proteins of interest if that is provided."""

    def __init__(self, datafile, sep = "\t"):
        """ Starts the class and parses the datafile. The parsed data are stored in the 'data' variable.
        Note that the input file has to contain at least two columns, 'ID' and 'Peptide' in which the latter
        contains the peptide sequences returned by RNPxl or another software program. When loading the file,
        this class will remove any modifications from the sequence string, such as phosphorylations and
        oxidations. This is important as it would otherwise be impossible to map the peptides to the pdb
        files later on."""

        try:
            self.data = pd.read_excel(datafile,header=0,index_col=0)
        except ValueError:
            self.data = pd.read_csv(datafile,header=0,index_col=0, sep = sep)
        self.data.replace(np.nan,'',inplace=True)
        ### Removing modifications from the peptide string:
        try:
            self.data.loc[:,'Peptide'] = self.data.loc[:,'Peptide'].str.replace('\\s*\\([^\\)]+\\)','')
        except:
            sys.stderr.write("ERROR! The peptide column is not available in this file.\n")

    def filterByLocalisationScore(self,score=0):
        """ RNPxl provides a column with localisation scores. Some scores have a
        value of 0 and you may want to remove these before we continue with the analyses.
        If score is set to 0 then only those peptides with a localisation score
        higher than 0 are selected. This function returns a new dataframe with
        the peptides filtered by the score. """

        return self.data[self.data['Best loc score'] > score]

    def selectProteinsFromData(self,protein_list):
        """ Extracts specific proteins/genes from the data using a list as input.
        NOTE! If a specific gene in the list cannot be found in the data dataframe,
        it will not be considered. Returns a new dataframe with the selected genes."""

        available_genes = self.data[self.data.index.isin(protein_list)].index
        return self.data.loc[available_genes]

    def exportParsedData(self,dataframe=pd.DataFrame(),outputfile=None,verbose=True):
        """Exports the parsed data in a csv file containing only the 'ID' and 'Peptide' columns.
        You can provide a dataframe to the function that you want to write to a csv file.
        By default it will export the entire dataset to a new file.
        Do not forget to add your output file name. If an 'outputfile' is not provided, the data is simply returned
        as a dataframe. Set verbose to True if you do not want to get progress messages."""

        if not dataframe.empty:
            data = dataframe
        else:
            data = self.data

        ### Only keeping the 'ID' and 'Peptide' columns:
        data = data.loc[:, 'Peptide']
        data = pd.DataFrame(data.drop_duplicates())

        if outputfile:
            data.to_csv(outputfile)
            if not verbose:
                sys.stdout.write('Data was written to %s.\n' % outputfile)
        else:
            return data
