__author__		= "Sander Granneman"
__copyright__	= "Copyright 2023"
__version__		= "0.1.6"
__credits__		= ["Sander Granneman"]
__maintainer__	= "Sander Granneman"
__email__		= "Sander.Granneman@ed.ac.uk"
__status__		= "beta"

##################################################################################
#
#	FTMapAnalysis class
#
#
#	Copyright (c) Sander Granneman 2021
#
#	Permission is hereby granted, free of charge, to any person obtaining a copy
#	of this software and associated documentation files (the "Software"), to deal
#	in the Software without restriction, including without limitation the rights
#	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#	copies of the Software, and to permit persons to whom the Software is
#	furnished to do so, subject to the following conditions:
#
#	The above copyright notice and this permission notice shall be included in
#	all copies or substantial portions of the Software.
#
#	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#	THE SOFTWARE.
#
##################################################################################

import os
import sys
import time
import pandas as pd
from shutil import move
from selenium import webdriver
from bs4 import BeautifulSoup
from io import StringIO
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from pyRBDome.Functions.RBDomeSQL import *
from pyRBDome import config

def waitUntilDownloaded(file_name,time_out=10):
    """ Checks if a specific file has been downloaded or not.
    You can set a time_out (in seconds) and if the file has not been
    downloaded within that time, the function returns False. If the file
    has been downloaded, the function returns True."""

    counter = 0
    while not os.path.exists(file_name):
        time.sleep(1)
        counter += 1
        if os.path.exists(file_name):
            return True
            break
        if counter == time_out:
            return False
            break

class FTMapAnalysis():
    """ A class for submitting and downloading FTMap jobs. Requires the pdb ID (i.e. IVL9),
    the chains you want to analyse (i.e. ABC), a username and a password. So you need
    to register yourself on the website before running this script!
    This class was successfully tested on MacOSX and Ubuntu machines using Chrome
    as default browser. However, in order to get it to work you need
    to have the chromedriver script(download from Google) in your system path!
    The chromedriver script should also be compatible with your version of Chrome.
    Safest is to download the latest versions of both! """

    def __init__(self,verbose=True,overwrite=False,headless=True,out_dir=".",database=True,database_verbose=False,database_table='pyrbdome_analysis',database_name='pyrbdome.db'):
        """ Initialising the variables """
        self.pdb_file = str()
        self.out_dir = out_dir
        self.chains = list()
        self.job_error = False
        self.job_title = str()
        self.job_id = str()
        self.headless = headless
        self.browser = None
        self.verbose = verbose
        self.overwrite = overwrite
        self.connected = False
        self.download_link = str()
        self.jobinfo_submitted = False
        self.job_submitted = False
        self.job_finished = False
        self.url = "https://ftmap.bu.edu/login.php"
        self.results_table = pd.DataFrame()
        self.queue_table = pd.DataFrame()
        self.out_dir = os.path.abspath(out_dir)
        self.database = database
        self.database_verbose = database_verbose
        self.database_table = database_table
        self.database_name = database_name
        #Add columns to the database. They will be populated later when calling some of the class methods
        if database:
            addEmptyColumn('FTMap', self.database_table, self.database_name, verbose=self.database_verbose)

    def connectToServer(self,username,password):
        """ To login to the FTMap server. You need to
        provide your username and password """

        ### Set Chrome options
        chrome_options = webdriver.ChromeOptions()
        chrome_options.binary_location = config.CHROME_PATH
        prefs = {'download.default_directory' : '%s' % self.out_dir}
        chrome_options.add_experimental_option('prefs', prefs)
        
        ### Configure Chrome options:
        chrome_options.add_argument(f'--chromedriver={config.CHROME_DRIVER_PATH}')
        
        if self.headless:
            chrome_options.add_argument('--headless')
            chrome_options.add_argument("--window-size=1920,1200")
            chrome_options.add_argument("--disable-extensions")
            chrome_options.add_argument('--disable-gpu')
            chrome_options.add_argument('--disable-dev-shm-usage')
            chrome_options.add_argument('--no-sandbox')
            chrome_options.add_argument('--ignore-certificate-errors')
            
        ### Connecting to the FTMap website:
        self.browser = webdriver.Chrome(options=chrome_options)
        self.browser.implicitly_wait(20)
        self.browser.get(self.url)

        ### Logging in:
        username_submit = self.browser.find_element(By.ID, value="username")
        username_submit.clear()
        username_submit.send_keys(username)

        pwd_submit = self.browser.find_element(By.ID, value="password")
        pwd_submit.clear()
        pwd_submit.send_keys(password)

        submit_job = self.browser.find_element(By.ID, value="submit")
        submit_job.click()

        timeout=10

        ### Waiting for the job submission page to load:
        try:
            WebDriverWait(self.browser,timeout).until(EC.presence_of_element_located((By.NAME,"jobname")))
        except:
            sys.stderr.write("It is taking too long for the page to load. Exiting!\n")
            self.connected = False
        else:
            self.connected = True
            if self.verbose:
                sys.stdout.write("You have successfully connected to the FTMap server\n")

    def getQueueTable(self):
        """Returns a table containing an overview of all the jobs that are running."""
        try:
            queue_link = "https://ftmap.bu.edu/queue.php"
            self.browser.get(queue_link)
            html = self.browser.page_source
            soup = BeautifulSoup(html, 'html.parser')
            alltables = soup.findAll("table")

            if alltables:
                html_string = str(alltables[0])
                html_data = StringIO(html_string)
                self.queue_table = pd.read_html(html_data, index_col=0)[0]
                return True
            else:
                print("No tables found on the page.")
                return False
        except Exception as e:
            print(f"Error occurred: {str(e)}")
            return False

    def getResultsTables(self, max_pages=4):
        """Returns a table with all of the jobs that have been submitted
        in the past and reports the job names and the outcome of the job."""

        self.results_table = pd.DataFrame(columns=["Name", "Status"])
        offset = 0
        while True:
            if offset == max_pages:  # max number of pages
                break
            else:
                try:
                    results_link = 'https://ftmap.bu.edu/results.php?offset=%s' % offset
                    self.browser.get(results_link)
                except:
                    break
                else:
                    html = self.browser.page_source
                    soup = BeautifulSoup(html, 'html.parser')
                    alltables = soup.findAll("table")

                    if alltables:
                        html_string = str(alltables[0])
                        html_data = StringIO(html_string)
                        result_df = pd.read_html(html_data, index_col=0)[0]
                        self.results_table = pd.concat([self.results_table, result_df])
                        offset += 1
                    else:
                        print("No tables found on the page.")
                        break
        return True

    def submitJob(self,pdb_file,job_title,chains):
        """ Submits your pdb_file to the FTMap server. This requires the full
        path to the pdb file that you want to submit as well as a job title
        and the chains that you want to analyse. The chains should be provided
        as a single string (i.e. chains = ABCD)."""

        if not self.connected:
            sys.stderr.write("ERROR! You are not connected to the FTMap server (anymore)\n")
            self.jobinfo_submitted = False
            return False

        output_dir = self.out_dir
        output_name = "%s_FTMap.pdb" % job_title
        output_path = os.path.join(output_dir,output_name)
        self.pdb_id = pdb_file.split("/")[-1].strip(".pdb")

        if self.overwrite:
            if os.path.isfile(output_path):
                if self.verbose:
                    sys.stdout.write("Results for pdb file %s and chains %s already exist. Overwriting.\n" % \
                                (pdb_file,chains))
                os.remove(output_path)
        else:
            if os.path.isfile(output_path):
                sys.stderr.write("Results for pdb file %s and chains %s already exist. Exiting.\n" % \
                                (pdb_file,chains))
                self.jobinfo_submitted = False
                #Populate the column FTMap of the sqlite database
                if self.database:
                    updateColumn(self.database_table,'FTMap == "Results downloaded"', 'pdb_id == "%s"' % self.pdb_id, self.database_name, verbose=self.database_verbose)
                return False

        self.browser.find_element(By.ID,"tabMap").click()
        self.browser.implicitly_wait(20)

        self.pdb_file = os.path.abspath(pdb_file)
        self.chains = chains
        self.job_title = job_title

        ### Setting the job title
        job_name = self.browser.find_element(By.NAME,"jobname")
        job_name.clear
        job_name.send_keys(self.job_title)

        ### Uploading the pdb_file:
        self.browser.find_element(By.ID,"showprotfile").click()
        element_present = EC.presence_of_element_located((By.ID,"prot"))
        WebDriverWait(self.browser,10).until(element_present)
        pdb_submit = self.browser.find_element(By.ID,"prot")
        pdb_submit.clear()
        pdb_submit.send_keys(self.pdb_file)

        ### Submitting the chain info:
        chains_submit = self.browser.find_element(By.NAME,"protchains")
        chains_submit.clear()
        chains_submit.send_keys(" ".join(list(self.chains)))

        ### Submitting job:
        self.browser.find_element(By.NAME,"action").click()

        ### Waiting for th queue page to load:
        try:
            element_present = EC.presence_of_element_located((By.CLASS_NAME,'nice'))
            WebDriverWait(self.browser,30).until(element_present)
        except:
            sys.stderr.write("ERROR! Unable to submit job for pdb file %s!\n" % pdb_file)
            self.job_submitted = False
            return False

        ### Refreshing the queue table:
        self.getQueueTable()
        ### Sometimes there are multiple jobs that have the same name. Get only the top one:
        if self.job_title in self.queue_table['Name'].values:
            indices =  self.queue_table[self.queue_table['Name'].str.contains(self.job_title)].index
            number_of_job_ids = len(indices)
            if number_of_job_ids > 1:
                sys.stderr.write("WARNING! There are %s jobs with the name %s. Picking the job ID for the most recent one!\n" \
                % (number_of_job_ids,self.job_title))
            self.job_id = indices[0]
            if self.job_id:
                self.job_submitted = True
                if self.verbose:
                    sys.stdout.write("You have successfully submitted your job titled %s. Job ID is %s\n"
                                     % (self.job_title,self.job_id))
                return True
        else:
            self.job_submitted = False
            sys.stderr.write("Your job titled %s has not been successfully submitted, please check the Queue on the FTMap website! \n" \
            % self.job_title)
            return False

    def jobIsFinished(self,job_id=None):
        """ Checks in the queue and results tables tosee if the job is finished """
        self.getQueueTable()
        self.getResultsTables()
        if job_id:
            if job_id in self.queue_table.index:
                self.job_finished = False
                return False
            elif job_id in self.results_table.index:
                self.job_finished = True
                return True
            else:
                self.job_error = True
                self.job_finished = True
                sys.stderr.write("ERROR! Job %s has not been properly submitted!\n" % job_id)
                return True              
        elif self.job_id:
            if self.job_id in self.queue_table.index:
                self.job_finished = False
                return False
            elif self.job_id in self.results_table.index:
                self.job_finished = True
                return True
            else:
                self.job_error = True
                self.job_finished = True
                sys.stderr.write("ERROR! Job %s has not been properly submitted!\n" % job_id)
                return True
        else:
            self.job_error = True
            self.job_finished = True
            sys.stderr.write("ERROR! No job ID or name available to analyse!\n")
            return True

    def downloadResults(self,job_id=None, pdb_id=None):
        """ Downloads the results once the job is finished """
        
        ##the pdb_id is needed for updating the SQLite database while running this method.
        if pdb_id:
            self.pdb_id=pdb_id

        if job_id:
            self.jobIsFinished(job_id=job_id)
        elif self.job_id:
            job_id = self.job_id
            self.jobIsFinished(job_id=job_id)
        else:
            sys.stderr.write("ERROR with downloading! No job ID available to analyse!\n")
            #Populate the column FTMap of the sqlite database
            if self.database:
                updateColumn(self.database_table, 'FTMap == "ERROR: job ID not found"', 'pdb_id == "%s"' % self.pdb_id, self.database_name, verbose=self.database_verbose)
            return False

        if not self.job_finished:
            sys.stderr.write("ERROR! Job is not finished yet!\n")
            #Populate the column FTMap of the sqlite database
            if self.database:
                updateColumn(self.database_table, 'FTMap == "ERROR: job not finished"', 'pdb_id == "%s"' % self.pdb_id, self.database_name, verbose=self.database_verbose)
            return False
        else:
            self.download_link = "https://ftmap.bu.edu/file.php?jobid=%s&coeffi=0&model=0&filetype=model_file" % job_id
            try:
                self.browser.get(self.download_link)
            except:
                sys.stderr.write("Results for job %s are not available anymore.\n" % job_id)
                self.downloaded = False
                self.download_link = None
                self.job_finished = True
                self.job_error = True
                #Populate the column FTMap of the sqlite database
                if self.database:
                    updateColumn(self.database_table, 'FTMap == "ERROR: results not available"', 'pdb_id == "%s"' % self.pdb_id, self.database_name, verbose=self.database_verbose)
                return False
            else:
                old_file_name_path = "%s/fftmap.%s.pdb" % (self.out_dir,job_id)
                waitUntilDownloaded(old_file_name_path,time_out=60)
                if not pdb_id:
                    job_title = self.results_table.loc[job_id,'Name']
                else:
                    job_title = pdb_id
                new_file_name = "%s_FTMap.pdb" % job_title
                new_file_name_path = os.path.join(self.out_dir,new_file_name)
                try:
                    move(old_file_name_path,new_file_name_path)
                except:
                    sys.stderr.write("ERROR! Could not move file %s to %s\n" \
                    % (old_file_name_path,new_file_name_path))
                    self.downloaded = False
                    self.job_error = True
                    self.job_finished = True
                    #Populate the column FTMap of the sqlite database
                    if self.database:
                        updateColumn(self.database_table, 'FTMap == "ERROR: Could not move file"', 'pdb_id == "%s"' % self.pdb_id, self.database_name, verbose=self.database_verbose)
                else:
                    self.downloaded = True
                    self.job_finished = True
                    if self.verbose:
                        sys.stdout.write("File %s has been downloaded\n" % new_file_name)
                    #Populate the column FTMap of the sqlite database
                    if self.database:
                        updateColumn(self.database_table, 'FTMap == "Results downloaded"', 'pdb_id == "%s"' % self.pdb_id, self.database_name, verbose=self.database_verbose)
                    return True

    def closeBrowser(self):
        """ Closes the driver that runs the browser """
        self.browser.quit()
