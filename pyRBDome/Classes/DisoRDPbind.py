#!/usr/bin/env python
# coding: utf-8

__author__      = ["Hugh McCaughan","Liang-Cui Chu","Sander Granneman"]
__copyright__   = "Copyright 2023"
__version__     = "0.0.4"
__credits__     = ["Hugh McCaughan","Liang-Cui Chu","Sander Granneman"]
__maintainer__  = "Sander Granneman"
__email__       = "Sander.Granneman@ed.ac.uk"
__status__      = "beta"

##################################################################################
#
#       DisoRDPbind
#
#
#       Copyright (c) Hugh McCaughan and Sander Granneman 2021
#
#       Permission is hereby granted, free of charge, to any person obtaining a copy
#       of this software and associated documentation files (the "Software"), to deal
#       in the Software without restriction, including without limitation the rights
#       to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#       copies of the Software, and to permit persons to whom the Software is
#       furnished to do so, subject to the following conditions:
#
#       The above copyright notice and this permission notice shall be included in
#       all copies or substantial portions of the Software.
#
#       THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#       IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#       FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#       AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#       LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#       OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#       THE SOFTWARE.
#
##################################################################################


import os
import sys
import re
import urllib
import numpy as np
import pandas as pd
from collections import defaultdict
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from pyRBDome import config

class DisoRDPBind():
    """ A class for submitting jobs and downloading results to the DisoRDPBind server.
    It requires a fasta file (.fasta) in standard protein format.
    This should be given as a variable fasta_id when initialising the class.
    If the source of this file is not in the current working directory then you
    should also provide the name of the directory with the variable fasta_dir.
    This can be a multiple fasta file. The output is a list of tuples with the ID
    pulled from the fasta file as the first value and a dataframe which contains
    an amino acid index number, an amino acid letter code, a binary score, and a
    probability score from 0-1 as the second value"""

    def __init__(self,verbose=True,overwrite=False,headless=True,out_dir="."):
        """ Initialising class variables """
        self.verbose = verbose
        self.headless = headless
        self.browser = None
        self.disordbbind_url = "http://biomine.cs.vcu.edu/servers/DisoRDPbind/"
        self.job_id = str()
        self.results_link = str()
        self.results_dict = defaultdict(list)
        self.__connected = False
        self.__seqs_uploaded = False
        self.__email_provided = False
        self.__job_submitted = False
        self.__reuslts_link_obtained = False
        self.__results_obtained = False

        if not os.path.isdir(out_dir):
            os.mkdir(out_dir)

        self.out_dir = os.path.abspath(out_dir)

    def connectToServer(self):
        """ Runs selenium and open the webpage for submitting jobs """
        
        ### Set Chrome options
        chrome_options = webdriver.ChromeOptions()
        chrome_options.binary_location = config.CHROME_PATH
        
        ### Configure Chrome options:
        chrome_options.add_argument(f'--chromedriver={config.CHROME_DRIVER_PATH}')
        
        if self.headless:
            chrome_options.add_argument('--headless')
            chrome_options.add_argument("--window-size=1920,1200")
            chrome_options.add_argument("--disable-extensions")
            chrome_options.add_argument('--disable-gpu')
            chrome_options.add_argument('--disable-dev-shm-usage')
            chrome_options.add_argument('--no-sandbox')
            chrome_options.add_argument('--ignore-certificate-errors')

        ### Connecting to the aaRNA website:
        self.browser = webdriver.Chrome(options=chrome_options)

        ### Start selenium and go to the DisoRDPBind webpage
        try:
            self.browser = webdriver.Chrome(options=chrome_options)
            self.browser.implicitly_wait(20)
            self.browser.get(self.disordbbind_url)
        except:
            sys.stderr.write("Unable to conect to DisoRDPBind!\n!")
            self.__connected = False
            return False
        else:
            self.__connected = True
            if self.verbose:
                sys.stdout.write("Connected to the server\n")
            return True

    def submitFastaFile(self,fasta_file=None):
        """ Upload the named fasta file to the website. You must provide the fasta_file variable.
        This must be the name of the fasta file you wish to upload. """

        assert self.__connected, "ERROR! You are not connected to the server!\n"

        ###Upload Fasta file
        self.fasta_file = fasta_file
        self.fasta_file = os.path.abspath(self.fasta_file)
        try:
            upload = self.browser.find_element(By.ID,"upload")
            upload.send_keys(self.fasta_file)
        except:
            sys.stderr.write("ERROR! Unable to upload the fasta file to the server!\n")
            return False
        else:
            if self.verbose:
                sys.stdout.write("Successfully uploaded fasta file\n")
            self.__seqs_uploaded = True

        return True

    def submitProteinSequences(self, prot_names=None, prot_seqs=None):
        """ Upload to DisoRDPBind using free text rather than a fasta file.
        Need to provide a list of protein names, and a list of protein sequences in order."""

        assert self.__connected, "ERROR! You are not connected to the server!\n"

        self.prot_names = prot_names
        self.prot_seqs = prot_seqs
        headers = [">" + s for s in self.prot_names]
        to_upload = list(zip(headers,self.prot_seqs))
        try:
            send_text = self.browser.find_element(By.NAME,"seq").send_keys(to_upload)
        except:
            sys.stderr.write("ERROR! Unable to upload the text into the text area!\n")
            return False
        else:
            if self.verbose:
                sys.stdout.write("Successfully pasted the fasta sequences in the text window\n")
            self.__seqs_uploaded = True

        return True

    def submitEmailAddress(self,email_address=None):
        """ Send your e-mail address to the server. This is essential if you want to
        get the results back. """

        assert self.__connected, "ERROR! You are not connected to the server!\n"

        try:
            self.email_address = email_address
            self.browser.find_element(By.NAME,"email1").send_keys(self.email_address)
        except:
            sys.stderr.write("ERROR! Unable to submit your e-mail address!\n")
            return False
        else:
            if self.verbose:
                sys.stdout.write('Email address uploaded\n')
            self.__email_provided = True

        return True

    def submitJob(self):
        """ Submits the job to the server if all the required information has been
        submitted to the server. """

        assert self.__connected, "ERROR! You are not connected to the server!\n"
        assert self.__seqs_uploaded, "ERROR! You haven't uploaded any protein sequences yet!\n"
        assert self.__email_provided, "ERROR! You haven't provided an e-mail address yet!\n"

        ### Submit job
        try:
            button = self.browser.find_element(By.NAME, "Button1")
            ActionChains(self.browser).move_to_element(button).perform()
            button.click()
            sys.stdout.write("Job submitted successfully.\n")
            self.__job_submitted = True
        except Exception as e:
            sys.stderr.write(f"ERROR! Unable to submit job: {str(e)}.\n")
            return False

        ### Getting the results:

        self.browser.switch_to.window(self.browser.window_handles[-1])

        try:
            WebDriverWait(self.browser,400).until(EC.presence_of_element_located((By.PARTIAL_LINK_TEXT,
                                                                                  "results.html")))
        except:
            sys.stderr.write("ERROR! The website did not generate any results for your %s fasta file!\n" \
                             % self.fasta_file)
            return False
        else:
            self.job_id = self.browser.current_url.split("=")[1]
            self.results_link = "http://biomine.cs.vcu.edu/webresults/DisoRDPbind/%s/results.txt" % self.job_id
            if self.verbose:
                sys.stdout.write("Run was successful and download link obtained\n")
            self.__results_link_obtained = True

            return True

    def downloadResults(self,job_id=None,outputfile=None):
        """ Downloads the results from the DisoRDPBind server.
        If a job_id is provided it will download the results for that
        specific id. If no job_id is provided it will check if any jobs
        were submitted when the class was initialised. You can also provide
        an outputfile name in case you wish to print the results straight to
        a text file. Or you can chose to use the processResults function to
        convert the text into a dictionary containing pandas dataframes with
        the results for each protein. """

        results_link = str()

        if job_id:
            results_link = "http://biomine.cs.vcu.edu/webresults/DisoRDPbind/%s/results.txt" % job_id
        elif self.results_link:
            results_link = self.results_link
        else:
            sys.stderr.write("ERROR! No job id provided or any download links available! Cannot download the results!\n")
            return False

        results = urllib.request.urlopen(results_link)
        self.results = [str(line,'utf-8').strip() for line in results]

        if outputfile:
            with open(outputfile,"w") as f:
                   f.write(urllib.request.urlopen(results_link).read().decode('utf-8'))
        return True

    def processResults(self,results=None):
        """ Converts the results into a dictionary containing pandas dataframes with
        the results for each protein. """

        to_process =  list()

        if results:
            to_process = results
        elif self.results:
            to_process = self.results
        else:
            sys.stderr.write("ERROR! No results were provided or avaiable! Cannot process the results!\n")
            return False

        results_dict = defaultdict(lambda: defaultdict(list))

        for line in to_process:
            prediction = str()
            if line.startswith(">"):
                job_id = line[1:]
            else:
                if re.search("residues",line):
                    identifyer,prediction = line.split(":")
                    results_dict[job_id][identifyer] = [int(i) for i in prediction if i]
                elif re.search("propensity",line):
                    identifyer,prediction = line.split(":")
                    results_dict[job_id][identifyer] = [float(i) for i in prediction.split(",") if i]
                else:
                    results_dict[job_id]['sequence'] = [i for i in line]

        for i in results_dict:
            df = pd.DataFrame.from_dict(results_dict[i])
            self.results_dict[i] = df

        return True

    def exportResults(self):
        """ Writes the results to csv files. """
        for protein in self.results_dict:
            out_file = "%s_DisoRDPbind_results.txt" % protein
            out_path = os.path.join(self.out_dir,out_file)
            self.results_dict[protein].to_csv(out_path,sep="\t")

    def closeBrowser(self):
        """ Closes the driver that runs the browser """
        if self.verbose:
            sys.stdout.write("Quitting browser\n")
        self.browser.quit()
