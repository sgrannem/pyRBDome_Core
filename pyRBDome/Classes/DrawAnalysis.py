#!/usr/bin/python

__author__		= ["Shichao Wang","Sander Granneman"]
__copyright__	= "Copyright 2023"
__version__		= "0.0.1"
__credits__		= ["Shichao Wang","Sander Granneman"]
__maintainer__	= "Sander Granneman"
__email__		= "Sander.Granneman@ed.ac.uk"
__status__		= "beta"

import os
import sys
import pandas as pd
from reportlab.lib.units import inch
from reportlab.pdfgen import canvas
from collections import defaultdict
from pyRBDome.Classes.PDBAnalyser import *

class DrawAnalysisResults:
    """ A class for plotting all the pyRBDome analysis results. 
    Makes a pdf file that highlights the results wihtin the protein
    amino acid sequence. """
    
    def __init__(self, font_size=12):
        self.data_df = pd.DataFrame(columns=['sequence','residue_number'])
        self.font_size = font_size
        self.domain_dict = {}
        self.peptide_data = False
        self.aminoacid_data = False
        self.prediction_data = False
        self.protein_sequence_stored = False
        
        ### defining the colors.
        self.eye_friendly_colors = [
            (0, 0, 0),             # Black
            (0.9, 0.63, 0),        # Orange
            (0.36, 0.71, 0.91),    # Blue
            (0, 0.63, 0.41),       # Green
            (0, 0.4, 0.7),         # Blue-Green
            (0.81, 0.39, 0),       # Red-Orange
            (0.8, 0.41, 0.61),     # Purple
            (255, 0, 0),           # Black
            (1, 0, 0),             # Red
            (0.35, 0.35, 0.35),    # Gray
            (0.35, 0.7, 0.9),      # Light Blue
            (0, 0.5, 0),           # Dark Green
            (0.61, 0.35, 0.71),    # Violet
            (0.7, 0.7, 0),         # Olive
            (0.35, 0.61, 0),       # Lime Green
            (0.4, 0.4, 0.4),       # Dark Gray
            (0.91, 0.51, 0.28),    # Terracotta
            (0.5, 0, 0.5),         # Purple
            (0.35, 0.35, 0.7),     # Navy Blue
            (0.85, 0.65, 0),       # Gold
            (0, 0.6, 0.6)          # Teal
        ]
        
        ### Only have 20 colours so can at most display data from 20 pdb files!
        self.number_of_tracks = 0
        self.max_number_of_tracks = len(self.eye_friendly_colors)
        
    def barcolor(self,score):
        """
        Generate RGB value for a given score ranging from 0-1.
        Return a tuple of RGB value (r, g, b).
        """
        return (0.5 + 0.5 * score, 0.5, 1 - 0.5 * score)

    def printScoreBar(self,x,y,x_step,score):
        """
        Print a bar representing a score at the specified coordinates with color based on the score.
        """
        r, g, b = self.barcolor(score)
        self.canvas.setFillColorRGB(r, g, b)
        x += x_step * 0.15
        y += x_step * 0.15
        w = x_step * 0.7
        h_max = self.font_size * 0.020
        h = (score + 0.01) * h_max
        self.canvas.rect(x * inch, y * inch, w * inch, h * inch, stroke=0, fill=1)

    def printRGBL(self,x,y,x_step,letter,r=0.8,g=0.8,b=0.8):
        """
        Print a letter at the specified coordinates with color (r, g, b).
        """
        letter = letter.upper()   # Make sure the letter is uppercased
        self.canvas.setFont("Helvetica-Bold", self.font_size)     # Choose the font
        self.canvas.setFillColorRGB(r, g, b, alpha=1)    # Set the color
        self.printLetter(x,y,x_step, letter)

    def printLetter(self,x,y,x_step,letter):
        """
        Print a letter on the canvas with proper spacing.
        """
        if not letter in ['M','W']:
            if letter in ['I','J','-']:
                x += x_step * 0.4 if letter == 'I' else x_step * 0.3
            elif letter in ['P','L','Q']:
                x += x_step * 0.08 if letter == 'Q' else x_step * 0.22
            else:
                x += x_step * 0.15  
        self.canvas.drawString(x * inch, y * inch, letter)

    def printRGBRect(self,x,y,x_step,r=0.8,g=0.8,b=0.8):
        """
        Print a small rectangle at the specified coordinates with color (r, g, b).
        """
        self.canvas.setFillColorRGB(r, g, b)
        x += x_step * 0.15
        y += x_step * 0.15
        w = h = x_step * 0.7
        self.canvas.rect(x * inch, y * inch, w * inch, h * inch, stroke=0, fill=1)

    def highlight(self,x,y,x_step,r=1,g=1,b=0):
        """
        Print a rectangle which is a little larger than the letter it covers.
        """
        self.canvas.setFillColorRGB(r, g, b)
        x -= 0
        y -= x_step * 0.12
        w, h = x_step, x_step * 1.04
        self.canvas.rect(x * inch, y * inch, w * inch, h * inch, stroke=0, fill=1)
        
    def drawRectangle(self,x,y,x_step):
        """
        draw a red ractangular in the canvas
        >>Inputs:
        self.canvas, canvas object
        x, y, x_step, location parameters
        self.font_size, the size of the letters
        counts, means the number of chars it need to cover
        """
        self.canvas.setStrokeColorRGB(1, 0, 0)
        w = x_step*1.1
        h = self.font_size*(0.14)
        self.canvasrect(x * inch, y * inch, w * inch, h * inch, fill=0)

    def drawRoundRect(self,x,y,x_step,counts):
        """
        draw a round rectangle on the canvas "c"
        (x,y) should be the coodinates of the last residue that covered by the rectangle
        >>Inputs:
        c, canvas object
        x, y, x_step, location parameters
        counts, means the number of chars it need to cover
        """
        ### Calculate the proper coordinates of the rectangle
        xx = x - x_step * (counts - 1)
        yy = y - self.font_size * (0.005)
        w = x_step * counts + 0.01
        h = self.font_size * (0.02)
        radius = h * 0.3
        self.canvas.roundRect(xx * inch, yy * inch, w * inch, h * inch, radius * inch, stroke=1, fill=0)

    def drawDomain(self,x,y,x_step,counts,domain_name,color_code,shift=0):
        """
        draw a round rectangle on the canvas to indicate a domain 
        add a domain name on the top of the rectangle
        >>Inputs:
        self.canvas, canvas object
        x, y, x_step, location parameters
        counts, means the number of chars it need to cover
        shift, should be positive if different domains have overlaps
        """
        
        ### length of each domain label
        length_of_label = 6
        shift = shift * length_of_label
        shift_r = -1 + 2 * (shift%(length_of_label+2))/length_of_label
        ### altomatically specify the color to represent the domain
        r,g,b = self.eye_friendly_colors[color_code%6 + 1]
        self.canvas.setFillColorRGB(r, g, b, alpha=0.6)
        self.canvas.setStrokeColorRGB(r, g, b, alpha=0.6)
        self.canvas.setLineWidth(2)
        ### draw the rectangle to display the location of the domain
        self.drawRoundRect(x-0.01*shift_r,y+0.01*shift_r,x_step,counts)
        ### choose the font for comments
        self.canvas.setFont("Helvetica-Bold",self.font_size-1)
        self.canvas.drawString((x - x_step * (counts - 1 - shift))*inch,
                               (y + self.font_size * (0.018))*inch, domain_name)
        ### reset the parameters
        self.canvas.setFillColorRGB(0, 0, 0, alpha=1) 
        self.canvas.setStrokeColorRGB(0, 0, 0, alpha=1)
        
    def readDomainInfo(self,pdb_file_path):
        """ Reads the pdb file containing the domain informaion and
        stores this into a dictionary """

        if os.path.exists(pdb_file_path):
            pdb = PDBAnalyser()
            pdb.loadPDBFile(pdb_file_path)

            for i in pdb.df['OTHERS'].index:
                if pdb.df['OTHERS'].loc[i,'record_name'] == "HEADER":
                    domain_name = pdb.df['OTHERS'].loc[i,'entry']
                    start_index = int(pdb.df['OTHERS'].loc[i,'line_idx']+1)
                    end_index   = int(pdb.df['OTHERS'].loc[i+1,'line_idx']-1)
                    selection = pdb.df['ATOM'].loc[(pdb.df['ATOM']['line_idx'] >= start_index) &
                                                   (pdb.df['ATOM']['line_idx'] <= end_index)]
                    self.domain_dict[domain_name] = sorted(set(selection['residue_number']))
                    
        else:
            sys.stderr.write(f"ERROR! I cannot find the file {pdb_file_path}.\n")
            self.domain_dict = {}
            
    def getProteinSequence(self,pdb_file_path):
        """ This is the first thing you need to do to create the self.data_df
        variable correctly. Pass a pdb file containing the full protein sequence
        and all the relevant residue numbers. This will be used to build the 
        results dataframe. """
        
        ### Open the pdb file:
        if os.path.exists(pdb_file_path):
            pdb = PDBAnalyser()
            pdb.loadPDBFile(pdb_file_path,records=['ATOM'])

            ### Create the data dataframe:
            if self.data_df.empty:
                self.data_df = pd.DataFrame(columns=['sequence','residue_number'])  

            ### Extract the chain:
            chain = pdb.getPDBProteinChains()[0]

            ### Extract the residue numbers:
            residue_numbers = list(set(pdb.pdb_df.loc[:,'residue_number']))

            ### Extract the protein sequence from the pdb file:
            protein_sequence = list(pdb.getPDBProteinSequence(chain))

            ### Check if it can actually find a proper protein sequence. Sometimes there are characters
            ### in the pdb file that are unknown to pyRBDome.
            if protein_sequence:
                ### Populating the dataframe:
                self.data_df['sequence'] = protein_sequence
                self.data_df['residue_number'] = residue_numbers
                self.protein_sequence_stored = True
            else:
                sys.stderr.write(f"ERROR! Could not get the protein sequence from pdb {os.path.basename(pdb_file_path)}")
                self.data_df = pd.DataFrame()
        else:
            sys.stderr.write(f"ERROR! Could not find the file {pdb_file_path}.\n")
        
    def readData(self,data_type,pdb_file_path,binary=False,threshold=1.0,operator=">="):
        """
        Stores the values from the b-factor columns in pdb files in the self.data_df
        dataframe. You also have the option to set a threshold for converting the 
        b-factor values to binary data (i.e. 0 and 1's). The operator string allows
        you to specify whether you want the b-factor value to be higher or lower than
        the set threshold if it is to be considered a '1'.
        """ 
        
        if os.path.exists(pdb_file_path):
            if self.protein_sequence_stored:                             
                ### Open the pdb file:
                pdb = PDBAnalyser()
                pdb.loadPDBFile(pdb_file_path,records=['ATOM'])
    
                ### Extract the chain:
                chain = pdb.getPDBProteinChains()[0]
    
                ### Extract the residue numbers:
                residue_numbers = list(set(pdb.pdb_df.loc[:,'residue_number']))
                
                ### Extracting the b-factor values. Converting to binary values (0 or 1) if requested.
                pdb.pdb_df = pdb.pdb_df[~pdb.pdb_df.index.duplicated(keep='first')]
                
                b_factors = pdb.pdb_df.loc[:,'b_factor'].values
                if binary:
                    if operator == ">=":
                        b_factors = [1 if x >= threshold else 0 for x in b_factors]
                    elif operator == "<=":
                        b_factors = [1 if x <= threshold else 0 for x in b_factors]
                    else:
                        sys.stderr.write(f"ERROR! I cannot interpret your operator {operator}")
    
                ### Make a new column with the new data-type:
                if data_type not in self.data_df.columns:
                    self.data_df[data_type] = 0
    
                self.data_df.loc[self.data_df['residue_number'].isin(residue_numbers),data_type] = b_factors
                
                ### Updating the user of your progress:
                sys.stdout.write(f"Data for {data_type} has been successfully stored.\n")
            else:
                sys.stderr.write("ERROR!! You have to use the getProteinSequence function first to grab the protein sequence information!\n")
        else:
            sys.stderr.write(f"ERROR! Could not find the file {pdb_file_path}.\n")            
                            
    def drawPDF(self,protein_name,display_keys=[],outfile_path=None,characters_per_row=40,number_of_rows=5):
        """
        Draws a PDF file to visualize the data
        
        """
        ### Check if a list of features that need to be displayed are provided:
        if not display_keys:
            sys.stderr.write("ERROR! You need to provide a list of column names that you want to display (display_keys)\n")
            return None
        ### Create the output file name:
        if not outfile_path:
            outfile_path = f"{protein_name}_results.pdf"
                
        ### Create a new canvas:
        self.canvas = canvas.Canvas(outfile_path)

        ### Calculate the number of pages:
        length = len(self.data_df)
        characters_per_page = characters_per_row * number_of_rows
        number_of_pages = (length + characters_per_page-1)//characters_per_page
        flag = False  # flag for excel the boundary of the sequence

        ### Set the spacing between letters:
        x_step = self.font_size * (0.145/12)

        ### Calculate the batch num (a batch = score_bar + display_keys)
        batch_size = 1 + len(display_keys)

        ### Print the chain name
        self.canvas.setFont("Helvetica-Bold",self.font_size+5)
        self.canvas.setLineWidth(2)
        self.canvas.setStrokeColorRGB(0,0,0,alpha=1)
        self.canvas.drawString(1.2*inch,10.8*inch,protein_name)

        if self.domain_dict:
            domains = list(self.domain_dict.keys())
            # 0 means no domain exist
            domain_flags = [0 for dnum in range(len(domains))]

        ### Initiate the nearest domain
        nearest_domain_index = 0
        nearest_domain_shift = 0
                             
        ### Checking if certain data types exist in the dataframe.
        ### If you want to add information about where cross-linked
        ### peptides or amino acids are, the column names should be as follows:
        peptide_coordinates = list()
        if 'cross_linked_peptides' in self.data_df.columns:
            peptide_coordinates = self.data_df.loc[self.data_df['cross_linked_peptides'] == 1].index

        for n in range(number_of_pages):
            for k in range(number_of_rows):
                ### start of a row
                x = 1.9
                if flag:
                    break
                for i in range(characters_per_row):
                    ### Index start from 1
                    index = n * characters_per_page + k * characters_per_row + i + 1
                    ### Check availability
                    if index >= length:
                        flag = True
                             
                        ### Check if any domains end here
                        if self.domain_dict:
                            for dnum in range(len(domains)):
                                if domain_flags[dnum] > 0:
                                    counts = index - domain_flags[dnum]
                                    y_start = 10.4 - self.font_size * (0 + 0.2*k)
                             
                                    ### Judge if we need shift the label to avoid overlap
                                    if domain_flags[dnum] - nearest_domain_index < 6:
                                        shift = nearest_domain_shift + 1
                                    else:
                                        shift, nearest_domain_shift = 0, 0
                             
                                    ### Draw a ractangle to represent the domain
                                    self.drawDomain(x,y_start,x_step,counts,domains[dnum],dnum,shift)
                                    nearest_domain_index = domain_flags[dnum]
                                    nearest_domain_shift = shift
                                    domain_flags[dnum] = -1
                        break       

                    ### Check if any domains end here before do other things
                    if self.domain_dict:
                        for dnum in range(len(domains)):
                            if domain_flags[dnum] > 0:
                                if index not in self.domain_dict[domains[dnum]]:                            
                                    counts = index - domain_flags[dnum]
                                    y_start = 10.4 - self.font_size * (0 + 0.2*k)
                                    
                                    ### Judge if we need shift the lable to avoid overlap
                                    if domain_flags[dnum] - nearest_domain_index < 6:
                                        shift = nearest_domain_shift + 1
                                    else:
                                        shift, nearest_domain_shift = 0, 0
                                    self.drawDomain(x,y_start,x_step,counts,domains[dnum],dnum,shift)
                                    nearest_domain_index = domain_flags[dnum]
                                    nearest_domain_shift = shift
                                    domain_flags[dnum] = -1
                            elif index in self.domain_dict[domains[dnum]]:
                                ### Which means a new domain start here
                                domain_flags[dnum] = index

                    ### Extract the information of a specific index 
                    letter = str(self.data_df.loc[index,'sequence'])   

                    x = 1.9 + i*x_step 
                    y = 10.4 - self.font_size * (0.2*k + 0.03)
                             
                    ### Draw a bar represent the score of agreement
                    ### Check if predictions from an ML model were included:
                    if 'predictions' in self.data_df.columns:
                        score = float(self.data_df.at[index,'predictions'])/100.0
                        self.printScoreBar(x,y,x_step,score)
                             
                    for j in range(batch_size):
                        h_score = self.data_df.at[index,display_keys[j-1]] if j>0 else 1
                        y_start = 10.4 - self.font_size * (0.015*j + 0.2*k + 0.03)
                        y = y_start
                        if j == 0 or h_score >= 0.80:
                            r,g,b = self.eye_friendly_colors[j]
                            
                            ### Check whether a cross-linked peptide mapped here
                            if j == 0 and index in peptide_coordinates:
                                self.highlight(x,y+0.03*self.font_size, x_step)
                        else:
                            r,g,b = 0.8,0.8,0.8
                             
                        y += 0.03*self.font_size if j == 0 else 0
                        self.printRGBL(x,y,x_step,'-' if h_score==-1 else letter, r, g, b)

                        ### Print display_keys as header
                        if i == 0:
                            x_start = 0.6
                            self.canvas.setFont("Helvetica", self.font_size-1)    
                            self.canvas.setFillColorRGB(0, 0, 0)
                            self.canvas.drawString(x_start*inch,y_start*inch,'scoreBar' if j<1 else display_keys[j-1])                           

                ### End of a row
                ### Check if any domains end here
                if self.domain_dict:
                    for dnum in range(len(domains)):
                        if domain_flags[dnum] > 0:
                            ### The formular for counts is a little different from other situation
                            counts = index - domain_flags[dnum] + 1
                            y_start = 10.4 - self.font_size * (0 + 0.2*k)
                            if domain_flags[dnum] - nearest_domain_index < 6:
                                shift = nearest_domain_shift + 1
                            else:
                                shift, nearest_domain_shift = 0, 0                            
                            self.drawDomain(x,y_start,x_step,counts,domains[dnum],dnum,shift) 
                            nearest_domain_index = domain_flags[dnum]
                            nearest_domain_shift = shift
                            domain_flags[dnum] = -1

            self.canvas.showPage()  ### Save the canvas as a new page

        self.canvas.save()  ### Save the file
        return True
