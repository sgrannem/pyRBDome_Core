__author__              = ["Niki Christopoulou","Sander Granneman"]
__copyright__           = "Copyright 2021"
__version__             = "0.1.0"
__credits__             = ["Niki Christopoulou","Sander Granneman"]
__maintainer__          = "Sander Granneman"
__email__               = "Sander.Granneman@ed.ac.uk"
__status__              = "beta"

##################################################################################
#
#       PDBFinder
#
#
#       Copyright (c) Niki Christopoulou and Sander Granneman 2021
#
#       Permission is hereby granted, free of charge, to any person obtaining a copy
#       of this software and associated documentation files (the "Software"), to deal
#       in the Software without restriction, including without limitation the rights
#       to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#       copies of the Software, and to permit persons to whom the Software is
#       furnished to do so, subject to the following conditions:
#
#       The above copyright notice and this permission notice shall be included in
#       all copies or substantial portions of the Software.
#
#       THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#       IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#       FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#       AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#       LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#       OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#       THE SOFTWARE.
#
##################################################################################

from collections import defaultdict
from pypdb import *
from pyRBDome.Classes.PDBAnalyser import *
from pyRBDome.Classes.PyPDBResults import *
from pyRBDome.Functions.RBDomeSQL import *
from GenbankParser.Classes.ParseGenbank import *

class FindPDB(PDBAnalyser):
    """ A class for finding and downloading PDB files from rcsb.org.
    Note that by default it strips away any data other than ATOM and
    HETATM and ANISOU data as this makes downstream processing steps much simpler """

    def __init__(self,verbose=True,pdb_dir="pdb_files",sequence_identity=1.0,sequence_coverage=0.7,database=True,database_verbose=False,database_table='pyrbdome_analysis',database_name='pyrbdome.db'):
        """ Starts the class and creates """
        super().__init__()
        self.results = pd.DataFrame()
        self.verbose = verbose
        self.downloaded_pdbs = list()
        self.no_pdbs_found = list()
        self.not_downloaded_pdbs =  list()
        self.pdb_dir = pdb_dir
        self.sequence_identity = sequence_identity
        self.sequence_coverage = sequence_coverage
        self.database = database
        self.database_verbose = database_verbose
        self.database_table = database_table
        self.database_name = database_name
        if database:
            #Add columns to the database. They will be populated later when calling some of the class methods
            addEmptyColumn('chains', self.database_table, self.database_name,verbose=database_verbose)
            # the column pdb_id will have either the rcsb or the swissmodel ID, whichever of the two will be used for the analyses
            addEmptyColumn('pdb_id', self.database_table, self.database_name,verbose=database_verbose)
            addEmptyColumn('pdb_downloaded', self.database_table, self.database_name, verbose=database_verbose)

    def searchForPDBsByGenBank(self,data,genbankfile):
        """ Creatres a dataframe containing PDB_ID for the entries of the input data.
        The dataframe created also contains a column with the sequence of each protein.
        The dataframe is stored in the 'data' variable.
        You need to provide as input:
            - either a csv file containing your parsed data (that can be created using the DataFileParser class)
            - or a pandas dataframe
        The file or the dataframe MUST contain a column named 'ID' with the ID of each protein and a column named 'sequence' with the sequence of each protein.
        Also need to provide the genbank file for the organism you study."""

        #proteins = pd.read_csv(parsed_datafile, index_col='ID')
        #unique_id = sorted(set(proteins.index))
        #self.results = pd.DataFrame(index=unique_id)
        
        ### the if-elif block of code below is so that we can have as input either a parsed datafile (where data will be a string) or a dataframe that will contain the ID of the proteins either in the index column or in a column named 'ID'
        if type(data) is str:
            proteins = pd.read_csv(data, index_col='ID')
            unique_id = sorted(set(proteins.index))
            self.results = pd.DataFrame(index=unique_id)
        elif type(data) is pd.DataFrame:
            if 'ID' in data.columns:
                #this is if the ID is in a column of the dataframe and not already the index of it
                proteins = data.set_index('ID')
                unique_id = sorted(set(data['ID']))
                self.results = pd.DataFrame(index=unique_id)
            else:
                #this is if the ID is already the index of dataframe
                proteins = data
                unique_id = sorted(set(data.index))
                self.results = pd.DataFrame(index=unique_id)
                
        self.results['sequence'] = None
        self.results['sequence_coverage'] = 0
        parser = ParseGenbank(genbankfile)
        sequences = list()
        #notfound = defaultdict(str)

        ### Submitting data to rcsb.org
        
        if len(self.results.index)>0:

            if self.verbose:
                sys.stdout.write("Submitting jobs to rcsb.org\n")

            for i in self.results.index:
                sequence = parser.getProteinSequence(i)
                self.results.loc[i,'sequence'] = sequence
                #Populate the column 'sequence' of the sqlite database
                if self.database:
                    updateColumn(self.database_table, 'sequence == "%s"' % sequence, 'ID == "%s"' % i , self.database_name, verbose = self.database_verbose)
                try:
                    q = Query(sequence,query_type="sequence",return_type="polymer_entity")
                except:
                    sys.stderr.write("ERROR!!!! could not find a pdb file for %s in rcsb.org\n" % i)
                    notfound[i] = sequence
                    #Populate the column rcsb_id of the sqlite database
                    if self.database:
                        updateColumn(self.database_table, 'rcsb_id == "ERROR"', 'ID == "%s"' % i , self.database_name, verbose = self.database_verbose)
                else:
                    try:
                        results = ParsePyPDBResults(q.search())
                        if results.resultsdict:
                            top_result = results.getTopMatchingResult()
                            columns = top_result.index
                            values = top_result.values
                            self.results.loc[i,columns] = values
                            #Populate the column rcsb_id of the sqlite database
                            if self.database:
                                updateColumn(self.database_table, 'rcsb_id == "%s"' % values[0], 'ID == "%s"' % i , self.database_name, verbose = self.database_verbose)
                        else:
                            #Populate the column rcsb_id of the sqlite database
                            if self.database:
                                updateColumn(self.database_table, 'rcsb_id == "ERROR"', 'ID == "%s"' % i , self.database_name, verbose = self.database_verbose)
                            continue
                    except AttributeError:
                        continue

            ### Reformatting the data.
            self.results.index.name = 'ID'
            
            ## the if statement is for the case where the Query did not work for any of the proteins, so there is not search related columns in the results dataframe and this would give an error.
            
            if ('PDB_ID' in self.results.columns) and  ('alignment_length' in self.results.columns) and ('query_length' in self.results.columns):
                self.results.rename(columns={"PDB_ID": "pdb_id"},inplace=True)
                self.results['sequence_coverage'] = self.results['alignment_length'] / self.results['query_length']
                #self.notfound = pd.DataFrame.from_dict(notfound, orient='index', columns=['sequence'])
            if self.verbose:
                sys.stdout.write("Downloaded and reformatted the results.\n")
            return True
        else:
            if self.verbose:
                sys.stderr.write("Dataframe or datafile provided has no protein entries \n")
            
            return False
    
    def searchForPDBsBySequence(self,data):
        """ Creates a dataframe containing PDB_ID for the entries of the input data.
        The dataframe created also contains a column with the sequence of each protein.
        The dataframe is stored in the 'data' variable.
        You need to provide as input:
            - either a csv file
            - or a pandas dataframe
        The file or the dataframe MUST contain a column named 'ID' with the ID of each protein and a column named 'sequence' with the sequence of each protein."""

        #proteins = pd.read_csv(parsed_datafile, index_col='ID')
        #unique_id = sorted(set(proteins.index))
        #self.results = pd.DataFrame(index=unique_id)
        
        ### the if-elif block of code below is so that we can have as input either a parsed datafile (where data will be a string) or a dataframe that will contain the ID of the proteins either in the index column or in a column named 'ID'
        if type(data) is str:
            proteins = pd.read_csv(data, index_col='ID')
            unique_id = sorted(set(proteins.index))
            self.results = pd.DataFrame(index=unique_id)
        elif type(data) is pd.DataFrame:
            if 'ID' in data.columns:
                proteins = data.set_index('ID')
                unique_id = sorted(set(data['ID']))
                self.results = pd.DataFrame(index=unique_id)
            else:
                proteins = data
                unique_id = sorted(set(data.index))
                self.results = pd.DataFrame(index=unique_id)
                
        self.results['sequence'] = None
        self.results['sequence_coverage'] = 0

        ### Submitting data to rcsb.org
        
        if len(self.results.index)>0:

            if self.verbose:
                sys.stdout.write("Submitting jobs to rcsb.org\n")

            for i in self.results.index:
                sequence = proteins.loc[proteins.index==i,'sequence'][0]
                self.results.loc[i,'sequence'] = sequence
                #Populate the column 'sequence' of the sqlite database
                if self.database:
                    updateColumn(self.database_table, 'sequence == "%s"' % sequence, 'ID == "%s"' % i , self.database_name, verbose = self.database_verbose)
                try:
                    q = Query(sequence,query_type="sequence",return_type="polymer_entity")
                except:
                    sys.stderr.write("ERROR!!!! could not find a pdb file for %s in rcsb.org\n" % i)
                    notfound[i] = sequence
                    #Populate the column rcsb_id of the sqlite database
                    if self.database:
                        updateColumn(self.database_table, 'rcsb_id == "ERROR"', 'ID == "%s"' % i , self.database_name, verbose = self.database_verbose)
                else:
                    try:
                        results = ParsePyPDBResults(q.search())
                        if results.resultsdict:
                            top_result = results.getTopMatchingResult()
                            columns = top_result.index
                            values = top_result.values
                            self.results.loc[i,columns] = values
                            #Populate the column rcsb_id of the sqlite database
                            if self.database:
                                updateColumn(self.database_table, 'rcsb_id == "%s"' % values[0], 'ID == "%s"' % i , self.database_name, verbose = self.database_verbose)
                        else:
                            #Populate the column rcsb_id of the sqlite database
                            if self.database:
                                updateColumn(self.database_table, 'rcsb_id == "ERROR"', 'ID == "%s"' % i , self.database_name, verbose = self.database_verbose)
                            continue
                    except AttributeError:
                        continue

            ### Reformatting the data:
            self.results.index.name = 'ID'
            
            ## the if statement is for the case where the Query did not work for any of the proteins, so there is not search related columns in the results dataframe and this would give an error.
            if ('PDB_ID' in self.results.columns) and  ('alignment_length' in self.results.columns) and ('query_length' in self.results.columns):
                self.results.rename(columns={"PDB_ID": "pdb_id"},inplace=True)
                self.results['sequence_coverage'] = self.results['alignment_length'] / self.results['query_length']
                #self.notfound = pd.DataFrame.from_dict(notfound, orient='index', columns=['sequence'])
            if self.verbose:
                sys.stdout.write("Downloaded and reformatted the results.\n")

            return True
        
        else:
            if self.verbose:
                sys.stderr.write("Dataframe or datafile provided has no protein entries \n")
            return False

    def exportAllData(self,outputfile='PDB_table.txt'):
        """ Exports a csv file containing PDB_ID for the entries of the input data,
        various scores for the search on rcsb and the sequence of each protein (extracted from the genbank file).
        Do not forget to provide a name for the output file ('outputfile' argument). If a name is not provided, the file
        is called 'PDB_table.txt' by default."""

        self.results.to_csv(outputfile,sep="\t")

        if self.verbose:
            sys.stdout.write('%s was created.\n' % outputfile)

        return True

    def downloadPDBs(self,records=['ATOM','HETATM'],remove_ligands=True,remove_non_protein=False,overwrite=False):
        """ Downloads pdb files for the proteins that have a match on rcsb.
        - A minimum sequence identity score and a minimum sequence coverage score can be provided when initiating the class. These are set to 1.0 and 0.7 by default.
        - You can select which entries of the PDB file you want to keep ('keeprecords' argument. It should be a list containing at least one of these elements: ['ATOM','HETATM','ANISOU','OTHERS']). It is set by default to
        records=['ATOM','HETATM']. 
        - You can also remove ligands by setting remove_ligands=True. 
        - By default, it won't download pdb files that already exist in the pdb directory. But by setting overwrite to 'True' you can force the program to overwrite any existing pdb files. """

        if not os.path.exists(self.pdb_dir):
            os.makdirs(self.pdb_dir)

        pdb = PDBAnalyser()

        for i in self.results.index:
            pdb_id = self.results.loc[i,'pdb_id']
            if not pdb_id: ### In case no model structure was found.
                self.no_pdbs_found.append(i)
            else:
                seq_identity_score = self.results.loc[i,'sequence_identity']
                seq_coverage_score = self.results.loc[i,'sequence_coverage']

                if (seq_coverage_score >= self.sequence_coverage) and (seq_identity_score >= self.sequence_identity):
                    pdb_outfile_name = "%s/%s.pdb" % (self.pdb_dir,pdb_id)
                    ### Only download files that do not exist!
                    if (os.path.isfile(pdb_outfile_name) and not overwrite):
                        if self.verbose:
                            sys.stderr.write("The pdb file for %s already exists in the %s directory\n" % (pdb_id,self.pdb_dir))
                        self.downloaded_pdbs.append(pdb_id)
                        #Populate the column pdb_downloaded of the sqlite database
                        if self.database:
                            updateColumn(self.database_table, 
                                        'pdb_downloaded == "Yes"', 'ID == "%s"' % i , 
                                        self.database_name,
                                        verbose = self.database_verbose)
                    else:
                        ### Checking if the pdb file is available for these proteins:
                        try:
                            pdb.fetchPDBFile(pdb_id,records=records,remove_ligands=remove_ligands)
                            pdb.writePDBFile(outfilename=pdb_outfile_name,compress=False)
                            if self.verbose:
                                sys.stdout.write("Downloaded pdb file for %s\n" % pdb_id)
                            self.downloaded_pdbs.append(pdb_id)
                            #Populate the column pdb_downloaded of the sqlite database
                            if self.database:
                                updateColumn(self.database_table, 
                                            'pdb_downloaded == "Yes"', 'ID == "%s"' % i ,
                                            self.database_name,
                                            verbose = self.database_verbose)
                        except:
                            if self.verbose:
                                sys.stderr.write("Download of pdb file for %s has failed\n" % pdb_id)
                            #Populate the column pdb_downloaded of the sqlite database
                            if self.database:
                                updateColumn(self.database_table, 
                                            'pdb_downloaded == "ERROR"', 'ID == "%s"' % i, 
                                            self.database_name, 
                                            verbose = self.database_verbose)
                            # Downloading CIF files works but this format is not compatible with downstream analyses.
                            # Therefore we won't be downloading any cif files.
                            #try:
                            #    pdb.fetchCIFFile(pdb_id,records=records,remove_ligands=remove_ligands)
                            #    pdb.pdb_df = pdb.toPDBformat()
                            #    if remove_non_protein:
                            #        pdb.pdb_df = pdb.removeNonProteinChains()
                            #    pdb.writePDBFile(outfilename=pdb_outfile_name,compress=False)
                            #    if self.verbose:
                            #        sys.stdout.write("Downloaded cif file for %s and converted it to pdb format\n" % pdb_id)
                            #    self.downloaded_pdbs.append(pdb_id)
                            #    #Populate the column pdb_downloaded of the sqlite database
                            #    if self.database:
                            #        updateColumn(self.database_table, 'pdb_downloaded == "Yes"', 'ID == "%s"' % i , self.database_name, verbose = self.database_verbose)
                            #except:
                            #    self.not_downloaded_pdbs.append(pdb_id)
                            #    if self.verbose:
                            #        sys.stderr.write("Download of pdb files for %s failed!\n" % pdb_id)
                            #    #Populate the column pdb_downloaded of the sqlite database
                            #    if self.database:
                            #        updateColumn(self.database_table, 'pdb_downloaded == "ERROR"', 'ID == "%s"' % i , self.database_name, verbose = self.database_verbose)
                else:
                    self.not_downloaded_pdbs.append(pdb_id)
                    #Populate the column pdb_id of the sqlite database
                    if self.database:
                        updateColumn(self.database_table, 
                                    'pdb_downloaded == "Score not high enough to download"', 'ID == "%s"' % i ,
                                    self.database_name,
                                    verbose = self.database_verbose)
                        
        return True

    def exportUnsuccessfulProteins(self,outputfile='Unsuccessful_proteins.csv'):
        """ Exports a csv file containing the data for all the proteins for which a PDB file was not downloaded,
        for various reasons:
        a) a match was found on rcsb, but the sequence identity score was not above the set threshold
        b) a match was found on rcsb, but the sequence coverage score was not above the set threshold
        """

        filtered = self.results[(self.results['sequence_coverage'] < self.sequence_coverage) |
                                (self.results['sequence_identity'] < self.sequence_identity)]

        filtered.to_csv(outputfile,sep="\t")

        if self.verbose:
            sys.stdout.write('%s was created.\n' % outputfile)

        return True

    def exportSuccessfulProteins(self,outputfile='Successful_proteins.csv'):
        """ Exports a csv file containing the data for all the proteins for which a PDB file was downloaded. """

        successful = self.results[self.results['pdb_id'].isin(self.downloaded_pdbs)]
        successful.to_csv(outputfile,sep="\t")
        if self.verbose:
            sys.stdout.write("File %s successfully created\n" % outputfile)
        return True

    def unsuccessfulProteinsFASTA(self,outputfile="Unsuccessful_proteins.fasta"):
        """ Exports a fasta file containing the ID and sequence for all the proteins for which a PDB file was not downloaded,
        for various reasons:
        a) a match was found on rcsb, but the sequence identity score was not above the set threshold
        b) a match was found on rcsb, but the sequence coverage score was not above the set threshold
        """

        filtered = self.results[(self.results['sequence_coverage'] < self.sequence_coverage) |
                                (self.results['sequence_identity'] < self.sequence_identity)]
        fasta_file = open(outputfile,"w")
        for i in filtered.index:
            sequence = filtered.loc[i,'sequence']
            fasta_file.write(">%s\n%s\n" % (i,sequence))
        fasta_file.close()

        if self.verbose:
            sys.stdout.write("File %s successfully created\n" % outputfile)
        return True

    def successfulProteinsFASTA(self,outputfile="Successful_proteins.fasta"):
        """ Exports a fasta file containing the ID and sequence for all the proteins for which a PDB file was downloaded. """
        
        successful = self.results[self.results['pdb_id'].isin(self.downloaded_pdbs)]
        
        fasta_file = open(outputfile,"w")
        for i in successful.index:
            sequence = successful.loc[i,'sequence']
            fasta_file.write(">%s\n%s\n" % (i,sequence))
        fasta_file.close()

        if self.verbose:
            sys.stdout.write("File %s successfully created\n" % outputfile)
        return True
