AUTHORS =  ["Sander Granneman",
            "Niki Christopoulou",
            "Hugh McCaughan",
            "Liang-Cui Chu",
            "Vlad Litvin",
            "Salome Brunon"]

__author__		= AUTHORS
__copyright__	= "Copyright 2021"
__version__		= "0.0.2"
__credits__		= AUTHORS
__maintainer__	= "Sander Granneman"
__email__		= "Sander.Granneman@ed.ac.uk"
__status__		= "beta"

##################################################################################
#
#	PeptideAnalyser
#
#
#	Copyright (c) Sander Granneman 2021
#
#	Permission is hereby granted, free of charge, to any person obtaining a copy
#	of this software and associated documentation files (the "Software"), to deal
#	in the Software without restriction, including without limitation the rights
#	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#	copies of the Software, and to permit persons to whom the Software is
#	furnished to do so, subject to the following conditions:
#
#	The above copyright notice and this permission notice shall be included in
#	all copies or substantial portions of the Software.
#
#	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#	THE SOFTWARE.
#
##################################################################################

import pandas as pd
import sys
import re
import regex
from pathlib import Path
from scipy import spatial
from biopandas.pdb import PandasPdb
from biopandas.pdb.engines import *
from pyRBDome.Classes.PDBAnalyser import *
from pyRBDome.Classes.CIFAnalyser import *

class PeptidePDBAnalyser(CIFAnalyser):
    """ A class for obtaining peptide information from PDB files """

    def __init__(self):
        """ passing the PDBAnalyser and CIFAnalyser class objects to the
        PeptidePDBAnalyser class """
        super().__init__()
        self.mapped_peptides = pd.DataFrame()

    @staticmethod
    def peptideSearch(peptide,proteinseq):
        """ Uses regex module to search for peptide in the protein and returns
        the start and end position of that peptide in the sequence, one substitution
        and one insertion in the sequence is allowed. Returns 1-based coordinates of
        peptide positions in the protein sequence."""

        positions = list()
        if re.search('%s' % peptide,proteinseq): ### In case there is a perfect match
            search_result = re.search('%s' % peptide,proteinseq).group(0)
        elif regex.search('(?:%s){s<=1:[a-zA-Z]}' % peptide,proteinseq): ### In case there are insertions, substitutions
            search_result = regex.search('(?:%s){s<=1:[a-zA-Z]}' % peptide,proteinseq).group(0)
        elif regex.search('(?:%s){i<=1:[a-zA-Z]}' % peptide,proteinseq): ### In case there are insertions, substitutions
            search_result = regex.search('(?:%s){i<=1:[a-zA-Z]}' % peptide,proteinseq).group(0)
        elif regex.search('(?:%s){d<=1:[a-zA-Z]}' % peptide,proteinseq): ### In case there are insertions, substitutions and/or deletions
            search_result = regex.search('(?:%s){d<=1:[a-zA-Z]}' % peptide,proteinseq).group(0)
        elif regex.search('(?:%s){d<=2:[a-zA-Z]}' % peptide,proteinseq): ### In case there are insertions, substitutions and/or deletions
            search_result = regex.search('(?:%s){d<=2:[a-zA-Z]}' % peptide,proteinseq).group(0)
        elif regex.search('(?:%s){s<=1,i<=1:[a-zA-Z]}' % peptide,proteinseq): ### In case there are insertions, substitutions and/or deletions
            search_result = regex.search('(?:%s){s<=1,i<=1:[a-zA-Z]}' % peptide,proteinseq).group(0)
        else:
            search_result = None
        if search_result:
            for match in re.finditer(search_result,proteinseq):
                positions.append((match.start(),match.end()))
        return positions

    def mapPeptidesToStructures(self,pdb_id,chains,peptides):
        """ Maps the peptides in the data files to the available protein structures.
        It then returns a DataFrame with the results."""

        self.mapped_peptides = pd.DataFrame(columns=['pdb_id','chains','Peptide','Found_peptide'])

        pdb_df = self.pdb_df[~self.pdb_df.index.duplicated(keep='first')]

        for peptide in peptides:
            for chain in chains:
                proteinseq = self.getPDBProteinSequence(chain)
                positions = self.peptideSearch(peptide,proteinseq)
                if len(positions) > 1:
                    sys.stderr.write("WARNING!!! Peptide %s found at multiple positions %s in chain %s! Skipping!\n"
                                     % (peptide,positions,chain))
                    self.mapped_peptides = pd.concat([self.mapped_peptides,
                                                     pd.DataFrame(
                                                         {'pdb_id':[pdb_id],
                                                          'chains':[chains],
                                                          'Peptide':[peptide],
                                                          'Found_peptide':['unambiguous']})],
                                                     ignore_index=True)
                elif len(positions) == 1:
                    start,end = positions[0]
                    selection = PDBAnalyser()
                    selection.makeNewPDBDataFrame()
                    selection.pdb_df = pdb_df[pdb_df['chain_id'] == chain].iloc[start:end,:]
                    sequence = selection.getPDBProteinSequence(chain).lower()
                    if len(sequence) != end-start:
                        sys.stderr.write("The length of the sequence %s is not the same as the length of the peptide: %s!\n" % (sequence,end-start))
                    start = selection.pdb_df.index[0]
                    end = selection.pdb_df.index[-1]
                    if len(sequence) != len(selection.pdb_df.index):
                        sys.stderr.write("The length of the sequence %s is not the same as the length of the seqeunce in the pdb file: %s\n"
                                         % (sequence,len(selection.pdb_df.index)))
                    outstring = ("%s_%s_%s" % (start,sequence,end))
                    self.mapped_peptides = pd.concat([self.mapped_peptides, 
                                                      pd.DataFrame(
                                                          {'pdb_id':[pdb_id],
                                                           'chains':[chains],
                                                           'Peptide':[peptide],
                                                           'Found_peptide':[outstring]})],
                                                     ignore_index=True)
                    
                else:
                    sys.stderr.write("ERROR! Could not map peptide %s to the structure %s.\n" % (peptide,pdb_id))
                    self.mapped_peptides = pd.concat([self.mapped_peptides,
                                                  pd.DataFrame(
                                                      {'pdb_id':[pdb_id],
                                                       'chains':[chains],
                                                       'Peptide':[peptide],
                                                       'Found_peptide':['not_found']})],
                                                 ignore_index=True)
    
        return True

    def highlightPeptideAminoAcidsByScore(self,score=10,header='aaRNA_results'):
        """ Looks for a peptide in the protein sequence of the pdb_file and asks if there are any
        amino acids in the sequence that have a high scores in the analyses of the peptides.
        Before you can run this function you need to run the mapPeptidesToStructures function
        first as this returns a table (called mapped_peptides) that contains information
        about where the peptides were mappted to in the structure. This information is
        needed before the peptides can be annotated based on their scores.
        The scores are usually placed in the b-factor column of the pdb file.
        It returns the peptide sequence it found in lower case and puts the amino acids
        with high scores in uppercase."""

        pdb_df = self.collapsePDBbyBfactorValue()

        def findAminoAcidsWithScore(peptide):
            # **findAminoAcidsWithScore method is here because it is only used inside highlightPeptideAminoAcidsByScore**
            """ Returns the peptide string with amino acids with scores
            equal or higher than the threshold in uppercase. """
            peptideinfo = self.getPeptideInfo(peptide)
            if peptideinfo:
                sequence,start,end = peptideinfo
                aminoacids = list(sequence)
                try:
                    peptide_coordinates = pdb_df.loc[start:end,:]
                except:
                    sys.stderr.write("ERROR! Could not find the coordinates for the peptide %s in the pdb file!\n" % peptide)
                else:
                    count = 0
                    for i in peptide_coordinates.index:
                        try:
                            if peptide_coordinates.loc[i,'b_factor'] >= score:
                                aminoacids[count] = aminoacids[count].upper()
                        except IndexError:
                            sys.stderr.write("ERROR! Could not figure out where the binding site is in the peptide %s\n" % "".join(peptide))
                            return 'no_data'
                        count += 1

                    newpeptide = "".join(aminoacids)
                    return "%s_%s_%s" % (start,newpeptide,end)
            else:
                return peptide

        if self.mapped_peptides.empty:
            sys.stderr.write("Please run the mapPeptidesToStructures function with your list\
            of peptides before you run this function!\n")
            return False
        else:
            self.mapped_peptides[header] = list(map(findAminoAcidsWithScore,self.mapped_peptides['Found_peptide']))
            return True

    def calculateMinPeptideDistanceToReferences(self,referencepoints,peptide):
        """ Calculates the minimal distance between reference points and start and end
        positions of a peptide sequence"""

        ### Making sure the reference points are floats:
        referencepoints = referencepoints.astype(float)
        
        ### Extracting pdb mapping positions from the peptide:
        if len(peptide.split("_")) == 5:
            pepdtideseq,start,end = self.getPeptideInfo(peptide)
        else:
            return 'no_data'

        ### Getting peptide coordinates from the pdb file:
        try:
            pdbdata = self.pdb_df.loc[start:end,['x_coord','y_coord','z_coord']]
        except:
            sys.stderr.write("ERROR! Could not find the peptide %s in the pdb file\n" % peptide)
            return 'no_data'
        else:
            ### Function for calculating the minimum euclidian distance:
            tree = spatial.cKDTree(referencepoints)
            return min(tree.query(np.array(pdbdata.values))[0],default="EMPTY")


    def measureAllPeptideDistancesToReferences(self,referencepoints,analysistype="aaRNA_distances"):
        """ This function calculates distances beween peptides from the mapped_peptides
        and any references points that you provide. These reference points
        could be 3D coordinates from predicted ligand-binding sites, for example.
        This function then updates the mapped_peptides dataframe with the distances
        to the reference points in Å. The anaylsistype variable is essentially the name
        of the column in the mapped_peptides where the distance measurements will
        be stored. """

        ### Extracting the peptide sequences from the data:
        peptides = self.mapped_peptides.loc[:,'Found_peptide'].values

        ### Calculatig the shortest distance from the peptide coordinates
        ### to neighbouring references points:

        mindistances = [self.calculateMinPeptideDistanceToReferences(referencepoints,peptide) for peptide in peptides]

        ### Updating the mapped_peptides dataframe with the distances:
        self.mapped_peptides[analysistype] = mindistances

        return True

    def getPeptideInfo(self,peptide,split_coordinates=False):
        """ Grabs the peptide string, coordinates in PDB file and chain
        from the analysis results table. The input string should be
        formatted like below:

        69_A_yfYsnrGPviDyenqelvhfffneLskyvk_98_A

        This function should then return:

        (yfYsnrGPviDyenqelvhfffneLskyvk,69,98,A)

        """

        if len(peptide.split("_")) == 5:
            startinfo,chain,sequence,endinfo,_ = peptide.split("_")
            if split_coordinates:
                return (sequence,int(startinfo),int(endinfo),chain)
            else:
                return (sequence,f"{startinfo}_{chain}",f"{endinfo}_{chain}")
        else:
            return None

    def getPeptideCoordinatesFromPDBFile(self,peptides):
        """ Takes mapped peptide sequences with attached pdb coordinates,extracts
        the coordinates from the pdb file and puts the coordiantes of the
        individual peptides in a new pdb file.

        This function expects the peptide string to have the following format:

        69_A_yfYsnrGPviDyenqelvhfffneLskyvk_98_A

        Where 69_A and 98_A represent the indices of the location of the
        peptide in the self.pdb_df dataframe. 69 is the startpostion and
        98 is the end position in the chain and 'A' here indicates the chain
        itself.

        """

        results_df = pd.DataFrame(columns=self.pdb_df.columns)

        for peptide in peptides:
            ### Grabbing coordinates from peptide sequences from the pdb file:
            try:
                sequence,start,end,chain = self.getPeptideInfo(peptide,split_coordinates=True)
                #print(sequence,start,end,chain)
            except:
                ### This means that the peptide was probably unambiguous
                sys.stderr.write("ERROR! Peptide %s was not mapped to the structure!\n" % peptide)
            else:
                coordinates = self.pdb_df[(self.pdb_df.loc[:,'chain_id'] == chain) &
                              (self.pdb_df.loc[:,'residue_number'] >= start) &
                              (self.pdb_df.loc[:,'residue_number'] <= end)]

                results_df = pd.concat([results_df,coordinates],ignore_index=True)

        return results_df
