__author__		= "Sander Granneman"
__copyright__	= "Copyright 2020"
__version__		= "0.0.1"
__credits__		= "Sander Granneman"
__maintainer__	= "Sander Granneman"
__email__		= "Sander.Granneman@ed.ac.uk"
__status__		= "beta"

##################################################################################
#
#	CIFAnalyser
#
#
#	Copyright (c) Sander Granneman 2020
#
#	Permission is hereby granted, free of charge, to any person obtaining a copy
#	of this software and associated documentation files (the "Software"), to deal
#	in the Software without restriction, including without limitation the rights
#	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#	copies of the Software, and to permit persons to whom the Software is
#	furnished to do so, subject to the following conditions:
#
#	The above copyright notice and this permission notice shall be included in
#	all copies or substantial portions of the Software.
#
#	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#	THE SOFTWARE.
#
##################################################################################

import pandas as pd
import sys
import re
import numpy as np
import regex
from warnings import warn
try:
    from urllib.request import urlopen
    from urllib.error import HTTPError, URLError
except ImportError:
    from urllib2 import urlopen, HTTPError, URLError  # Python 2.7 compatible
from biopandas.pdb import PandasPdb
from biopandas.pdb.engines import *
from pyRBDome.Classes.PDBAnalyser import *

class CIFAnalyser(PDBAnalyser):
    """ A class for fetching, loading and processing CIF files """

    def __init__(self):
        ### passing the PandasPdb class objects to PDBAnalyser class:
        super().__init__()

        ### setting the class variables:
        self.pandaspdb = None
        self.pdb_df = None
        self.cif_columns = ['record_name',
                            'atom_number',
                            'element_symbol',
                            'atom_name',
                            'alt_id',
                            'residue_name',
                            'chain_id',
                            'seq_id',
                            'residue_number',
                            'pdbx_PDB_ins_code',
                            'x_coord',
                            'y_coord',
                            'z_coord',
                            'occupancy',
                            'b_factor',
                            'charge',
                            'auth_seq_id',
                            'auth_comp_id',
                            'auth_asym_id',
                            'auth_atom_id',
                            'pdbx_PDB_model_num']

    def __fetch_pdb(self,pdb_code,file_type="pdb"):
        """Load PDB file from rcsb.org."""
        txt = None
        url = 'https://files.rcsb.org/download/%s.%s' % (pdb_code.lower(),file_type)
        try:
            response = urlopen(url)
            txt = response.read()
            if sys.version_info[0] >= 3:
                txt = txt.decode('utf-8')
            else:
                txt = txt.encode('ascii')
        except HTTPError as e:
            sys.stderr.write('HTTP Error %s' % e.code)
        except URLError as e:
            sys.stderr.write('URL Error %s' % e.args)
        return txt.split('\n')

    def fetchCIFFile(self,cif_file,records=['ATOM','HETATM','ANISOU'],remove_ligands=False):
        """ Fetches a cif file from rcsb.org and loads it into a pandas dataframe """

        cif_data_file = self.__fetch_pdb(cif_file,file_type='cif')
        ### Grabbing the lines starting with the selected records and splitting them:
        cif_data_file = [re.split('\s+',i.strip()) for i in cif_data_file if any(i.startswith(record) for record in records)]

        ### Converting the table to pandas dataframe:
        pdb_df = pd.DataFrame(cif_data_file,columns=self.cif_columns,dtype=float)
        pdb_df = pdb_df.convert_dtypes()
        pdb_df['index'] = pdb_df['residue_number'].astype(str) + pdb_df['chain_id'].astype(str)
        pdb_df = pdb_df.set_index('index').sort_values(by='atom_number')
        #pdb_df.loc[:,'line_idx'] = np.arange(1,len(pdb_df.index)+1)

        self.pdb_df = pdb_df

        if remove_ligands:
            self.pdb_df = self.removeAllLigands()

    def loadCIFFile(self,cif_file,records=['ATOM','HETATM','ANISOU'],remove_ligands=False):
        """ Loads the cif file into a pandas dataframe """

        cif_data_file = open(cif_file).readlines()
        cif_data_file = [re.split('\s+',i.strip()) for i in cif_data_file if any(record in i for record in records)]

        ### Converting the table to pandas dataframe:
        pdb_df = pd.DataFrame(cif_data_file,columns=self.cif_columns,dtype=float)
        pdb_df = pdb_df.convert_dtypes()
        pdb_df['index'] = pdb_df['residue_number'].astype(str) + pdb_df['chain_id'].astype(str)
        pdb_df = pdb_df.set_index('index').sort_values(by='atom_number')
        #pdb_df.loc[:,'line_idx'] = np.arange(1,len(pdb_df.index)+1)

        self.pdb_df = pdb_df

        if remove_ligands:
            self.pdb_df = self.removeAllLigands()

    def toPDBformat(self):
        """ Converts the pandas dataframe with the data from the cif file into
        the pdb format. This has to be done before the file can be written todo
        a pdb file by the writePDBFile function The minimum format for pdb needs
        to have the following columns:
        """

        columns = ['record_name', 'atom_number', 'blank_1',
                  'atom_name', 'alt_loc', 'residue_name',
                  'blank_2', 'chain_id', 'residue_number',
                  'insertion', 'blank_3', 'x_coord', 'y_coord',
                  'z_coord', 'occupancy', 'b_factor',
                  'blank_4', 'segment_id', 'element_symbol',
                  'charge','line_idx']

        converted_pdb_df = pd.DataFrame(columns=columns,
                                        index=self.pdb_df.index)
        for col in converted_pdb_df:
            if col in self.pdb_df.columns:
                converted_pdb_df.loc[:,col] = self.pdb_df.loc[:,col]

        converted_pdb_df.line_idx = np.arange(1,len(converted_pdb_df.index)+1)
        converted_pdb_df = converted_pdb_df.replace(np.nan,'')
        converted_pdb_df = converted_pdb_df.replace('?',np.nan)
        converted_pdb_df['charge'] = pd.to_numeric(converted_pdb_df['charge'])

        return converted_pdb_df
        
