AUTHORS =  ["Sander Granneman",
            "Niki Christopoulou",
            "Hugh McCaughan",
            "Liang-Cui Chu",
            "Vlad Litvin",
            "Salome Brunon"]

__author__		= AUTHORS
__copyright__	= "Copyright 2023"
__version__		= "0.0.4"
__credits__		= AUTHORS
__maintainer__	= "Sander Granneman"
__email__		= "Sander.Granneman@ed.ac.uk"
__status__		= "beta"

##################################################################################
#
#	PDBAnalyser
#
#
#	Copyright (c) Sander Granneman 2023
#
#	Permission is hereby granted, free of charge, to any person obtaining a copy
#	of this software and associated documentation files (the "Software"), to deal
#	in the Software without restriction, including without limitation the rights
#	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#	copies of the Software, and to permit persons to whom the Software is
#	furnished to do so, subject to the following conditions:
#
#	The above copyright notice and this permission notice shall be included in
#	all copies or substantial portions of the Software.
#
#	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#	THE SOFTWARE.
#
##################################################################################

import pandas as pd
import sys
import re
import numpy as np
import regex
from biopandas.pdb import PandasPdb
from biopandas.pdb.engines import *

class PDBAnalyser(PandasPdb):
    """ A class for loading and processing PDB files """

    def __init__(self):
        ### passing the PandasPdb class objects to PDBAnalyser class:
        super().__init__()

        ### setting the class variables:
        self.pandaspdb = None
        self.pdb_df = None

    def fetchPDBFile(self,pdb_id,records=['ATOM','HETATM',],remove_ligands=False,remove_non_protein=False):
        """ Downloads the pdb file and stores it into memory. NOTE! Only 'ATOM' or
        'HETATM' records are stored as this makes the downstream processing steps
        easier. You can also use the records argument to select them individually.Replaces
        the biopandas fetch_pdb function """

        self.fetch_pdb(pdb_id)
        self.pandaspdb = self.df
        self.pdb_df = pd.DataFrame()

        pdb_df = pd.concat([self.pandaspdb[i] for i in records]).sort_values(by='residue_number')
        pdb_df.index = ["%s%s_%s" % (a,b,c) for a,b,c in pdb_df.loc[:,['residue_number','insertion','chain_id']].values]
        pdb_df = pdb_df.sort_values(by='atom_number')

        self.pdb_df = pdb_df

        if remove_ligands:
            self.pdb_df = self.removeAllLigands()
        if remove_non_protein:
            self.pdb_df = self.removeNonProteinChains()
        return True

    def loadPDBFile(self,pdb_file,records=['ATOM','HETATM'],remove_ligands=False,remove_non_protein=False):
        """ Loads the PDB file into a pandas dataframe. NOTE! Only 'ATOM' or
        'HETATM' records are stored as this makes the downstream processing steps
        easier. You can also use the records argument to select them individually. """

        self.pandaspdb = self.read_pdb(pdb_file).df ### read_pdb is a function from biopandas!
        self.pdb_df = pd.DataFrame()

        pdb_df = pd.concat([self.pandaspdb[i] for i in records]).sort_values(by='residue_number')
        pdb_df.index = ["%s%s_%s" % (a,b,c) for a,b,c in pdb_df.loc[:,['residue_number','insertion','chain_id']].values]
        pdb_df = pdb_df.sort_values(by='atom_number')

        self.pdb_df = pdb_df

        if remove_ligands:
            self.pdb_df = self.removeAllLigands()
        if remove_non_protein:
            self.pdb_df = self.removeNonProteinChains()
        return True

    def makeNewPDBDataFrame(self):
        """ Returns an empty PandasPdb object with ATOM, HEADER and OTHERS keys.
        This function can be very useful if you simply want to select some coordinates
        and put them into a new dataframe. This function is also used to extract the
        FTMap ligand coordinates from FTMap pdb files."""

        keys = ['ATOM','HETATM','OTHERS']
        others_columns = ['record_name','entry','line_idx']

        ### Make an empty PandasPdb:
        ppdb = PandasPdb()

        ### Populate the dictionary keys and add the collumns:
        ppdb._df["OTHERS"] = pd.DataFrame(columns=list(others_columns))
        ppdb._df["ATOM"]  = pd.DataFrame(columns=list(pdb_df_columns))
        ppdb._df["HETATM"] = pd.DataFrame(columns=list(pdb_df_columns))

        return ppdb

    def writePDBFile(self,outfilename=None,records=None,compress=False,df=pd.DataFrame()):
        """ Converts the biopandas dataframe into a pdb file

        records : iterable, default: None
        A list of PDB record sections in
        ['ATOM', 'HETATM', 'ANISOU', 'OTHERS'] that are to be written.
        Writes all lines to PDB if `records=None`.
        gz : bool, default: False
        outputfilename: the name of the output file.

        This code has been adapted from the biopandas.pdb code.

        """

        ### First I need to make a copy of the self.pdb_df as I need to
        ### manipulate the data and I don't want to further mess with the
        ### self.pdb_df variable.

        if not df.empty:
            pdb_df = df
        else:
            pdb_df = self.pdb_df.copy(deep=True)

        if records:
            pdb_df = pdb_df[pdb_df['record_name'].isin(records)]
            if pdb_df.empty:
                sys.stderr.write("No data available for records %s\n" % ", ".join(records))
                return None

        for col in pdb_atomdict:
            try:
                pdb_df[col['id']] = pdb_df[col['id']].apply(col['strf'])
                pdb_df['OUT'] = pd.Series('', index=pdb_df.index)
            except:
                sys.stderr.write("The dataframe does not have the right format. Could it be a CIF file?\n")
                return None

        for c in pdb_df.columns:
            if c in {'line_idx', 'OUT'}:
                pass
            else:
                pdb_df['OUT'] = pdb_df['OUT'] + pdb_df[c]

            pdb_df.sort_values(by='line_idx',inplace=True)

            if outfilename:
                with open(outfilename,"w") as f:
                    s = pdb_df['OUT'].tolist()
                    for idx in range(len(s)):
                        if len(s[idx]) < 80:
                            s[idx] = '%s%s' % (s[idx], ' ' * (80 - len(s[idx])))
                    to_write = '\n'.join(s)
                    f.write(to_write)
        return True

    def removeLigands(self,molecules=['HOH','ZN','CA']):
        """ Function that removes specific molecules from the pdb file """

        ### You don't want to change the self.pdb_df variable because if you
        ### make a mistake you can't change it back again without reloading the
        ### pdb file again. So here it returns a slice of self.pdb_file.

        return self.pdb_df[~self.pdb_df['residue_name'].isin(molecules)]

    def removeAllLigands(self):
        """ Removes all ligands from the pdb dataframe that are not in the amino acid dict """

        amino_acids = ['ALA','ARG','ASH','ASN','ASP','ASX','CGV','CSD',
                       'CSO','CYS','CYX','GLH','GLN','GLU','GLX','GLY',
                       'HID','HIE','HIP','HIS','HYP','ILE','LEU','LYS',
                       'MET','MSE','OCS','PHE','PRO','PYL','SEL','SER',
                       'THR','TRP','TYR','VAL','YCM']

        ### You don't want to change the self.pdb_df variable because if you
        ### make a mistake you can't change it back again without reloading the
        ### pdb file again. So here it returns a slice of self.pdb_file.

        return self.pdb_df[self.pdb_df['residue_name'].isin(amino_acids)]

    def collapsePDBbyBfactorValue(self):
        """ For each atom of each residue it looks at the maximum b-factor value and removes
        those atoms that have b-factor values lower than the maximum from the pdb DataFrame.
        Essentially collapses the pdb DataFrame based on b-factor values. Useful for the
        analysis of the STP pdb files."""

        ### You don't want to change the self.pdb_df variable because if you
        ### make a mistake you can't change it back again without reloading the
        ### pdb file again. So here it returns a slice of self.pdb_file.

        #pdb_df = self.pdb_df.sort_values('b_factor',ascending=False)
        pdb_df = self.pdb_df.sort_values(['residue_number','b_factor'],ascending=[True,False])
        #pdb_df = self.pdb_df[~self.pdb_df.index.duplicated(keep='first')]
        pdb_df = pdb_df[~pdb_df.index.duplicated(keep='first')]
        pdb_df.sort_values(by='atom_number',inplace=True)
        return pdb_df

    def normaliseBfactorValues(self):
        """ Some pdb output files have b-factor columns that actually contain the results
        of the analyses. However, with STP for example, the range of b-factor values is not
        consistent between individual analyses. Therefore, by normalising them to values
        between 0 and 1 the results should be more consistent."""

        scores = self.pdb_df.loc[:,'b_factor'].values
        smallest = min(scores)
        largest = max(scores)
        normscores = [(i-smallest)/(largest-smallest) for i in scores]

        ### You don't want to change the self.pdb_df variable because if you
        ### make a mistake you can't change it back again without reloading the
        ### pdb file again. So here it returns a copy of the self.pdb_file with
        ### the normalised b-factor values.

        pdb_df = self.pdb_df.copy(deep=True)
        pdb_df.loc[:,'b_factor'] = normscores

        return pdb_df

    def getPDBProteinSequence(self,chain,warning=False):
        """ Extracts the protein sequence from the PDB file and converts
        it into single-letter code """

        aadict = {'ASH':'A','ALA':'A','ASX':'B','CYX':'C','CSD':'C','CYS':'C',
                  'CGV':'C','CSO':'C','OCS':'C','YCM':'C','ASP':'D','GLU':'E',
                  'PHE':'F','GLY':'G','HIS':'H','HID':'H','HIE':'H','HIP':'H',
                  'ILE':'I','LYS':'K','LEU':'L','MET':'M','MSE':'M','ASN':'N',
                  'PYL':'O','HYP':'P','PRO':'P','GLN':'Q','ARG':'R','SER':'S',
                  'THR':'T','SEL':'U','VAL':'V','TRP':'W','TYR':'Y','GLX':'Z',
                  'GLH':'E'}

        sequence = list()
        pdb_df = self.pdb_df[~self.pdb_df.index.duplicated(keep='first')]
        for i in pdb_df[pdb_df['chain_id'] == chain]['residue_name']:

            if i in aadict.keys():
                sequence.append(aadict[i])
            else:
                sequence.append('-')
                if warning:
                    sys.stderr.write("WARNING! Could not find residue %s in the amino acid dictionary!\n" % i)

        return ''.join(sequence)

    def getPDBNucleotideSequence(self,chain,warning=False):
        """ Extracts the sequence sequence from the PDB file """

        nuclist = ['A','U','T','C','G','a','u','t','c','g']

        sequence = list()
        pdb_df = self.pdb_df[~self.pdb_df.index.duplicated(keep='first')]
        for i in pdb_df[pdb_df['chain_id'] == chain]['residue_name']:

            if i in nuclist:
                sequence.append(i)
            else:
                sequence.append('-')
                if warning:
                    sys.stderr.write("WARNING! Residue %s is not a nucleotide!\n" % i)

        return ''.join(sequence)

    def grabReferencePoints(self,score=0.18):
        """ grabs the x_coord,y_coord and z_coords for a specific indices
        in the pdb file that have b-factor values higher than a specific
        score."""

        pdb_df = self.pdb_df[self.pdb_df['b_factor'] >= score]
        return pdb_df[['x_coord','y_coord','z_coord']].values

    def getChains(self):
        """ Returns a list of chains present in the pdb file """

        chains = sorted(set(self.pdb_df['chain_id'].values))
        return chains

    def getPDBProteinChains(self):
        """ Returns a list of chains which contain amino acid sequences """

        chains = sorted(set(self.pdb_df['chain_id'].values))
        protein_chain_list = list()

        for i in chains:
            sequence = self.getPDBProteinSequence(i)
            if '-' not in sequence:
                protein_chain_list.append(i)
            else:
                sys.stdout.write("chain %s has unknown characters in the sequence. Skipping\n" % i)

        return protein_chain_list

    def removeNonProteinChains(self):
        """ Removes any chains from a pdb file that do not have proteins in them."""

        protein_chains = self.getPDBProteinChains()
        pdb_df = self.pdb_df[self.pdb_df['chain_id'].isin(protein_chains)]
        return pdb_df
