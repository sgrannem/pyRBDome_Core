__author__		= "Sander Granneman"
__copyright__	= "Copyright 2025"
__version__		= "0.1.3"
__credits__		= ["Sander Granneman"]
__maintainer__	= "Sander Granneman"
__email__		= "Sander.Granneman@ed.ac.uk"
__status__		= "beta"

##################################################################################
#
#	BindUP class
#
#
#	Copyright (c) Sander Granneman 2025
#
#	Permission is hereby granted, free of charge, to any person obtaining a copy
#	of this software and associated documentation files (the "Software"), to deal
#	in the Software without restriction, including without limitation the rights
#	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#	copies of the Software, and to permit persons to whom the Software is
#	furnished to do so, subject to the following conditions:
#
#	The above copyright notice and this permission notice shall be included in
#	all copies or substantial portions of the Software.
#
#	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#	THE SOFTWARE.
#
##################################################################################

import os
import sys
import requests
import io
import time
import zipfile
from shutil import move
from pathlib import Path
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from pyRBDome.Functions.RBDomeSQL import *
from pyRBDome.Classes.PDBAnalyser import *
from pyRBDome import config


class BindUPanalysis():
    """ A class for submitting pdb files and chains to the BindUP server.
    Requires the pdb ID (i.e. 'ILV7'). By default it analyses all the chains that
    have protein sequences in the pdb file.
    This class was successfully tested on MacOSX and Ubuntu machines using Chrome
    as default browser. To get it to work you need to have the chromedriver
    script(download from Google) in your system path! The chromedriver script
    should also be compatible with your version of Chrome.
    Safest is to download the latest versions of both! """

    def __init__(self,
                 verbose=True,
                 overwrite=False,
                 headless=False,
                 out_dir=None,
                 database=True,
                 database_verbose=False,
                 database_table='pyrbdome_analysis',
                 database_name='pyrbdome.db'):
                 
        """ Initialising the variables """
        self.out_dir = out_dir
        if not os.path.exists(self.out_dir):
            os.mkdir(self.out_dir)
        self.pdb_file = str()
        self.url = "https://bindup.technion.ac.il/"
        self.pdb_id = str()
        self.chains = str()
        self.headless = headless
        self.bindup_file = str()
        self.downloaded = False
        self.download_link = str()
        self.overwrite = overwrite
        self.browser = None
        self.verbose = verbose
        self.connected = False
        self.job_id = str()
        self.job_finished = False
        self.job_submitted = False
        self.job_error = False
        self.database = database
        self.database_verbose = database_verbose
        self.database_table = database_table
        self.database_name = database_name
        #Add columns to the database. They will be populated later when calling some of the class methods
        if database:
            addEmptyColumn('BindUP', 
                            self.database_table, 
                            self.database_name, 
                            verbose=self.database_verbose
                            )

    def connectToServer(self):
        """ To connect to the BindUP server. """

        ### Setting Chrome options:
        download_dir = str()
        if self.out_dir:
            download_dir = Path(self.out_dir)
        else:
            download_dir = Path(os.getcwd())
            
        ### Set Chrome options
        chrome_options = webdriver.ChromeOptions()
        chrome_options.binary_location = config.CHROME_PATH
        
        ### Configure Chrome options:
        chrome_options.add_argument(f'--chromedriver={config.CHROME_DRIVER_PATH}')
        prefs = {'download.default_directory' : '%s' % download_dir}
        chrome_options.add_experimental_option('prefs', prefs)

        if self.headless:
            chrome_options.add_argument('--headless')
            chrome_options.add_argument("--window-size=1920,1200")
            chrome_options.add_argument("--disable-gpu")  # Disable GPU acceleration
            chrome_options.add_argument('--no-sandbox')  # This might help in headless mode
            chrome_options.add_argument('--disable-dev-shm-usage')  # Disable shared memory for headless mode

        ### Connecting to the BindUP website:
        self.browser = webdriver.Chrome(options=chrome_options)
        self.browser.implicitly_wait(20)
        self.browser.get(self.url)
        self.current_url = self.browser.current_url
        self.connected = True
        if self.verbose:
            sys.stdout.write("Successfully connected to the BindUP server\n")

        self.connected = True
        return True

    def submitJob(self,pdb_file, chain):
        """ Submits the job to the server if all the required information has been
        submitted to the server. You need to provide the path to your pdb file as
        input. The pdb name will subsequently be included in the output pdb file."""

        self.pdb_file = os.path.abspath(pdb_file)
        self.pdb_id = os.path.splitext(os.path.basename(pdb_file))[0]
        self.chain = chain

        if not os.path.isfile(pdb_file):
            sys.stderr.write(f"ERROR! Cannot find your pdb file in {self.pdb_id}")
            return False

        if not self.connected:
            sys.stderr.write("ERROR! You are not connected to the BindUP server!\n")
            return False
        else:
            if self.verbose:
                sys.stdout.write(f"Submitting job {self.pdb_id}.pdb with chain {self.chain}\n")
            ### Find the element by its ID and click it
            experimental_mode_element = self.browser.find_element(By.CSS_SELECTOR,"input[name='model_type'][value='experimental']")
            experimental_mode_element.click()

            radio_button = self.browser.find_element(By.CSS_SELECTOR,"input[name='input_type'][value='file']")
            radio_button.click()

            ### Uploading the pdb file:
            # Find the file input element
            file_input = self.browser.find_element(By.NAME, "PDB_file")

            # Wait for the file input element to be clickable
            file_input = WebDriverWait(self.browser, 10).until(EC.element_to_be_clickable((By.NAME, "PDB_file")))

            # Provide the path to your .pdb file:
            # Upload the file by sending the file path to the input element
            file_input.send_keys(self.pdb_file)

            ### Setting the chain to be analysed:
            # Use the correct selector with variable interpolation
            chain_radio_button = WebDriverWait(self.browser, 10).until(
                EC.visibility_of_element_located(
                    (By.CSS_SELECTOR, f"input[name='chain_type'][value='selected_chain']")
                )
            )

            # Click the radio button
            chain_radio_button.click()

            # Step 1: Wait until the chain_id input field is located
            chain_id_input = WebDriverWait(self.browser, 10).until(
                EC.visibility_of_element_located((By.NAME, "chain_id"))
            )

            try:# Step 2: Clear the input field (optional, but recommended if the input field might have previous data)
                chain_id_input.clear()
            except:
                pass

            # Step 3: Enter the selected chain into the input box
            chain_id_input.send_keys(self.chain)

            ### Submitting the job:
            submit_button = self.browser.find_element(By.CSS_SELECTOR,"input[type='image'][src='images/submit.png']")
            submit_button.click()

            self.job_submitted = True

            if self.verbose:
                sys.stdout.write(f"Job successfully submitted.\n")

    def downloadResults(self):
        """ Downloads the results pdb file if the job finished finished. """
    
        if not self.pdb_file or not self.chain:
            sys.stderr.write("ERROR! You need to submit a PDB file and a chain before you can download any results!\n")

        ### Setting a time-out. Change this if you have a very large complex.
        timeout = 300 ### Can't take more than 5 minutes to do a BindUP run!

        pdb = PDBAnalyser()
        pdb.loadPDBFile(self.pdb_file)
        pdb.pdb_df = pdb.pdb_df.loc[pdb.pdb_df['chain_id'] == self.chain]
        pdb.pdb_df['b_factor'] = 0

        try:
            # Wait until the download button is clickable
            self.download_button = WebDriverWait(self.browser, timeout).until(
                EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT, "View the results"))
            )
        except:
            sys.stderr.write(f"Analysis of {self.pdb_id} is taking too long! Exiting!\n")
            self.bindup_file = None
            self.job_finished = True
            self.job_error = True
            return False
        else:
            self.download_link = self.download_button.get_attribute("href")
            self.job_finished = True
            self.job_id = self.download_link.split("/")[-1]
            self.bindup_file = f"{self.out_dir}/{self.pdb_id}_{self.chain}_BindUP.pdb"

            if os.path.exists(self.bindup_file) and not self.overwrite:
                sys.stderr.write(f"ERROR! File {self.bindup_file} already exists and you asked me not to overwrite any files!\n")
                #Populate the column BindUP of the sqlite database
                if self.database:
                    updateColumn(self.database_table, 
                                'BindUP == "Results downloaded"', 
                                'pdb_id == "%s"' % self.pdb_id,
                                self.database_name, 
                                verbose=self.database_verbose
                                )
                return False
            elif not os.path.exists(self.bindup_file):
                # Step 1: Download the ZIP file
                response = requests.get(self.download_link)
                # Check if the download was successful
                if response.status_code == 200:
                    results = response.content.strip().decode("utf-8")
                    #print(results)
                    if re.search("Largest Positive Patch", results):
                        amino_acids = results.split('\n')[-1].split()
                        residue_numbers = [int(i[3:]) for i in amino_acids]
                        pdb.pdb_df.loc[pdb.pdb_df['residue_number'].isin(residue_numbers),'b_factor'] = 10
                        pdb.writePDBFile(self.bindup_file)

                        if self.database:
                            updateColumn(self.database_table, 
                                        'BindUP == "Results downloaded"', 
                                        'pdb_id == "%s"' % self.pdb_id, 
                                        self.database_name, 
                                        verbose=self.database_verbose
                                        )
                        return True
                    else:
                        sys.stderr.write(f"ERROR! No positively charged patched found in {os.path.basename(self.pdb_file)}")
                        
                        # Writing the PDB output file but with empty b-factor column (i.e no patch)
                        pdb.writePDBFile(self.bindup_file)
                        
                        if self.database:
                            updateColumn(self.database_table, 
                                        'BindUP == "ERROR! No positively charged patched found"', 
                                        'pdb_id == "%s"' % self.pdb_id, 
                                        self.database_name, 
                                        verbose=self.database_verbose
                                        )
                        return False
                else:
                    sys.stderr.write(f"ERROR! Response code {response.status_code}! Job {self.job_id} is not present on the server!.\n")
                    #Populate the column BindUP of the sqlite database
                    if self.database:
                        updateColumn(self.database_table, 
                                    'BindUP == "ERROR! No results!"', 
                                    'pdb_id == "%s"' % self.pdb_id, 
                                    self.database_name, 
                                    verbose=self.database_verbose
                                    )
                    return False
            else:
                sys.stderr.write(f"ERROR! No results were generated for the job or file already exists!\n")
                #Populate the column BindUP of the sqlite database
                if self.database:
                    updateColumn(self.database_table, 
                                'BindUP == f"ERROR! No results were generated for job\n"', 
                                'pdb_id == "%s"' % self.pdb_id,
                                self.database_name, 
                                verbose=self.database_verbose)
                return False 

    def closeBrowser(self):
        """ Closes the driver that runs the browser """
        if self.verbose:
            sys.stdout.write("Quitting browser\n")
        self.browser.quit()
